
import copy
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit, newton, brentq, broyden1, minimize_scalar, minimize
from scipy.interpolate import UnivariateSpline, InterpolatedUnivariateSpline, interp1d
import warnings
import pylab # needed?
import os.path as op
import os
import glob
import csv
import time
import argparse
import warnings
import sys, getopt
import re
import networkx as nx
import cPickle as pickle

# TODO: For the relative IDT I get a lower bound of about 49. Should be zero right? Find out
# if something goes wrong somewhere. For a high temperature results file, plot the avgMI(t)
# of a sample node, and also the probability curve, and see if it is really halftime 49, or that
# there is a bug somewhere.

# assume that every rel. IDT is lower than this value (for brentq root finding)
max_relidt = 1e08
# assume that every abs. IDT is lower than this value (for brentq root finding)
max_idt = 1e08
# the 'brentq' option is much more accurate, the 'newton' option seems faster
method_find_root = 'newton'
method_find_root = 'brentq'
method_find_root = 'brentq_only'  # same as 'brentq' but without custom code to find a tight range to search in
max_iter_root_finding = 3000


### ASSUMING DOUBLE EXPONENTIAL DECAY ###

def residuals_double_decay(params, xdata, ydata):
    '''
    Residuals of the curve a*exp(-b*x) + c*exp(-d*x) + plateau
    :param params: (a, b, c, d, plateau)
    :return: residuals of fitting to xdata, ydata
    '''
    a, b, c, d, plateau = params
    return np.linalg.norm(a*np.exp(-b*xdata) + c*np.exp(-d*xdata) + plateau - ydata)


def values_double_decay(params, xdata):
    '''
    Values of the curve a*exp(-b*x) + c*exp(-d*x) + plateau.
    :param params: (a, b, c, d, plateau)
    :param xdata:
    :return: a*exp(-b*x) + c*exp(-d*x) + plateau
    '''
    a, b, c, d, plateau = params
    return a*np.exp(-b*xdata) + c*np.exp(-d*xdata) + plateau


def fit_double_exp_decay(mi_over_time, ntrials=5):
    """
    Residuals of the curve a*exp(-b*x) + c*exp(-d*x) + plateau
    :return: (a, b, c, d, plateau), making sure that b >= d (so the first exponential is the fast decay).
    To calculate the two corresponding relative IDTs (fraction 1/e) you simply do 1/b and 1/d. For the
    absolute IDTs to MI=eps do -1/b * np.log(eps/a) and -1/d * np.log(eps/c)
    """
    if ntrials == 1:
        optres = minimize(residuals_double_decay,
                          x0=np.array([min(0.1, max(mi_over_time)), 3., min(0.9, max(mi_over_time)), 1., 0.]),
                          args=(np.arange(len(mi_over_time)), np.array(mi_over_time)),
                          bounds=[(0., max(mi_over_time)),
                                  (0., 100.),
                                  (0., max(mi_over_time)),
                                  (0., 100.),
                                  (0., max(mi_over_time))])

        if optres.success:
            a, b, c, d, plateau = optres.x  # readability
            if b < d:
                return c, d, a, b, plateau
            else:
                return a, b, c, d, plateau
        else:
            raise UserWarning('could not find solution')
    else:
        # first call matches the above case
        optres = [None]*ntrials  # prealloc

        optres[0] = minimize(residuals_double_decay,
                             x0=np.array([0.1*max(mi_over_time), 3., 0.9*max(mi_over_time), 1., 0.]),
                             args=(np.arange(len(mi_over_time)), np.array(mi_over_time)),
                             bounds=[(0., max(mi_over_time)),
                                     (0., 100.),
                                     (0., max(mi_over_time)),
                                     (0., 100.),
                                     (0., max(mi_over_time))])

        if ntrials > 1:
            optres[1] = minimize(residuals_double_decay,
                                 x0=np.array([0.5*max(mi_over_time), 3., 0.5*max(mi_over_time), 1., 0.]),
                                 args=(np.arange(len(mi_over_time)), np.array(mi_over_time)),
                                 bounds=[(0., max(mi_over_time)),
                                         (0., 100.),
                                         (0., max(mi_over_time)),
                                         (0., 100.),
                                         (0., max(mi_over_time))])

        initial_b_guess = max(-np.log(mi_over_time[1] / mi_over_time[0]), 0.01)

        if ntrials > 2:
            optres[2] = minimize(residuals_double_decay,
                                 x0=np.array([0.25*max(mi_over_time), initial_b_guess,
                                              0.75*max(mi_over_time), initial_b_guess * 0.33,
                                              0.]),
                                 args=(np.arange(len(mi_over_time)), np.array(mi_over_time)),
                                 bounds=[(0., max(mi_over_time)),
                                         (0., 100.),
                                         (0., max(mi_over_time)),
                                         (0., 100.),
                                         (0., max(mi_over_time))])

        if ntrials > 3:
            for oix in range(3, ntrials):
                r, r2, r3, r4 = np.random.random(4)
                optres[oix] = minimize(residuals_double_decay,
                                     x0=np.array([r*max(mi_over_time), r2 * initial_b_guess * 2.0,
                                                  (1.0-r)*max(mi_over_time), r2 * initial_b_guess * 2.0 * r3,
                                                  r4 * min(mi_over_time) * 1.5]),
                                     args=(np.arange(len(mi_over_time)), np.array(mi_over_time)),
                                     bounds=[(0., max(mi_over_time)),
                                             (0., 100.),
                                             (0., max(mi_over_time)),
                                             (0., 100.),
                                             (0., max(mi_over_time))])



        best_optres = optres[0]
        for res in optres:
            if res.success and (res.fun < best_optres.fun or not best_optres.success):
                best_optres = res

        if best_optres.success:
            a, b, c, d, plateau = best_optres.x  # readability
            if b < d:
                return c, d, a, b, plateau
            else:
                return a, b, c, d, plateau
        else:
            raise UserWarning('could not find solution')


def convert_double_exp_params_to_idts(params, epsilon=0.001):
    """
    Convert the result of fit_double_exp_decay() to IDT (or equivalently IDL) values, which may be
    more meaningful to your users as the units are number of time steps.
    :params params: result from fit_double_exp_decay()
    :param epsilon: a very small absolute and positive amount of mutual information
    :return: (rel IDT 1, rel IDT 2, abs IDT 1, abs IDT 2).
    Due to fit_double_exp_decay()'s ordering, rel IDT 2 >= rel IDT 1.
    """
    a, b, c, d, plateau = params
    return 1. / b, 1. / d, -1. / b * np.log(epsilon / a), -1. / d * np.log(epsilon / c)




### FUNCTIONS FOR SINGLE (NON-PARAMETRIC) DECAY ###




# calculate the average mutual information for a node given its list of "conditional probability curves" and H(prior)
# by calculating the entropies (H()) on each probability curve and then averaging. The prob.
# curves are actually stored as lists of three parameters, each of which identifies an exponential curve
# which is assumed to fit the measured conditional probability curve well and can also be used
# for extrapolation. 
# @probparams:   [[a1,b1,1/0],[a2,b2,1/0],...]
#
# returns <H(s(T-t)|S(T))>
# NOTE: this function computes the arithmetic average and does not do the more exact Sum[p(S(T))*H(s(T-t)|S(T))]
# because the p(S(T)) is unknown in general. In the Ising spin case one could read the snapshot states S(T)
# and compute some prior prob. for these states, but in general I wouldn't know what to do... Technically
# this script should not need to care what the simulated dynamics was. TODO: let run_idt_*.py also output
# in a file the prior prob. of the init_*.dat state that it outputs?
def avgMI(t,probparams,heq,epsilon):
    assert(not isinstance(t, (list, tuple)))
##    assert t >= 0
    assert(np.isfinite(t))
    sumH = 0.0
    if (t < 0):
        return -float(heq)*(t-1.0)*2.
    else:
        for params in probparams:
            if params[2] == 1:
                if not (fitfuncfrom1(t,params[0],params[1]) >= -0.01 and fitfuncfrom1(t,params[0],params[1]) <= 1.01):
                    exit('error: t='+str(t)+', a='+str(params[0])+', b='+str(params[1])+', start='+str(params[2])+': p = '+
                         str(fitfuncfrom1(t,params[0],params[1])))
                sumH = sumH + H(fitfuncfrom1(t,params[0],params[1]))
            else:
                if not (fitfuncfrom0(t,params[0],params[1]) >= -0.01 and fitfuncfrom0(t,params[0],params[1]) <= 1.01):
                    exit('error: t='+str(t)+', a='+str(params[0])+', b='+str(params[1])+', start='+str(params[2])+': p = '+
                         str(fitfuncfrom0(t,params[0],params[1])))
                assert(params[2] == 0)
                sumH = sumH + H(fitfuncfrom0(t,params[0],params[1]))
        assert(sumH >= 0.0)
        assert(sumH / len(probparams) <= 1.0) # remove this once you support more than two states for nodes
        # return the mutual information H(s(T-t)) - H(s(T-t)|S(T)) but with epsilon subtracted,
        # so that a function like newton() can find the t at which MI==epsilon (root-finding)
        return heq - (sumH / len(probparams)) - epsilon

# debugging: same as avgMI but return prob instead of MI
def avgprob(t,probparams):
    assert(not isinstance(t, (list, tuple)))
    sumH = 0.0
    for params in probparams:
        if params[2] == 1:
            if not (fitfuncfrom1(t,params[0],params[1]) >= -0.01 and fitfuncfrom1(t,params[0],params[1]) <= 1.01):
                exit('error: t='+str(t)+', a='+str(params[0])+', b='+str(params[1])+', start='+str(params[2])+': p = '+
                     str(fitfuncfrom1(t,params[0],params[1])))
            sumH = sumH + fitfuncfrom1(t,params[0],params[1])
        else:
            if not (fitfuncfrom0(t,params[0],params[1]) >= -0.01 and fitfuncfrom0(t,params[0],params[1]) <= 1.01):
                exit('error: t='+str(t)+', a='+str(params[0])+', b='+str(params[1])+', start='+str(params[2])+': p = '+
                     str(fitfuncfrom0(t,params[0],params[1])))
            assert(params[2] == 0)
            sumH = sumH + fitfuncfrom0(t,params[0],params[1])
    assert(sumH >= 0.0)
    # return the mutual information H(s(T-t)) - H(s(T-t)|S(T)) but with epsilon subtracted,
    # so that a function like newton() can find the t at which MI==epsilon (root-finding)
    return (sumH / len(probparams))

# only find the first solution (MI=epsilon) and ignore all others
def idt(probparams,heq,epsilon=0.001):
##    heq = H(eqprob)
    assert(heq >= 0.0)

    if (heq < epsilon):
        # in case the conditional entropy (condent) curve never reaches the value epsilon,
        # then the root finding function below cannot find the root of course, and will give
        # a warning. Since the threshold is 'already' reached, technically I'll say the time
        # to reach it is 0:
        t = 0.0
##    elif ((heq > epsilon and (heq - max(condent)) > epsilon) if not allowLinearExtrapolationCondEnt else False):
##        # if this happens then you most likely did not choose the parameter 'numsteps' big enough.
##        # The state probability curve of the node does not yet settle to its equilibrium value
##        # within the current time frame, and so also the cond. ent. curve does not settle yet, and
##        # its settlement value is estimated to be later than the end of the current timeframe.
##        # Either now I would have to do some extrapolation of the cond. ent. curve to find an estimate
##        # for IDT, or I return some infinity value to say that I couldn't find it. I'll do the latter.
##        # NOTE: visually, doing a linear interpolation of the last (few) points seems reasonable
##        # to do for testing purposes, to get some intelligible results for small simulation runs.
##        # Regulate this with setting allowLinearExtrapolationCondEnt above.
##        t = np.inf
    else:
        try:
            # find the root (only the first one) of the Heq - Hcond(t) - epsilon curve
##            t = newton(avgMI,x0=1,maxiter=1000,args=(probparams,heq,epsilon),tol=epsilon/100.) # old
            if (heq == 0.0):
                # regardless of the root-finding method, for a flat MI curve the root cannot be found.
                
                # for insufficient numbers of simulations, it may happen that a particular
                # node's state is (practically) never observed to fluctuate. Then its state probability is
                # very close to a constant 1.0 (for Ising spins), and the estimated Heq can become
                # exactly zero, and the MI curve then becomes a perfect flat line. (I verified this
                # with the commented-out section of code in the second elif below.) In this case
                # the halftime of decay of this curve becomes ill-defined, so I return NaN.
                # You can remedy this by (i) lowering the temperature and/or (ii) increase the
                # number of simulation runs.
                
                t = np.nan

                warning('equilibrium entropy of a node\'s state is exactly zero. Setting abs. IDT = '+str(t))
            else:
                t = -3 # erroneous value
                
                if (method_find_root == 'newton'):
                    # I sometimes got bad tolerances from this method, I suggest using brentq instead
                    t = newton(avgMI,x0=1,maxiter=max_iter_root_finding,args=(probparams,heq,epsilon),tol=epsilon/100.)

                    bad_fit = abs(avgMI(t,probparams,heq,0)-epsilon) > epsilon/100.

                    if (bad_fit):
                        warning('idt: newton() reached a relative error of '+str(abs(avgMI(t,probparams,heq,0)-epsilon)/float(epsilon))+
                                ', which is larger than 1%, so I will now repeat the root finding with '
                                'the brentq method. (t='+str(t)+')')
                else:
                    bad_fit = False

                # note: this brentq method can also be used in case the newton() method
                # failed to keep the error within the tolerance of 1% (sometimes happens)
                # (todo: is this method slower?)
                if method_find_root == 'brentq' or bad_fit == True:
    ##                assert heq > 0.0, "error: idt: handle the case where H_eq == 0 (IDT = 0?)"

                    # find an initial guess for the left side of a range around the root of avgMI
                    t_left = 0.05
                    if avgMI(t_left,probparams,heq,epsilon) <= 0.0:
                        # cannot go further back
                        t_left = 0.0
                        assert avgMI(t_left,probparams,heq,epsilon) > 0.0
                    else:
                        while avgMI(t_left*2.0,probparams,heq,epsilon) > 0.0 and t_left < max_idt*0.1:
                            t_left = t_left * 2.0
                        assert avgMI(t_left,probparams,heq,epsilon) > 0.0

                    # find an initial guess for the right side of a range around the root of avgMI
                    t_right = 200.0
                    # I'm on the wrong side of the root, run to the right
                    if avgMI(t_right,probparams,heq,epsilon) >= 0.0:
                        while avgMI(t_right,probparams,heq,epsilon) >= 0.0 and t_right < max_idt:
                            t_right = t_right * 1.5
                        assert avgMI(t_right,probparams,heq,epsilon) < 0.0
                    else:
                        # I'm on the correct side of the root, run left towards the root
                        while avgMI(t_right*0.7,probparams,heq,epsilon) < 0.0 and t_right > t_left:
                            t_right = t_right * 0.7
                        assert avgMI(t_right,probparams,heq,epsilon) < 0.0

                    t = brentq(avgMI, t_left, t_right, args=(probparams,heq,epsilon), rtol=1e-6, maxiter=max_iter_root_finding)
                    
                if t == -3: # still erroneous value? (unchanged?)
                    exit('error: relidt: unsupported method for root finding: '+str(method_find_root))
            # note: using avgMI means that extrapolation is used automatically (but I'm confident
            # about it)
            assert(t >= 0.0 or heq == 0.0)
            # found a good solution?
            if (heq > 0.0):
                # within 1%?
                assert(abs(avgMI(t,probparams,heq,0)-epsilon) <= epsilon/100.) 
        except RuntimeError:
            print('error: heq = '+str(heq))
            print('error: frac = '+str(frac))
            print('error: epsilon = '+str(epsilon))
            print('error: probparams = '+str(probparams))
            print('error: method for root finding: '+str(method_find_root))
##            print('----\nerror: message:\n'+str(e)+'----\n')
            exit('error: idt: exception occurred during root-finding.')
            t = -1
    return t

num_warnings = 0
max_warnings = 10

# display warning message (unless already too many were printed before at any time)
def warning(s, mx=-1):
    global num_warnings
    global max_warnings
    if num_warnings < max_warnings and mx == -1 or num_warnings < mx and mx != -1:
        print('warning: '+str(s))
        num_warnings = num_warnings + 1
        if num_warnings == max_warnings:
            print('warning: further warnings will not be displayed (max. '+str(max_warnings)+' reached)')


class DecayTimeResponse():
    decay_times = None  # list of floats
    asymptotic_values = None  # list of floats

    # starting from these indices (rounded down) the decay_times are calculated. each index is argmax_t MI(t)
    # per node
    peak_locations = None  # list of floats


# NOTE: try to use abs_idts_raw instead
def abs_idts_raw_old(mi_per_node, heqs=None, epsilon=0.001, allow_extrapolation=False):
    if not heqs is None:
        assert len(mi_per_node) == len(heqs)

    # todo: also correct for lower MI in abs_*, like in rel_idts_raw

    # TODO: phase out the use of heqs, it has no function anymore. Then also assume_uniform_entropy can be removed.

    idts = [-2]*len(mi_per_node)

    # if the 'equilibrium' entropy per node is an array then it is assumed to depend on the time-since-snapshot,
    # otherwise it is assumed constant. It can depend on the time-since-snapshot for instance if the snapshots were
    # generated according to some constraints, such as a specific magnetization value.
    if not heqs is None:
        assume_uniform_entropy = bool(np.isscalar(heqs[0]))
    else:
        assume_uniform_entropy = True

    for i in xrange(len(mi_per_node)):
        assert len(mi_per_node[0]) > 1  # should equal 'numsteps' but I don't know that variable here

        mi_pts_for_zero = np.subtract(mi_per_node[i], float(epsilon))

        # note: I think it is theoretically possible, in case assume_uniform_entropy == False, to have heq[0] == 0.0
        # for some node, but that other nodes have heq[0] > 0.0, and that for some t>0 the first node also has
        # non-zero entropy (heq[t] > 0.0), so that mutual_information(t) for this first node is first zero, then
        # increases to a peak, and then drops to some constant again. In this case the 'IDT' becomes a bit ill-defined?
        # I did not handle this case now but maybe I should. Basically the question is whether the mutual information
        # is a monotonically decreasing function (which I expect) or not.
        if __debug__ and len(mi_per_node[i]) > 10:
            diffs_mi = np.subtract(mi_per_node[i][1:], mi_per_node[i][:-1])

            # some rudimentary test to see if the mutual information curve seems to be a decreasing funnction on the
            # whole...
            # if this fails and it is not due to low --ntrials or low --numsteps then you can still comment this out,
            # but you should really check the intermediate results with --save_intermediate_results
            if not np.average(diffs_mi) <= 0.0:
                warnings.warn('abs_idts_raw: the mutual information curve seems to NOT be a decreasing funnction on the'
                              ' whole. The average dMI/dt is positive: '
                              + str(np.average(diffs_mi)) + '. if this fails and it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')
            if not np.average(diffs_mi[:int(len(diffs_mi)/2)]) <= 0.0:
                warnings.warn('abs_idts_raw: the mutual information curve seems to NOT be a decreasing funnction in the'
                              ' first half. The average dMI/dt in the first half is positive: '
                              + str(np.average(diffs_mi[:int(len(diffs_mi)/2)])) + '. if this fails and '
                                                                                                'it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')
            if not np.average(diffs_mi[:int(len(diffs_mi)/4)]) <= 0.0:
                warnings.warn('abs_idts_raw: the mutual information curve seems to NOT be a decreasing funnction in the'
                              ' first half. The average dMI/dt in the first half is positive: '
                              + str(np.average(diffs_mi[:int(len(diffs_mi)/2)])) + '. if this fails and '
                                                                                                'it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')

        try:
            mi_curve_for_zero = InterpolatedUnivariateSpline(range(0, len(mi_pts_for_zero)), mi_pts_for_zero, k=3)
        except:
            print 'error: mi_per_node = ', mi_per_node[:10]
            print 'error: mi_pts_for_zero = ', mi_pts_for_zero[:10]
            print 'error: rank mi_per_node = ', np.rank(mi_per_node)
            print 'error: rank mi_pts_for_zero = ', np.rank(mi_pts_for_zero)

            assert False

        first_mi = mi_per_node[0]

        if not heqs is None:
            heq = heqs[i]
        else:
            heq = first_mi

        # find zero of this curve, that is the IDT
        try:
            # find the root (only the first one) of the Heq - Hcond(t) - epsilon curve
            if (first_mi == 0.0):
                # regardless of the root-finding method, for a flat MI curve the root cannot be found. The MI could
                # still increase to a peak but then I did not really define what IDT means

                # for insufficient numbers of simulations, it may happen that a particular
                # node's state is (practically) never observed to fluctuate. Then its state probability is
                # very close to a constant 1.0 (for Ising spins), and the estimated Heq can become
                # exactly zero, and the MI curve then becomes a perfect flat line. (I verified this
                # with the commented-out section of code in the second elif below.) In this case
                # the halftime of decay of this curve becomes ill-defined, so I return NaN.
                # You can remedy this by (i) lowering the temperature and/or (ii) increase the
                # number of simulation runs.

                t = np.nan

                warning('equilibrium entropy of a node\'s state is exactly zero. Setting abs. IDT = '+str(t))
            else:
                t = -3 # invalid value

                if (method_find_root == 'newton'):
                    # I sometimes got bad tolerances from this method, I suggest using brentq instead
                    t = newton(mi_curve_for_zero,x0=2,maxiter=max_iter_root_finding,tol=epsilon/100.)

                    bad_fit = abs(mi_curve_for_zero(t)) > 1/100.

                    if (bad_fit):
                        warning('abs_idt_raw: newton() reached a relative error of '+str(abs(mi_curve_for_zero(t)))+
                                ', which is larger than 1%, so I will now repeat the root finding with '
                                'the brentq method. (t='+str(t)+')')
                else:
                    bad_fit = False

                if method_find_root == 'brentq' or bad_fit == True:
                    # find an initial guess for the left side of a range around the root of avgMI
                    t_left = 0.05
                    if mi_curve_for_zero(t_left) <= 0.0:
                        # cannot go further back
                        t_left = 0.0
                        assert mi_curve_for_zero(t_left) > 0.0
                    else:
                        while mi_curve_for_zero(t_left*2.0) > 0.0 and t_left < max_relidt*0.1:
                            t_left = t_left * 2.0
                        assert mi_curve_for_zero(t_left) > 0.0

                    # find an initial guess for the right side of a range around the root of avgMI
                    t_right = 200.0
                    # I'm on the wrong side of the root, run to the right
                    if mi_curve_for_zero(t_right) >= 0.0:
                        while mi_curve_for_zero(t_right) >= 0.0 and t_right < max_relidt:
                            t_right = t_right * 1.5
                        #debugging
                        if mi_curve_for_zero(t_right) >= 0.0:
                            ts = np.linspace(0.0,200.0,1000)
                            plt.figure()
                            plt.plot(ts,[mi_curve_for_zero(ti) for ti in ts])
                            plt.figure()
                            plt.plot([[ti]*len(probparams) for ti in ts],[[avgprob(ti,[pp]) for pp in probparams] for ti in ts])
                            print(str('error: t_right = '+str(t_right)+
                                                ', MI = '+str(mi_curve_for_zero(t_right))
                                                +', heq = '+str(heq)+', eps = '+str(epsilon)+
                                                ', probparams = '+str(probparams)))
                            plt.show()
                        assert mi_curve_for_zero(t_right) < 0.0, str('error: t_right = '+str(t_right)+
                                                                                ', MI = '+str(mi_curve_for_zero(t_right))
                                                                                +', heq = '+str(heq)+', eps = '+str(epsilon)+
                                                                                ', probparams = '+str(probparams))
                    else:
                        # I'm on the correct side of the root, run left towards the root
                        while mi_curve_for_zero(t_right*0.7) < 0.0 and t_right > t_left:
                            t_right = t_right * 0.7
                        assert mi_curve_for_zero(t_right) < 0.0

                    t = brentq(mi_curve_for_zero, t_left, t_right, rtol=1e-6, maxiter=max_iter_root_finding)

                # testing...
                # result: just as fast as the above 'brentq' option where I fit the xrange myself
                if method_find_root == 'brentq_only':
                    t = brentq(mi_curve_for_zero, 0.0, max_relidt, rtol=1e-6, maxiter=max_iter_root_finding)

                if t == -3: # still invalid init value?
                    exit('error: relidt: unsupported method for root finding: '+str(method_find_root))

            # note: using avgMI means that extrapolation is used automatically (but I'm confident
            # about it)
            assert(t >= 0.0 or first_mi == 0.0)

            # found a good solution?
            if (first_mi > 0.0):
                if not abs(mi_curve_for_zero(t) <= 1/100.): # consider some roundoff
                    # mi_curve_for_zero was constructed with subtracting epsilon so that brentq could find root
                    print('error: avgMI(t) = '+str(mi_curve_for_zero(t) + epsilon))
                    print('error: t = '+str(t))
                    print('error: heq = '+str(heq))
                    print('error: epsilon = '+str(epsilon))
                    print('error: tolerance tol = epsilon/100. = '+str(epsilon/100.))
                    print('error: error observed = '+str(abs(mi_curve_for_zero(t) - 0.0)))
                    print('error: method for root finding: '+str(method_find_root))
    ##                print('error: t_brentq = '+str(t_brentq))
    ##                print('error: avgMI(t_brentq,probparams,heq,0) = '+str(avgMI(t_brentq,probparams,heq,0)))
    ##                print('error: error t_brentq = '+str(abs(avgMI(t_brentq,probparams,heq,0) - heq*frac)))
                    plt.figure()
                    plt.plot([mi_curve_for_zero(ti) for ti in xrange(200)])
                    plt.figure()
                    plt.plot([mi_curve_for_zero(ti) for ti in xrange(200)])
                    exit('error: abs_idt_raw: bad fit returned.')
        except RuntimeError, e:
            print('error: heq = '+str(heqs[i]))
            print('error: epsilon = '+str(epsilon))
            print('error: MI[:5] = '+str(mi_per_node[:5]))
            print('error: allow extrapolation: '+str(allow_extrapolation))
            print('error: method for root finding: '+str(method_find_root))
##            print('----\nerror: message:\n'+str(e)+'----\n')
            exit('error: abs_idt_raw: exception occurred during root-finding: '+str(e))
            t = -1

        if t >= len(mi_per_node) and not allow_extrapolation:
            idts[i] = np.inf
        else:
            idts[i] = t

    return idts


def find_arg_max(array):
    """
    Return the x value such that array[x] is the maximum value, found by interpolation and optimization. This may
    smooth out noise in array.
    :param array: assumed to be a discretized univariate curve, array_like
     :type array: list
    :return: float
    :raise ValueError:
    """

    assert False, 'I have found a clear case where this method fails to find the maximum, so reprogram this?' \
                  ' maybe do a gaussian smooth and then np.argmax()?'

    func1d = UnivariateSpline(range(len(array)), np.subtract(0.0, array))

    optres = minimize_scalar(func1d, method='bounded', bounds=(0, len(array) - 1))

    if optres.success:
        return float(optres.x)
    else:
        raise ValueError('could not find the minimum?!')


def rel_idts_raw(mi_per_node, heqs=None, frac=0.5, allow_extrapolation=False):
    # wrapper for:
    return estimate_idts_raw(mi_per_node, heqs=heqs, frac=frac, allow_extrapolation=allow_extrapolation, method='rel')


def abs_idts_raw(mi_per_node, heqs=None, epsilon=0.01, allow_extrapolation=False):
    # wrapper for:
    return estimate_idts_raw(mi_per_node, heqs=heqs, frac=epsilon, allow_extrapolation=allow_extrapolation,
                             method='abs')


def estimate_idts_raw(mi_per_node, heqs=None, frac=0.5, allow_extrapolation=False, method='rel'):

    if not heqs is None:
        assert len(mi_per_node) == len(heqs)

    # TODO: phase out the use of heqs, it has no function anymore. Then also assume_uniform_entropy can be removed.
    # (I.e., never assume equilibrium entropy, always use the simulation results to estimate the entropy over time
    # for each node)

    # if the 'equilibrium' entropy per node is an array then it is assumed to depend on the time-since-snapshot,
    # otherwise it is assumed constant. It can depend on the time-since-snapshot for instance if the snapshots were
    # generated according to some constraints, such as a specific magnetization value.
    if not heqs is None:
        assume_uniform_entropy = bool(np.isscalar(heqs[0]))

        if assume_uniform_entropy:
            warnings.warn('You are assuming an equilibrium entropy per node, i.e., that it does not change over time'
                          '-since-snapshot, which would be better if you did not?')
    else:
        assume_uniform_entropy = True

    # pre-alloc
    idts = [-2]*len(mi_per_node)
    asympts = [-2]*len(mi_per_node)
    max_ats = [-2]*len(mi_per_node)

    assert method in ('rel', 'abs'), 'unrecognized method, should be rel or abs'

    relative = bool(method == 'rel')

    for i in xrange(len(mi_per_node)):
        first_mi = mi_per_node[i][0]
        if not heqs is None:
            heq = heqs[i]

            # note: heq is probably still a 1D list of length <numsteps>
        else:
            heq = first_mi

        # the MI does not always converge to 0, sometimes to a constant > 0, in which case the
        # computation of epsilon below would go wrong
        min_mi = mi_per_node[i][-1]

        # mi_per_node[i] is a discretized curve of mutual information over time. Find the point (time) where the
        # curve has its peak
        # max_at = find_arg_max(mi_per_node[i])  # this method was found to be wrong in one clear case
        max_at = np.argmax(mi_per_node[i])  # note: should a gaussian smooth occur first? what does it do at left end?

        # make sure that the epsilon (the threshold for MI to pass) is computed as a fraction of the initial mutual
        # information, which I suppose should equal I(s_0 | S_0) = H(s_0) - H(s_0 | S_0), and H(s_0 | S_0) = 0 so then
        # I(s_0 | S_0) = H(s_0), which is the first eq. entropy stored in heqs[i]
        if assume_uniform_entropy:
            # warnings.warn('should you really want assume_uniform_entropy=True? (looking for bug)')

            min_mi = min(heq, min_mi)

            if relative:
                epsilon = (heq - min_mi) * frac + min_mi
            else:
                epsilon = frac + min_mi  # absolute

            # I just ventured this test, see what happens... Due to the small derivation in the comment above I think
            # this must hold true.
            assert abs(first_mi - heq) < 0.01
        else:
            assert np.isscalar(heq[0]), 'assumed below'

            if heq[0] < min_mi:
                warnings.warn('seems very weird, heq=[0]=' + str(heq[0]) + ' is lower than min_mi=' + str(min_mi)
                              + ', which are the start end end value of the MI curve over time for node ' + str(i)
                              + ' (looking for bug)')

            min_mi = min(heq[0], min_mi)

            if relative:
                epsilon = (heq[0] - min_mi) * frac + min_mi
            else:
                epsilon = frac + min_mi  # absolute

        # note: I think it is theoretically possible, in case assume_uniform_entropy == False, to have heq[0] == 0.0
        # for some node, but that other nodes have heq[0] > 0.0, and that for some t>0 the first node also has
        # non-zero entropy (heq[t] > 0.0), so that mutual_information(t) for this first node is first zero, then
        # increases to a peak, and then drops to some constant again. In this case the 'IDT' becomes a bit ill-defined?
        # I did not handle this case now but maybe I should. Basically the question is whether the mutual information
        # is a monotonically decreasing function (which I expect) or not.
        if __debug__ and len(mi_per_node[i]) > 10:
            diffs_mi = np.subtract(mi_per_node[i][1:], mi_per_node[i][:-1])

            # some rudimentary test to see if the mutual information curve seems to be a decreasing funnction on the
            # whole...
            # if this fails and it is not due to low --ntrials or low --numsteps then you can still comment this out,
            # but you should really check the intermediate results with --save_intermediate_results
            if not np.average(diffs_mi) <= 0.0:
                warnings.warn('rel_idts_raw: the mutual information curve seems to NOT be a decreasing funnction on the'
                              ' whole. The average dMI/dt is positive: '
                              + str(np.average(diffs_mi)) + '. if this fails and it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')
            if not np.average(diffs_mi[:int(len(diffs_mi)/2)]) <= 0.0:
                warnings.warn('rel_idts_raw: the mutual information curve seems to NOT be a decreasing funnction in the'
                              ' first half. The average dMI/dt in the first half is positive: '
                              + str(np.average(diffs_mi[:int(len(diffs_mi)/2)])) + '. if this fails and '
                                                                                                'it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')
            if not np.average(diffs_mi[:int(len(diffs_mi)/4)]) <= 0.0:
                warnings.warn('rel_idts_raw: the mutual information curve seems to NOT be a decreasing funnction in the'
                              ' first half. The average dMI/dt in the first half is positive: '
                              + str(np.average(diffs_mi[:int(len(diffs_mi)/2)])) + '. if this fails and '
                                                                                                'it is '
                               'not due to low --ntrials or low --numsteps then you can still go further, but '
                               'you should really check the intermediate results with --save_intermediate_results.'
                               ' It could be that mutual_information(t) for this node is first zero, then increases to'
                               ' a peak, and then drops to some constant again. In this case the \'IDT\' becomes a '
                               'bit ill-defined I think... At least as originally intended.')

        mi_pts_shifted_around_zero = np.subtract(mi_per_node[i], float(epsilon))

        # only compute IDT starting from the peak,
        mi_pts_shifted_around_zero = mi_pts_shifted_around_zero[int(max_at):]

        assert len(mi_pts_shifted_around_zero) >= 2, 'is max_at near the end? that would be unexpected. ' \
                                                     'Higher --numsteps?'

        func_mi_curve_around_zero = InterpolatedUnivariateSpline(range(0, len(mi_pts_shifted_around_zero)),
                                                         mi_pts_shifted_around_zero, k=1)

        # ### DEBUGGING
        #
        # if len(heqs) == 1:  # most likely called by show_single_node.py
        #     ts = np.linspace(0.0, float(len(mi_pts_for_zero)), 1000)
        #     plt.figure()
        #     plt.plot(ts,[mi_curve_for_zero(ti) for ti in ts])
        #     plt.title('Debug plot; timesteps, [mi_curve_for_zero(ti) for ti in timesteps]')
        #     plt.show()
        #
        # ### END OF DEBUGGING

        if epsilon < first_mi:
            # find zero of this curve, that is the IDT
            try:
                # find the root (only the first one) of the Heq - Hcond(t) - epsilon curve
                if (first_mi == 0.0):
                    # regardless of the root-finding method, for a flat MI curve the root cannot be found.

                    # for insufficient numbers of simulations, it may happen that a particular
                    # node's state is (practically) never observed to fluctuate. Then its state probability is
                    # very close to a constant 1.0 (for Ising spins), and the estimated Heq can become
                    # exactly zero, and the MI curve then becomes a perfect flat line. (I verified this
                    # with the commented-out section of code in the second elif below.) In this case
                    # the halftime of decay of this curve becomes ill-defined, so I return NaN.
                    # You can remedy this by (i) lowering the temperature and/or (ii) increase the
                    # number of simulation runs.

                    t = np.nan

                    warning('equilibrium entropy of a node\'s state is exactly zero. Setting rel. IDT = '+str(t))
                else:
                    t = -3 # invalid value

                    if (method_find_root == 'newton'):
                        # I sometimes got bad tolerances from this method, I suggest using brentq instead
                        t = newton(func_mi_curve_around_zero,x0=2,maxiter=max_iter_root_finding,tol=epsilon/100.)

                        bad_fit = abs(func_mi_curve_around_zero(t) - 0.0) > 1/100.

                        if (bad_fit):
                            warning('rel_idt_raw: newton() reached a relative error of ' + str(abs(func_mi_curve_around_zero(t))) +
                                    ', which is larger than 1%, so I will now repeat the root finding with '
                                    'the brentq method. (t='+str(t)+')')
                    else:
                        bad_fit = False

                    if False:  # remove this? seems unnecessary, just as fast as elif
        ##                if not heq > 0.0: # debugging
        ##                    ts = np.linspace(0.0,200.0,1000)
        ##                    plt.figure()
        ##                    plt.plot(ts,[avgMI(ti,probparams,heq,0) for ti in ts])
        ##                    plt.figure()
        ##                    plt.plot([[ti]*len(probparams) for ti in ts],[[avgprob(ti,[pp]) for pp in probparams] for ti in ts])
        ##                    if debug_relidt_prob_data != []:
        ##                        plt.plot(np.transpose([debug_relidt_prob_data[pp] for pp in xrange(len(debug_relidt_prob_data))]),'--x')
        ##                    print('error: heq = '+str(heq))
        ##                    plt.show()
        ##                assert heq > 0.0, "error: handle the case where H_eq == 0 (IDT = 0?)"

                        # find an initial guess for the left side of a range around the root of avgMI
                        t_left = 0.05
                        if func_mi_curve_around_zero(t_left) <= 0.0:
                            # cannot go further back
                            t_left = 0.0
                            assert func_mi_curve_around_zero(t_left) > 0.0
                        else:
                            while func_mi_curve_around_zero(t_left*2.0) > 0.0 and t_left < max_relidt*0.1:
                                t_left = t_left * 2.0
                            assert func_mi_curve_around_zero(t_left) > 0.0

                        # find an initial guess for the right side of a range around the root of avgMI
                        t_right = 2.0
                        # I'm on the wrong side of the root, run to the right
                        if func_mi_curve_around_zero(t_right) >= 0.0:
                            while func_mi_curve_around_zero(t_right) >= 0.0 and t_right < max_relidt:
                                t_right = t_right * 1.5
                            #debugging
                            if func_mi_curve_around_zero(t_right) >= 0.0:
                                print('error: mi_curve_for_zero(t_right) >= 0.0')
                                ts = np.linspace(0.0,200.0,1000)
                                plt.figure()
                                plt.plot(ts,[func_mi_curve_around_zero(ti) for ti in ts])
                                plt.title('Debug plot: timesteps, [mi_curve_for_zero(ti) for ti in timesteps]')
                                # plt.figure()
                                # plt.plot([[ti]*len(probparams) for ti in ts],[[avgprob(ti,[pp]) for pp in probparams] for ti in ts])
                                # print(str('error: t_right = '+str(t_right)+
                                #                     ', MI = '+str(mi_curve_for_zero(t_right))
                                #                     +', heq = '+str(heq)+', eps = '+str(epsilon)+
                                #                     ', probparams = '+str(probparams)))
                                plt.show()
                            assert func_mi_curve_around_zero(t_right) < 0.0, str('error: t_right = '+str(t_right)+
                                                                            ', MI = '+str(func_mi_curve_around_zero(t_right))
                                                                            +', heq = '+str(heq)+', eps = '+str(epsilon))
                        else:
                            # I'm on the correct side of the root, run left towards the root
                            while func_mi_curve_around_zero(t_right*0.7) < 0.0 and t_right > t_left:
                                t_right = t_right * 0.7
                            assert func_mi_curve_around_zero(t_right) < 0.0

                        t = brentq(func_mi_curve_around_zero, t_left, t_right, rtol=1e-6, maxiter=max_iter_root_finding)

                    # seems equally fast as above, so remove above?
                    if method_find_root in ('brent', 'brentq_only') or bad_fit is True:
                        try:
                            t_left = 0.0
                            t_right = float(len(mi_pts_shifted_around_zero) - 1)

                            # if the signs of the left and right points are not opposite then try to shift the right
                            # side of the range so that its sign changes. This basically happens due to fluctuations in
                            # the curve, as far as I have seen, so this search should not take too long.
                            if func_mi_curve_around_zero(t_left) >= 0.0 and func_mi_curve_around_zero(t_right) >= 0.0:
                                warnings.warn('finding root: both end points are positive')

                                for tr in xrange(int(t_right), int(t_left), -1):
                                    if func_mi_curve_around_zero(tr) < 0.0:
                                        t_right = tr
                                        break
                            elif func_mi_curve_around_zero(t_left) < 0.0 and func_mi_curve_around_zero(t_right) < 0.0:
                                warnings.warn('finding root: both end points are negative')

                                print 'error: len(mi_per_node[i]) =', len(mi_per_node[i])
                                print 'error: mi_per_node[i][:10] =', mi_per_node[i][:10]
                                print 'error: mi_per_node[i][:10] =', mi_per_node[i][-10:]
                                print 'error: min(mi_per_node[i]) =', min(mi_per_node[i])
                                print 'error: max(mi_per_node[i]) =', max(mi_per_node[i])
                                print 'error: np.mean(mi_per_node[i]) =', np.mean(mi_per_node[i])
                                print 'error: epsilon = frac * heq =', epsilon
                                print 'error: heq =', heq
                                print 'error: frac =', frac
                                print 'error: min_mi =', min_mi
                                print 'error: max_at =', max_at
                                print 'error: relative =', relative

                                # note: mi_pts_shifted_around_zero is "mi_per_node[max_at:] - epsilon"

                                plt.figure()
                                plt.plot(mi_per_node[i])
                                plt.title('mi_per_node[i] (error: does not cross zero?)\nendpoints both negative')
                                plt.ylabel('Mutual information')
                                plt.xlabel('Time step')
                                plt.show()

                                assert False, 'that is just truly weird, mi_pts_shifted_around_zero(0.0) should be >= 0\n' \
                                              + 'mi_pts_shifted_around_zero[:10] = ' \
                                              + str(mi_pts_shifted_around_zero[:min(10, len(mi_pts_shifted_around_zero))]) \
                                              + ', epsilon = ' \
                                              + str(epsilon) + ', max_at = ' + str(max_at)
                                for tl in xrange(int(t_left), int(t_right), 1):
                                    if func_mi_curve_around_zero(tr) >= 0.0:
                                        t_right = tr
                                        break

                            t = brentq(func_mi_curve_around_zero, t_left, t_right, rtol=1e-6,
                                       maxiter=max_iter_root_finding)
                        except ValueError as e:
                            ### DEBUGGING

                            print('error: mi-epsilon curve does not drop below zero?')

                            ts = np.linspace(0.0, float(len(mi_pts_shifted_around_zero) - 1.0), 1000)
                            plt.figure()
                            plt.plot(ts,[func_mi_curve_around_zero(ti) + epsilon for ti in ts], '-k')
                            plt.plot(ts,[epsilon for ti in ts], '--', color='0.75')
                            plt.plot(mi_per_node[i], '.r')
                            plt.title('Debug plot:: timesteps, [mi_pts_shifted_around_zero(ti) for ti in timesteps]')
                            plt.show()

                            ### END OF DEBUGGING

                            assert False
                            t = np.inf
                        # optres = minimize_scalar(func_mi_curve_around_zero, method='bounded',
                        #                          bounds=(0.0, float(len(mi_pts_shifted_around_zero) - 1)))
                        #
                        # if not optres.success:
                        #     warnings.warn('minimization of function failed, for some reason. '
                        #                        'mi_pts_shifted_around_zero[:10] = '
                        #                        + str(mi_pts_shifted_around_zero[:min(10, len(mi_pts_shifted_around_zero))]))
                        # else:
                        #     t = optres.x

                    if t == -3: # still invalid init value?
                        exit('error: relidt: unsupported method for root finding: '+str(method_find_root))

                assert(t >= 0.0 or first_mi == 0.0)

                # found a good solution?
                if (first_mi > 0.0):
                    if not abs(func_mi_curve_around_zero(t) <= 1/100.): # consider some roundoff
                        # mi_curve_for_zero was constructed with subtracting epsilon so that brentq could find root
                        print('error: avgMI(t) = '+str(func_mi_curve_around_zero(t) + epsilon))
                        print('error: t = '+str(t))
                        print('error: heq = '+str(heq))
                        print('error: epsilon = '+str(epsilon))
                        print('error: tolerance tol = epsilon/100. = '+str(epsilon/100.))
                        print('error: error observed = '+str(abs(func_mi_curve_around_zero(t) - 0.0)))
                        print('error: method for root finding: '+str(method_find_root))
        ##                print('error: t_brentq = '+str(t_brentq))
        ##                print('error: avgMI(t_brentq,probparams,heq,0) = '+str(avgMI(t_brentq,probparams,heq,0)))
        ##                print('error: error t_brentq = '+str(abs(avgMI(t_brentq,probparams,heq,0) - heq*frac)))
                        plt.figure()
                        plt.figure()
                        plt.plot([func_mi_curve_around_zero(ti) for ti in xrange(0,100)])
                        plt.show()
                        exit('error: rel_idt_raw: bad fit returned.')
            except RuntimeError, e:
                print('error: heq = '+str(heqs[i]))
                print('error: epsilon = '+str(epsilon))
                print('error: MI[:5] = '+str(mi_pts_shifted_around_zero[:5]))
                print('error: allow extrapolation: {0}'.format(str(allow_extrapolation)))
                print('error: method for root finding: '+str(method_find_root))
    ##            print('----\nerror: message:\n'+str(e)+'----\n')
                exit('error: rel_idt_raw: exception occurred during root-finding: '+str(e))
                t = -1
        else:
            assert not relative or first_mi == min_mi or frac == 1.0, \
                'can only happen in case absolute IDT, because otherwise epsilon is calculated to lie between ' \
                'first_mi and min_mi'

            t = 0  # epsilon 'immediately' reached

        if t >= len(mi_pts_shifted_around_zero) and not allow_extrapolation:
            print('debug: t='+str(t)+' >= len(mi_pts_for_zero)='+str(len(mi_pts_shifted_around_zero))+' and not '
                   'allow_extrapolation, so idts['+str(i)+'] = np.inf')
            idts[i] = np.inf
        else:
            idts[i] = t + max_at

        asympts[i] = min_mi
        max_ats[i] = max_at

    resp = DecayTimeResponse()

    resp.decay_times = idts
    resp.asymptotic_values = asympts
    resp.peak_locations = max_ats

    return resp

# only find the first solution (MI=epsilon) and ignore all others
# Dec 2015: I think this is ***DEPRECATED***, use rel_idts_raw
def relidt(probparams,heq,frac=0.5,debug_relidt_prob_data=[]):
##    heq = H(eqprob)
    assert(heq >= 0.0)
    assert(frac <= 1.0 and frac >= 0.0)

    # what is the absolute MI that corresponds to fraction frac of its maximum?
    epsilon = heq * frac

    if (frac == 1):
        # corresponds to the maximum value itself, which is immediately reached
        t = 0.0

        assert (abs(avgMI(t,probparams,heq,0) - heq*frac) <= epsilon/90.)
    else:
        try:
            # find the root (only the first one) of the Heq - Hcond(t) - epsilon curve
            if (heq == 0.0):
                # regardless of the root-finding method, for a flat MI curve the root cannot be found.
                
                # for insufficient numbers of simulations, it may happen that a particular
                # node's state is (practically) never observed to fluctuate. Then its state probability is
                # very close to a constant 1.0 (for Ising spins), and the estimated Heq can become
                # exactly zero, and the MI curve then becomes a perfect flat line. (I verified this
                # with the commented-out section of code in the second elif below.) In this case
                # the halftime of decay of this curve becomes ill-defined, so I return NaN.
                # You can remedy this by (i) lowering the temperature and/or (ii) increase the
                # number of simulation runs.
                
                t = np.nan

                warning('equilibrium entropy of a node\'s state is exactly zero. Setting rel. IDT = '+str(t))
            else:
                t = -3 # invalid value
                
                if (method_find_root == 'newton'):
                    # I sometimes got bad tolerances from this method, I suggest using brentq instead
                    t = newton(avgMI,x0=1,maxiter=max_iter_root_finding,args=(probparams,heq,epsilon),tol=epsilon/100.)

                    bad_fit = abs(avgMI(t,probparams,heq,0) - heq*frac) > epsilon/100.

                    if (bad_fit):
                        warning('relidt: newton() reached a relative error of '+str(abs(avgMI(t,probparams,heq,0)-epsilon)/float(epsilon))+
                                ', which is larger than 1%, so I will now repeat the root finding with '
                                'the brentq method. (t='+str(t)+')')
                else:
                    bad_fit = False
                    
                if method_find_root == 'brentq' or bad_fit == True:
    ##                if not heq > 0.0: # debugging
    ##                    ts = np.linspace(0.0,200.0,1000)
    ##                    plt.figure()
    ##                    plt.plot(ts,[avgMI(ti,probparams,heq,0) for ti in ts])
    ##                    plt.figure()
    ##                    plt.plot([[ti]*len(probparams) for ti in ts],[[avgprob(ti,[pp]) for pp in probparams] for ti in ts])
    ##                    if debug_relidt_prob_data != []:                
    ##                        plt.plot(np.transpose([debug_relidt_prob_data[pp] for pp in xrange(len(debug_relidt_prob_data))]),'--x')
    ##                    print('error: heq = '+str(heq))
    ##                    plt.show()
    ##                assert heq > 0.0, "error: handle the case where H_eq == 0 (IDT = 0?)"

                    # find an initial guess for the left side of a range around the root of avgMI
                    t_left = 0.05
                    if avgMI(t_left,probparams,heq,epsilon) <= 0.0:
                        # cannot go further back
                        t_left = 0.0
                        assert avgMI(t_left,probparams,heq,epsilon) > 0.0
                    else:
                        while avgMI(t_left*2.0,probparams,heq,epsilon) > 0.0 and t_left < max_relidt*0.1:
                            t_left = t_left * 2.0
                        assert avgMI(t_left,probparams,heq,epsilon) > 0.0

                    # find an initial guess for the right side of a range around the root of avgMI
                    t_right = 200.0
                    # I'm on the wrong side of the root, run to the right
                    if avgMI(t_right,probparams,heq,epsilon) >= 0.0:
                        while avgMI(t_right,probparams,heq,epsilon) >= 0.0 and t_right < max_relidt:
                            t_right = t_right * 1.5
                        #debugging
                        if avgMI(t_right,probparams,heq,epsilon) >= 0.0:
                            ts = np.linspace(0.0,200.0,1000)
                            plt.figure()
                            plt.plot(ts,[avgMI(ti,probparams,heq,0) for ti in ts])
                            plt.figure()
                            plt.plot([[ti]*len(probparams) for ti in ts],[[avgprob(ti,[pp]) for pp in probparams] for ti in ts])
                            if debug_relidt_prob_data != []:                
                                plt.plot(np.transpose([debug_relidt_prob_data[pp] for pp in xrange(len(debug_relidt_prob_data))]),'--x')
                            print(str('error: t_right = '+str(t_right)+
                                                ', MI = '+str(avgMI(t_right,probparams,heq,epsilon))
                                                +', heq = '+str(heq)+', eps = '+str(epsilon)+
                                                ', probparams = '+str(probparams)))
                            plt.show()
                        assert avgMI(t_right,probparams,heq,epsilon) < 0.0, str('error: t_right = '+str(t_right)+
                                                                                ', MI = '+str(avgMI(t_right,probparams,heq,epsilon))
                                                                                +', heq = '+str(heq)+', eps = '+str(epsilon)+
                                                                                ', probparams = '+str(probparams))
                    else:
                        # I'm on the correct side of the root, run left towards the root
                        while avgMI(t_right*0.7,probparams,heq,epsilon) < 0.0 and t_right > t_left:
                            t_right = t_right * 0.7
                        assert avgMI(t_right,probparams,heq,epsilon) < 0.0

                    t = brentq(avgMI, t_left, t_right, args=(probparams,heq,epsilon), rtol=1e-6, maxiter=max_iter_root_finding)

                # testing...
                # result: just as fast as the above 'brentq' option where I fit the xrange myself
                if method_find_root == 'brentq_only':
                    t = brentq(avgMI, 0.0, max_relidt, args=(probparams,heq,epsilon), rtol=1e-6, maxiter=max_iter_root_finding)
                    
                if t == -3: # still invalid init value?
                    exit('error: relidt: unsupported method for root finding: '+str(method_find_root))
                    
            # note: using avgMI means that extrapolation is used automatically (but I'm confident
            # about it)
            assert(t >= 0.0 or heq == 0.0)
            
            # found a good solution?
            if (heq > 0.0):
                if not (abs(avgMI(t,probparams,heq,0) - heq*frac) <= epsilon/100.): # consider some roundoff
                    print('error: avgMI(t,probparams,heq,0) = '+str(avgMI(t,probparams,heq,0)))
                    print('error: t = '+str(t))
                    print('error: heq = '+str(heq))
                    print('error: frac = '+str(frac))
                    print('error: epsilon = '+str(epsilon))
                    print('error: probparams = '+str(probparams))
                    print('error: tolerance passed as argument tol = epsilon/100. = '+str(epsilon/100.))
                    print('error: error observed (> tol + 11%) = '+str(abs(avgMI(t,probparams,heq,0) - heq*frac)))
                    print('error: method for root finding: '+str(method_find_root))
    ##                print('error: t_brentq = '+str(t_brentq))
    ##                print('error: avgMI(t_brentq,probparams,heq,0) = '+str(avgMI(t_brentq,probparams,heq,0)))
    ##                print('error: error t_brentq = '+str(abs(avgMI(t_brentq,probparams,heq,0) - heq*frac)))
                    plt.figure()
                    plt.plot([avgMI(ti,probparams,heq,0) for ti in xrange(200)])
                    plt.figure()
                    plt.plot([avgMI(ti,probparams,heq,epsilon) for ti in xrange(200)])
                    plt.figure()
                    plt.plot([[avgprob(ti,[pp]) for pp in probparams] for ti in xrange(200)])
                    if debug_relidt_prob_data != []:                
                        plt.plot(np.transpose([debug_relidt_prob_data[pp] for pp in xrange(len(debug_relidt_prob_data))]),'--x')
                    plt.show()
                    exit('error: relidt: bad fit returned.')
        except RuntimeError:
            print('error: heq = '+str(heq))
            print('error: frac = '+str(frac))
            print('error: epsilon = '+str(epsilon))
            print('error: probparams = '+str(probparams))
            print('error: method for root finding: '+str(method_find_root))
##            print('----\nerror: message:\n'+str(e)+'----\n')
            exit('error: relidt: exception occurred during root-finding.')
            t = -1
    return t


def read_mathematica_matrix(filename_data):
    fin = open(filename_data,'r')
    fin.seek(0)

    text = fin.read()
    text = text.join(text.split())
    if not text.find('{{') != -1 or not text.find('}}') != -1:
        print 'error: filename_data =', filename_data, 'not formatted as Mathematica list of lists.'
    # if you hit either of the following two assertions, delete the file named above (filename_data)
    # and rerun the run_idt_*py script with the --skip_existing option
    assert text.find('{{') != -1  # IsingOnCN started writing to file? (Not interrupted?)
    assert text.find('}}') != -1  # IsingOnCN finished writing to file? (Not interrupted?)
    text = text.replace('{{','').replace('}}','').replace('},{',';')

    fin.close()

    matrix = [map(float, s.split(',')) for s in text.split(';')]

    return matrix


def prior_likelihood_system_state(node_probs, networkfile):
    assert not networkfile is None

    node_probs = (np.array(node_probs) - 0.5) * 2.0

    adjlists = read_numbered_network(networkfile)

    J = 0.1  # interaction strength

    assert len(adjlists) == len(node_probs)

    energy = 0.0

    for nodeid in xrange(len(adjlists)):
        for nbr_node in adjlists[nodeid]:
            energy += 0.0 - node_probs[nodeid] * node_probs[nbr_node]

    energy *= J

    assert np.isscalar(energy)

    return np.exp(0.0 - energy)


def compute_mi_curve_per_node_raw(files_data_runs_single_temp, verbose=True, nodeids=None, networkfile=None,
                                  assume_uniform_entropy=True):
    """
    Compute a list of "mutual information" quantities for each node. The input is a list of output_* files which are in
     Mathematica format and store the conditional probabilities of each node's state, one row per time step. Each
     list of "mutual information" quantities will be equal in length as the number of rows in the data files.
    :param files_data_runs_single_temp: list of file names, like you would get from "ls output_spinprobs_triodos_3.0_run*"
    :param verbose: print debug and progress messages
    :return: [MI_per_node, heqs, Hcond_per_node]. The first and last are a list of lists, the second is a list of floats
    """
    equil_H_per_node = []
    # a list of state probabilities for each node (list of lists). This is used to compute the average probabilities
    # of state per node as function of time-since-the-snapshot. It is used in case assume_uniform_entropy=False.
    # For instance, the snapshots may be chosen to satisfy a particular magnetization value, or even one specific
    # snapshot, and then the entropy of each node is not invariant of the time-since-snapshot.
    probs_per_time_per_node = []
    Hcond_per_node = []

    numnodes = -1

    oi = 0
    fid = 0
    for filename_data in files_data_runs_single_temp:
        oi = oi + 1
        if verbose:
            print('note: going to process output file #'+str(oi)+' of '+
                  str(len(files_data_runs_single_temp)))
            print('debug: namely: '+filename_data)

        if op.getsize(filename_data) <= 0:
            if verbose:
                print('note: output file is empty. Will skip it...')
            continue

        try:
            matrix_probs = read_mathematica_matrix(filename_data)
        except AssertionError as e:
            print 'error: assertion failed while trying to read file \'', filename_data, '\'. Will skip it...'
            print 'error: message = ', e
            continue

        # each snapshot is a specific system state, and I will compute quantities averaged over snapshots. To be more
        # accurate I should compute a weighted average, using the prior likelihood of the system state as weight.
        # I compute this prior likelihood here.
        if not networkfile is None:
            prior_lh_system_state = prior_likelihood_system_state(matrix_probs[0], networkfile)
        else:
            prior_lh_system_state = 1.0  # do not weigh by prior likelihood, because I need network for that

        if not nodeids is None:
            matrix_probs = [[row[ni] for ni in nodeids] for row in matrix_probs]

        # rectangle matrix? each column in the file should be equally long
        if not len(set(map(len,matrix_probs))) == 1:
            print('debug: len(set(map(len,array))) = '+str(len(set(map(len,matrix_probs)))))
            print('debug: set(map(len,array)) =', set(map(len,matrix_probs)))
            print('debug: each column in the file \''+str(filename_data)+'\' should be equally long.')

        # this becomes now a list of probability curves, one for each node in the network
        tr_matrix_probs = np.transpose(matrix_probs)
        matrix_probs = []

        numnodes = len(tr_matrix_probs)
        numsteps = len(tr_matrix_probs[0])

        if fid == 0:  # initialize results vectors to correct length, which we know now
            assert equil_H_per_node == []  # not yet initialized
            equil_H_per_node = [0.0]*numnodes
            # should be list of curves, but don't know yet how long, but np.add accepts it
            Hcond_per_node = [0.0]*numnodes
            probs_per_time_per_node = [0.0]*numnodes
        else:
            if not len(equil_H_per_node) == numnodes:
                print 'debug: len(equil_H_per_node) = ', len(equil_H_per_node)
                print 'debug: numnodes = ', numnodes
                assert False

            # other files should have the exact same number of nodes
            assert len(equil_H_per_node) == numnodes
            assert len(probs_per_time_per_node) == numnodes

        nodes_range = xrange(numnodes)

        # TODO: weigh all average computations (below here) by prior_lh_state_prob, and divide by it after the loop

        for nodeid in nodes_range:
            # to get the long-term equilibrium entropy I will take the average of the Entropy[state probability
            # that is farthest away from the (non-equilibrium) snapshot], which for sufficient number of
            # snapshots should approximate well
            equil_H_per_node[nodeid] += H(tr_matrix_probs[nodeid][-1])

            if not assume_uniform_entropy:
                probs_per_time_per_node[nodeid] = np.add(probs_per_time_per_node[nodeid], tr_matrix_probs[nodeid])
                assert len(probs_per_time_per_node[nodeid] == numsteps)
            else:
                pass

            Hcond_given_snapshot = map(H, tr_matrix_probs[nodeid])

            Hcond_per_node[nodeid] = np.add(Hcond_per_node[nodeid], Hcond_given_snapshot)

        fid += 1

    nodes_range = xrange(numnodes)

    # if you specified a custom node id list, then remove all other node id values to prevent doing computations for
    # those which will be ignored anyway
    # if not nodeids is None:
    #     equilprob_per_node = [equilprob_per_node[ni] for ni in nodeids]
    #     Hcond_per_node = [Hcond_per_node[ni] for ni in nodeids]

    # compute the averages (not weighting for the prior prob. of each snapshot, which I probably should do)
    # TODO: weighted average by snapshot likelihoods
    if not assume_uniform_entropy:
        probs_per_time_per_node = np.divide(probs_per_time_per_node, float(len(files_data_runs_single_temp)))

        equil_H_per_node = [map(H, probs_over_time) for probs_over_time in probs_per_time_per_node]

        assert len(equil_H_per_node) == numnodes
        assert len(equil_H_per_node[0]) == numsteps
        assert len(equil_H_per_node[-1]) == numsteps
    else:
        equil_H_per_node = np.divide(equil_H_per_node, float(len(files_data_runs_single_temp)))
        assert equil_H_per_node[0] >= 0.0 and equil_H_per_node[0] <= 1.0

    # equilibrium (long-term) entropy of a node's state
    heqs = equil_H_per_node
    # conditional entropy curve per time step, per node
    Hcond_per_node = np.divide(Hcond_per_node, float(len(files_data_runs_single_temp)))
    # mutual information I(S(t) | s_i(t-m)) per node i
    MI_per_node = [np.subtract(heqs[j], Hcond_per_node[j]) for j in nodes_range]
    # MI_per_node = np.subtract(heqs, Hcond_per_node)

    assert len(MI_per_node) == len(heqs)
    assert len(MI_per_node[0]) == numsteps

    return [MI_per_node, heqs, Hcond_per_node]


#def isfinite(ls): # quick workaround because I don't have numpy here
#    return [not (math.isnan(x) or math.isinf(x)) for x in ls]

# find all occurrences of MI(t)=epsilon. If a node has multiple IDTs then most
# likely you didn't average over system states enough, and the cond. prob. curve.
# of a node passes through 0.5.
def idts(condent,heq,epsilon=0.001):
    # This function has not yet been adapted to the new situation where the conditional probability
    # curves are passed instead of the avg. conditional entropy curve, and that this happens in
    # the form of parameters of a fitted exponential function (see idt())
    # TODO: use idt() as base, and replace newton by something else?
    assert(False)
##    heq = H(eqprob)
    assert(heq >= 0.0)
    try:
        fitfunc = InterpolatedUnivariateSpline(np.arange(0,len(condent),dtype=float), np.subtract(heq - epsilon,condent), k=1)
        # fitfunc = interp1d(np.arange(0,len(condent),dtype=float), np.subtract(heq - epsilon,condent), bounds_error=False, fill_value=1.0)
    except RuntimeWarning:
        exit('fatal error: warning in idts() interpolating 1d (tolerance?)')
    ier = 0
    try:
        if not all(np.isfinite([fitfunc(x) for x in xrange(len(condent))])):
            print('error: not all function values are finite: \ndata points: '+str(np.subtract(heq - epsilon,condent)[:20])
                  + '\nfunction values: '+str([fitfunc(x) for x in xrange(20)]))
            print('error: heq = '+str(heq)+', epsilon = '+str(epsilon)+', condent = '+str(condent[:10]))
        assert(all(np.isfinite([fitfunc(x) for x in xrange(len(condent))])))
        #t,_,ier,_ = fsolve(fitfunc,1,full_output=True)
        t = broyden1(fitfunc,0)
        ier = 0
    except RuntimeError:
        t = [-1]
    if (t == [-1] or ier == 1):
        plt.plot(np.arange(len(condent),dtype=float),np.array(condent,dtype=float))
        plt.plot(np.arange(len(condent),dtype=float),np.subtract(heq - epsilon,condent))
        plt.show()
        exit('error: could not find crossing point with epsilon in idts()')
    return t if ier == 1 else [-1]

# transpose a rectangular 2d list
def transpose(l2d):
    return [[j[i] for j in l2d] for i in range(len(l2d[0]))]

# Shannon entropy function, binary
def H(p):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        
        if isinstance(p,(float,int,long)):
            assert(p >= -0.01 and p <= 1.01)
            p = max(0.0,min(1.0,p))
            if (p==1) or (p==0):
                return 0
            else:
                if not (p >= 0 and p <= 1):
                    exit('error: p = '+str(p))
                return -p*np.log2(p) - (1-p)*np.log2(1-p)
        else:
            return [(a if (a >= 0) else 0)
                    for a in (np.subtract(0.0,p)*np.log2(p) - np.subtract(1.0,p)*np.log2(np.subtract(1.0,p)))]

# this is an exponential function that starts at zero and converges to the value b (0<b<1)
def fitfuncfrom0(t,a,b):
    assert np.isfinite(a)
    assert np.isfinite(b)
    assert type(a) == float or type(a) == np.float64
    assert type(b) == float or type(b) == np.float64
    
    if not isinstance(t,(int,float,long)):
        if (b < 0):
            return [-1-2**(0-b)]*len(t)
        elif (b > 1):
            return [-1-2**(b-1)]*len(t)
        elif (a < 0):
            return [-1-2**(-int(a-1.0))]*len(t)
        elif any([g < 0 for g in t]):
            assert(False) # should do something smart here
        else:
            res = b-b*np.exp(-a*t)
            
            assert all([type(r) == float or type(r) == np.float64 for r in res])
            assert all([np.isfinite(r) for r in res])
            assert len(res) == len(t)
            assert len(np.shape(res)) == 1
            
            return res
    else:
        if (b < 0):
            return -1-2**(0-b)
        elif (b > 1):
            return -1-2**(b-1)
        elif (a < 0):
            return -1-2**(-int(a-1.0))
        elif (t < 0):
            return -1 + t
        else:
            return b-b*np.exp(-a*t)
        
# this is an exponential function that starts at 1 and converges to the value b (0<b<1)
def fitfuncfrom1(t,param_decay_rate,param_decay_limit):
    assert np.isfinite(param_decay_rate)
    assert np.isfinite(param_decay_limit)
    assert type(param_decay_rate) == float or type(param_decay_rate) == np.float64
    assert type(param_decay_limit) == float or type(param_decay_limit) == np.float64
    
    if not isinstance(t,(int,float,long)):
        if (param_decay_limit < 0):
            res = [-1-2.0**(0-param_decay_limit)]*len(t)
        elif (param_decay_limit > 1):
            res = [-1-2.0**(param_decay_limit-1)]*len(t)
        elif (param_decay_rate < 0):
            res = [-1-2.0**(-int(param_decay_rate-1.0))]*len(t)
        elif any([g < 0 for g in t]):
            assert False, 'error: fitfuncfrom1: unhandled' # should do something smart here
        else:
##            return [param_decay_limit+(1-param_decay_limit)*np.exp(-param_decay_rate*ti) for ti in t]
##            res = [param_decay_limit+(1-param_decay_limit)*np.exp(-param_decay_rate*float(ti)) for ti in t]
            res = param_decay_limit+(1-param_decay_limit)*np.exp(-param_decay_rate*t)
            
        if not all([type(r) == float or type(r) == np.float64 for r in res]):
            print('error: t = '+str(t))
            print('error: type of t = '+str(type(t)))
            print('error: param_decay_rate = '+str(param_decay_rate))
            print('error: param_decay_limit = '+str(param_decay_limit))
            print('error: type(res) = '+str(type(res)))
            print('error: len(res) = '+str(len(res)))
            print('error: depth(res) = '+str(len(np.shape(res))))
            ix = 0
            nummsgs = 0
            for r in res:
                if type(r) != float and type(r) != np.float64:
                    print('...error: type of element #'+str(ix)+' ('+str(r)+') is '+str(type(r)))
                    nummsgs = nummsgs + 1
                    if (nummsgs > 5):
                        break
                ix = ix + 1
            exit(1)
        
        assert all([type(r) == float or type(r) == np.float64 for r in res])
        assert all([np.isscalar(r) for r in res])
        assert all([np.isfinite(r) for r in res])
        assert len(res) == len(t)
        assert len(np.shape(res)) == 1
            
        return res
    else:
        if (param_decay_limit < 0):
            res = -1-2.0**(0-param_decay_limit)
        elif (param_decay_limit > 1):
            res = -1-2.0**(param_decay_limit-1)
        elif (param_decay_rate < 0):
            res = -1-2.0**(-int(param_decay_rate-1.0))
        elif (t < 0):
            res = -1 + t
        else:
            res = param_decay_limit+(1-param_decay_limit)*np.exp(-param_decay_rate*float(t))

        assert np.isscalar(res)
        assert np.isfinite(res)
        assert type(res) == float or type(res) == np.float64
        
        return res

def extract_size(fn):
    fn = op.basename(fn) # get filename only
    match = re.search(r'_N(\d+)',fn)
    assert(match)
    return int(match.group(1))

# read the adjacency lists from a file, format may vary
def read_numbered_network(filename_network):
    # use this function everywhere here. The code currently assumes that the node ids are string-typed integers,
    # like '0', '1', ... it supports different file types, but returning a
    # graph (directed) always in this way. Also checks if the numbering is contiguous

    ext = op.splitext(filename_network)[-1]

    if ext in ('.txt', '.csv'):
        fin = open(filename_network,'rb')
        # adjlists = fin.readlines()
        # adjlists = [s.replace('\n','') for s in adjlists]
        # adjlists = [s.split(',') for s in adjlists]
        # adjlists = [[(int(s)) for s in lst] if lst != [''] else [] for lst in adjlists]
        csvr = csv.reader(fin)
        adjlists = [map(int, row) for row in csvr]
        fin.close()

        network = nx.empty_graph(create_using=nx.DiGraph())

        for source_nodeid in xrange(len(adjlists)):
            for target_nodeid in adjlists[source_nodeid]:
                assert 0 <= target_nodeid < len(adjlists)  # contiguous ids? main() code assumes it
                network.add_edge(str(source_nodeid), str(target_nodeid))

        return network
    elif ext in ('.adjlist',):
        network = nx.read_adjlist(filename_network, create_using=nx.DiGraph())

        missing_nodeids = [nodeid for nodeid in xrange(network.number_of_nodes()) if not str(nodeid) in network.nodes()]

        # the code in main() assumes that all nodes are contiguously numbered but in string type. The given network
        # either is not numbered at all, or if it is, there are missing nodes
        # .adjlist from networkx can leave missing nodes if they have no edges, bit stupid... but oh well
        for missing_nodeid in missing_nodeids:
            network.add_node(str(missing_nodeid))

        allowed_nodeids = map(str, range(network.number_of_nodes()))

        for nodeid in graph.nodes_iter():
            if not nodeid in allowed_nodeids:
                raise ValueError('read_numbered_network(): in file ' + str(filename_network) + ' I found a node id '
                                 + str(nodeid) + ', which is not an integer in 0,...,N-1 for N='
                                 + str(network.number_of_nodes()))

        return network
    else:
        raise NotImplementedError('read_numbered_network(): I have no supported code for network file extension: ' + str(ext))

def extract_temp(fn):
    fn = op.basename(fn) # get filename only
    match = re.search(r'_T([\d]+\.{0,1}\d*)',fn)
    assert(match)
    return float(match.group(1))


### IDT OR IDL COMPUTATION:


class IDTResponse():
    idts = None
    numnodes = None
    numsteps = None
    heqs = None
    MI_per_node = None
    asymptotic_values = None

    # index x for which MI_per_node[i][x] is the peak (maximum) MI, where MI_per_node[i] is taken as a discretized
    # scalar curve. IDT is computed starting from this x
    peak_locations = None
    peak_MIs = None

def calc_idt_from_data_files(files_data, method='raw', assume_uniform_entropy=False, abs_idt=False,
                             threshold_frac_mi=0.5, threshold_abs_mi=0.01):

    if method in ('fit','exp','curve_fit'):

        # NOTE: this option might be DEPRECATED by the 'raw' option, below... 'raw' is currently preferable

        # I did not refactor the code for this method of calculating IDT in case not assuming uniform
        # entropy. I did do it for method in ('raw', 'direct').
        assert assume_uniform_entropy

        # for storing intermediate results for this temperature, preallocate (resetting will occur in loop)
        eqprobs=[[]]*len(files_data)
        probparams=[[]]*len(files_data)
        startprobs=[[]]*len(files_data)

        oi = 0 # for debug only
        fid = 0
        for filename_data in files_data:
            oi = oi + 1
            print('debug: going to process output file #'+str(oi)+' of '+str(len(files_data)))
            print('debug: namely: '+filename_data)

            if op.getsize(filename_data) <= 0:
                print('debug: output file is empty. Will skip it...')
                continue

            # fin = open(filename_data,'r')
            # fin.seek(0)
            #
            # text = fin.read()
            # text = text.join(text.split())
            # # if you hit either of the following two assertions, delete the file named above (filename_data)
            # # and rerun the run_idt_*py script with the --skip_existing option
            # assert text.find('{{') != -1  # IsingOnCN started writing to file? (Not interrupted?)
            # assert text.find('}}') != -1  # IsingOnCN finished writing to file? (Not interrupted?)
            # text = text.replace('{{','').replace('}}','').replace('},{',';')
            #
            # fin.close()
            #
            # array = [[convert2float(x) for x in s.split(',')] for s in text.split(';')] # intermediate step
            array = read_mathematica_matrix(filename_data)  # this function does the same as the preceding 10+ lines

            text = ""

            # square matrix? each column in the file should be equally long
            if not len(set(map(len,array))) == 1:
                print('debug: len(set(map(len,array))) = '+str(len(set(map(len,array)))))
                print('debug: ')

            # this is now a list of probability curves, one for each node in the network
            tr_array = np.transpose(array)
            array = []  # not used anymore hereafter

            numnodes = len(tr_array)
            numsteps = len(tr_array[0])

    ##        # the time over which the conditional entropy must be calculated and stored, and used for IDT calcualtion.
    ##        # Since the conditional entropy is calculated using a fitted exponential function to the prob. curve,
    ##        # this fit can be used to extrapolate the cond. ent. curve and find the IDT
    ##        timespanCondEnt = min(100000,numsteps)

            # initialize the 1D array in which the equilibrium state prob. of each node will be stored
            eqprobs[fid] = [-1]*numnodes
    ##        # initialize the 2D array in which the conditional entropy curve for each node will be stored
    ##        condents[fid] = [[-1]*numsteps]*numnodes
            # initialize the 2D array where the list of probability curves (in the shape of its parameters)
            # are stored for each node
            probparams[fid] = [[]]*numnodes
            # the probability at which a node starts in the cond. prob. curve (either 0 or 1)
            startprobs[fid] = [-1]*numnodes

            #loop through each node, make an exponential fit to its probability curve, use it to
            # calculate a (smooth) conditional prob. curve, and store its parameters in probparams
            for i in xrange(numnodes):
                # values 0,1,2,...,numsteps-1 at which the exponentials will be evaluated
                t = np.arange(0,numsteps,dtype=float)

                if tr_array[i][0] > 0.5:
                    startprobs[fid][i] = 1

                    try:
                        guess_decay_limit = np.mean(tr_array[i][-int(len(tr_array[i])/10):])
                        assert guess_decay_limit >= 0.0 and guess_decay_limit <= 1.0
                        assert np.isfinite(guess_decay_limit)
                        assert type(guess_decay_limit) == float or type(guess_decay_limit) == np.float64

                        assert(len(np.shape(tr_array[i])) == 1)
                        assert(len(tr_array[i]) == numsteps)
                        assert(all([tr_array[i][tx] >= 0.0 and tr_array[i][tx] <= 1.0 for tx in xrange(len(tr_array[i]))]))
                        # assert(all([type(tr_array[i][tx]) == float for tx in xrange(len(tr_array[i]))]))

                        try:
                            node_prob_curve = np.array(tr_array[i],dtype=float)

                            fitparams, fitcovar = curve_fit(fitfuncfrom1, t, node_prob_curve,
                                                            p0=[1., guess_decay_limit], maxfev = 3500)
                        except RuntimeError:
    ##                            print('error: message = '+str(e))
                            print('error: tr_array[i] = '+str(tr_array[i]))
                            print('error: np.shape(tr_array[i]) = '+str(np.shape(tr_array[i])))
                            print('error: type array: '+str(type(tr_array[i])))
                            print('error: type 1st element: '+str(type(tr_array[i][0])))
                            print('error: type np.array: '+str(type(node_prob_curve)))
                            print('error: type 1st element: '+str(type(node_prob_curve[0])))
                            exit(1)

                        fitvals = np.array(fitfuncfrom1(t,fitparams[0],fitparams[1]),dtype=float)
                        datavals = np.array(tr_array[i],dtype=float)
                        assert len(fitvals) == len(datavals)

                        sumsq = fitvals - datavals
                        assert len(sumsq) == len(fitvals)
                        assert len(np.abs(sumsq)) == len(fitvals)
                        # mean absolute difference
                        sumsq = np.mean(np.abs(sumsq))

                        if (sumsq > 0.1):
                            plt.figure()
                            plt.plot(t,fitfuncfrom1(t,fitparams[0],fitparams[1]),'-k',t,np.array(tr_array[i],dtype=float),'--o')
                            print('fatal warning: curve_fit (from 1) found a \'bad\' fit? params = '+str(fitparams))
                            print('debug: guess_decay_limit = '+str(guess_decay_limit))
                            plt.show()
                            print('error: curve_fit\'s returned fit is bad: square root of average '
                                 'squared error per time point: '+str(sumsq))
                    except RuntimeWarning:
                        plt.plot(t,fitfuncfrom1(t,fitparams[0],fitparams[1]),t,np.array(tr_array[i],dtype=float))
                        print('fatal warning: curve_fit found a \'bad\' fit? params = '+str(fitparams))

                    # due to the fitting procedure (especially in smaller test sets) the estimated
                    # equilibrium prob may be out of the bounds, but let's clip it then if it is not
                    # too crazy
                    # TODO: figure out how I can let curve_fit be bounded within parameter ranges
                    # (or let the fitfuncfrom1/0 return crazy numbers of the parameters are out of
                    # bound? hmmm)
                    if not (fitparams[1] < 1.1 and fitparams[1] > -0.1):
                        print('error: fitparams[1] = '+str(fitparams[1]))
                    assert(fitparams[1] < 1.1 and fitparams[1] > -0.1)
                    fitparams[1] = max(min(fitparams[1],1.0),0.0)
                    # same for fitparams[0], can never be negative
                    if fitparams[0] > -0.0001:
                        fitparams[0] = max(0.0,fitparams[0])
                    assert(fitparams[0] >= 0.0)

    ##                # H(s[i,t-d] | S[t]=y)
    ##                condents[fid][i] = H(fitfuncfrom1(np.arange(timespanCondEnt,dtype=float),fitparams[0],fitparams[1]))

                    #plt.plot(t,fitfuncfrom1(t,fitparams[0],fitparams[1]),t,np.array(tr_array[i]))
                else:
                    startprobs[fid][i] = 0

                    try:
                        # assert type(tr_array[i][-int(len(tr_array[i])/10):]) == list # finding bug
                        guess_decay_limit = np.mean(tr_array[i][-int(len(tr_array[i])/10):])
                        assert guess_decay_limit >= 0.0 and guess_decay_limit <= 1.0

                        fitparams, fitcovar = curve_fit(fitfuncfrom0, t, np.array(tr_array[i],dtype=float),
                                                        p0=[1., guess_decay_limit], maxfev = 3500)

                        fitvals = np.array(fitfuncfrom0(t,fitparams[0],fitparams[1]),dtype=float)
                        datavals = np.array(tr_array[i],dtype=float)
                        assert len(fitvals) == len(datavals)

                        sumsq = fitvals - datavals
                        assert len(sumsq) == len(fitvals)
                        assert len(np.abs(sumsq)) == len(fitvals)
                        # mean absolute difference
                        sumsq = np.mean(np.abs(sumsq))

                        if (sumsq > 0.1):
                            plt.figure()
                            plt.plot(t, fitfuncfrom0(t,fitparams[0],fitparams[1]),'-k',linewidth=2)
                            plt.plot(t, np.array(tr_array[i],dtype=float), '--x')
                            print('fatal warning: curve_fit (from 0) found a \'bad\' fit? params = '+str(fitparams))
                            print('debug: guess_decay_limit = '+str(guess_decay_limit))
                            print('debug: tr_array[i][:5] = '+str(tr_array[i][:5]))
                            plt.show()
                            print('error: curve_fit\'s returned fit is bad: square root of average '
                                 'squared error per time point: '+str(sumsq))

                    except RuntimeWarning:
                        plt.plot(t,fitfuncfrom0(t,fitparams[0],fitparams[1]),t,np.array(tr_array[i],dtype=float))
                        print('fatal warning: curve_fit found a \'bad\' fit? params = '+str(fitparams))
                    #fitparams = [0.05,0.5]

                    # due to the fitting procedure (especially in smaller test sets) the estimated
                    # equilibrium prob may be out of the bounds, but let's clip it then if it is not
                    # to crazy
                    # TODO: figure out how I can let curve_fit be bounded within parameter ranges
                    # check: didn't I fix this?
                    if not (fitparams[1] < 1.2 and fitparams[1] > -0.2):
                        print('error: fitparams[1] = '+str(fitparams[1]))
                    assert(fitparams[1] < 1.2 and fitparams[1] > -0.2)
                    fitparams[1] = max(min(fitparams[1],1.0),0.0)

    ##                # H(s[i,t-d] | S[t]=y)
    ##                condents[fid][i] = H(fitfuncfrom0(np.arange(timespanCondEnt,dtype=float),fitparams[0],fitparams[1]))

                    #plt.plot(t,fitfuncfrom0(t,fitparams[0],fitparams[1]),t,np.array(tr_array[i]))

                eqprobs[fid][i] = fitparams[1]

                # format: (exponent, convergence level, startprob)
                probparams[fid][i] = (fitparams[0],fitparams[1],startprobs[fid][i])

                fitparams = []  # not to be used anymore
                fitovar = []  # not to be used anymore

            fid = fid + 1

        # compute the equilibrium entropy using the equilibrium probability
        # values; this way ensures that avgMI(t->Infinity,...) will be zero.
        heq_per_file_per_node = [[float(H(peq)) for peq in eqprobs[fid]] for fid in xrange(len(files_data))]
        if not len(np.shape(heq_per_file_per_node)) == 2:
            # I did not refactor the code for this method of calculating IDT in case not assuming uniform
            # entropy. I did do it for method in ('raw', 'direct').
            assert assume_uniform_entropy

            print('error: np.shape(eqprobs) = '+str(np.shape(eqprobs)))
            print('error: np.shape(heq_per_file_per_node) = '+str(np.shape(heq_per_file_per_node)))
            print('error: type(eqprobs) = '+str(type(eqprobs)))
            print('error: type(heq_per_file_per_node) = '+str(type(heq_per_file_per_node)))
        assert len(np.shape(heq_per_file_per_node)) == 2
        assert all([all([type(he) == float for he in heqs_per_file]) for heqs_per_file in heq_per_file_per_node])
        assert len(set(map(len,heq_per_file_per_node))) == 1 # all arrays of heqs-per-node equal length?
        heqs = np.average(heq_per_file_per_node,0)
    elif method in ('raw', 'direct'):  # if you have no clue, this is better
        [MI_per_node, heqs, Hcond_per_node] = compute_mi_curve_per_node_raw(files_data,
                                                            assume_uniform_entropy=assume_uniform_entropy)

        numnodes = len(MI_per_node)  # initialize this variable, it is used afterwards
        numsteps = len(MI_per_node[0])  # initialize this variable, it is used afterwards

        assert len(MI_per_node) == len(Hcond_per_node)
        assert len(MI_per_node) == len(heqs)

        if not assume_uniform_entropy:
            assert len(heqs[0]) == numsteps
        else:
            assert np.isscalar(heqs[0])
    else:
        exit('unknown method '+str(method))

    ### from this point, the equil. entropies should be computed

    assert(len(heqs) == numnodes)

    print('debug: prob. and cond. ent. curves retrieved. Will now compute IDTs...')

    # NOTE: in order to find the 'equilibrium' entropy of a node's state, I FIRST compute the
    # entropy of each equilibrium state probability I found in the simulations, and THEN I
    # compute the average. If you do the other way then you'll get strange results. (Of course
    # I fell into this trap myself.)

    if method in ('fit', 'exp'):
        # todo: deprecate the filterAmbiguousNodes option? What was it for anyway?
        # if filterAmbiguousNodes:
        #     if abs_idt:
        #         idts = [idts([probparams[fid][i] for fid in xrange(len(files_data_per_temp[temp_output]))],heqs[i])
        #                 for i in xrange(numnodes)]
        #     else:
        #         exit('error: not supported yet: filterAmbiguousNodes and abs_idt=False')
            #plt.plot(np.arange(numnodes),np.array([i[0] for i in idts]),np.arange(numnodes),np.array([i[-1] for i in idts]))
        # else:
        if True:
            if abs_idt:
                idts = [idt([probparams[fid][i] for fid in xrange(len(files_data))],heqs[i])
                        for i in xrange(numnodes)]
            else:
                idts = [relidt([probparams[fid][i] for fid in xrange(len(files_data))],heqs[i],
                                   threshold_frac_mi) for i in xrange(numnodes)]

        asympts = None
        max_ats = None
    elif method in ('raw', 'direct'):
        if abs_idt:
            resp = abs_idts_raw(MI_per_node, heqs, threshold_abs_mi)

            idts = resp.decay_times
            asympts = resp.asymptotic_values
            max_ats = resp.peak_locations
        else:
            resp = rel_idts_raw(MI_per_node, heqs, threshold_frac_mi)

            idts = resp.decay_times
            asympts = resp.asymptotic_values
            max_ats = resp.peak_locations
    else:
        exit('unknown method '+str(method))

    idt_resp = IDTResponse()

    idt_resp.heqs = heqs
    idt_resp.idts = idts
    idt_resp.MI_per_node = MI_per_node
    idt_resp.numsteps = numsteps
    idt_resp.numnodes = numnodes
    idt_resp.asymptotic_values = asympts
    idt_resp.peak_locations = max_ats
    assert len(MI_per_node) == len(max_ats)
    if not max_ats is None and not MI_per_node is None:
        idt_resp.peak_MIs = [MI_per_node[ni][int(max_ats[ni])] for ni in xrange(len(max_ats))]
    else:
        idt_resp.peak_MIs = None

    return idt_resp


def predecessors_within_distance(network, nodeid, max_dist):
    all_predecessors = [nodeid]  # this will store all predecessors with (directed) distance <= dt
    new_predecessors = []

    for pred_dist in xrange(1, max_dist + 1):
        for target_node in all_predecessors:
            new_predecessors = new_predecessors + network.predecessors(target_node)

        all_predecessors = all_predecessors + new_predecessors
        all_predecessors = list(set(all_predecessors))
        new_predecessors = []

    return all_predecessors

class IDLResponse():
    idls = None
    numnodes = None
    numsteps = None
    max_mi_per_dist_per_node = None
    network = None
    asymptotic_values = None

    peak_locations = None
    peak_MIs = None


def calc_idl_from_data_files(network_fn, files_data, abs_idl=False, threshold_frac_mi=0.5, threshold_abs_mi=0.01,
                             tol_factor=0.1, allow_extrapolation=False, verbose=True):
    """
    Calculate an IDL value (scalar) per node in the network stored in the file <network_fn> given a set of data files

    :param allow_extrapolation: when estimating IDL from a curve of max MIs, allow extrapolation if needed
    :param network_fn:
    :param files_data: list of data filenames
    :type files_data: list
    :param abs_idl:
    :param threshold_frac_mi:
    :param threshold_abs_mi:
    :param tol_factor: a value between 0.0 and 1.0, where 0.0 is most accurate and 1.0 is most inaccurate. Basically,
    when looping over # timesteps into history, the loop is stopped when the maximum MI observed is less than the
    threshold * tol_factor, since it becomes unexpected that any maximum MI at any distance still becomes updated,
    or that the estimation of the characteristic length of the MI-over-distance changes significantly,
    whereas it costs a lot of computation to compute, especially when the number of steps approaches system size.
    :raise UserWarning: if no data files are found
    :rtype: IDLResponse
    """

    # note: there is no method argument, it is assumed to be 'raw', and fitting is not supported

    # helper function for MI calculation:
    # the term p * log(p) but then preventing division by zero, as is usual in info theory
    plog2p_divby = lambda p, div: p * np.log2(p / div) if 0 < p < 1 and 0 < div else 0.0

    if len(files_data) == 0:
        raise UserWarning('cannot compute IDL without data files... You gave me none.')

    # read the matrix stored in each data file in files_data
    output_per_file = []
    for fn in files_data:
        try:
            output_per_file.append(read_mathematica_matrix(fn))
        except:
            print 'note: had to skip a file, malformatted: ' + str(fn)

    if len(output_per_file) == 0:
        raise UserWarning('no single data file had a valid format of Mathematica list of lists! Tried '
                          + str(len(files_data)))

    # make a function for the prior state probability of each node at each time step, made faster by 1x precalculation
    prior_probs_precalc = np.sum(output_per_file, axis=0) / float(len(output_per_file))
    prior_prob = lambda nodeid, state, num_steps_ago=0: prior_probs_precalc[num_steps_ago][int(nodeid)] \
        if state == 1 else 1 - prior_probs_precalc[num_steps_ago][int(nodeid)]

    # subset of the outputs in output_per_file where the condition is met: at t=0 (snapshot), nodeid's prob == <state>
    # (state will be 0 or 1 only, since t=0 is the snapshot)
    outputs_given_node_has_state_at_snapshot = lambda nodeid, state: \
        [outp for outp in output_per_file if outp[0][int(nodeid)] == state]

    # function to retrieve the posterior probability of a given node for given state at given # timesteps ago
    # todo: let this function also precalc?
    def probs_given_node_has_state(given_nodeid, has_state):
        outputs_selected = outputs_given_node_has_state_at_snapshot(given_nodeid, has_state)
        freq_state_1_at_snapshot = np.sum(outputs_selected, axis=0)
        if len(outputs_selected) > 0:
            freq_state_1_at_snapshot = freq_state_1_at_snapshot / len(outputs_selected)
        else:
            # making the return zero means that the MI calculation will also return 0, which is what
            # should happen. Basically this function is to return p(B|A) while p(A)=0.
            freq_state_1_at_snapshot = np.multiply(output_per_file[0], 0)
        return freq_state_1_at_snapshot  # matrix, (time, nodeid)

    precomp_from_tuple_to_prob = dict()
    def prob_given_node_has_state(prev_nodeid, prev_state, num_steps_ago, cur_nodeid, cur_state):
        if precomp_from_tuple_to_prob.has_key((prev_nodeid, prev_state, num_steps_ago, cur_nodeid, cur_state)):
            return precomp_from_tuple_to_prob[(prev_nodeid, prev_state, num_steps_ago, cur_nodeid, cur_state)]
        else:
            ret_prob = probs_given_node_has_state(cur_nodeid, cur_state)[num_steps_ago][int(prev_nodeid)] \
                if prev_state == 1 else \
                1 - probs_given_node_has_state(cur_nodeid, cur_state)[num_steps_ago][int(prev_nodeid)]

            # next time don't compute again, hopefully this is faster
            precomp_from_tuple_to_prob[(prev_nodeid, prev_state, num_steps_ago, cur_nodeid, cur_state)] = ret_prob

            return ret_prob

    states = [0, 1]

    def mi_time_delayed(cur_nodeid, prev_nodeid, num_steps_back):
        mi = np.sum([plog2p_divby(prior_prob(int(cur_nodeid), s1)
                       * prob_given_node_has_state(int(prev_nodeid), s2, num_steps_back, int(cur_nodeid), s1),
                      prior_prob(int(cur_nodeid), s1) * prior_prob(int(prev_nodeid), s2, num_steps_back))
                    for s1 in states for s2 in states])

        if not (mi <= 1.0 or len(states) > 2):
            print 'debug: prior_prob(int(cur_nodeid), s1) =', prior_prob(int(cur_nodeid), s1)
            print 'debug: prior_prob(int(prev_nodeid), s2, num_steps_back) =', \
                prior_prob(int(prev_nodeid), s2, num_steps_back)
            print 'debug: prob_given_node_has_state(int(prev_nodeid), s2, num_steps_back, int(cur_nodeid), s1) =', \
                prob_given_node_has_state(int(prev_nodeid), s2, num_steps_back, int(cur_nodeid), s1)
            assert False

        return mi

    numnodes = len(output_per_file[0][0])
    numsteps = len(output_per_file[0])

    if __debug__:
        cur_nodeid = np.random.randint(numnodes)
        prev_nodeid = np.random.randint(numnodes)
        num_steps_back = 1

        prob_prior_summed = np.sum([prior_prob(int(prev_nodeid), s2, num_steps_back) for s2 in states])
        prob_cond_summed = np.sum([prior_prob(int(cur_nodeid), s1)
                       * prob_given_node_has_state(int(prev_nodeid), s2, num_steps_back, int(cur_nodeid), s1)
                    for s1 in states for s2 in states])
        prob_prior_by_summing = lambda s2: np.sum([prior_prob(int(cur_nodeid), s1)
                       * prob_given_node_has_state(int(prev_nodeid), s2, num_steps_back, int(cur_nodeid), s1)
                    for s1 in states])

        np.testing.assert_approx_equal(prob_prior_summed, 1.0)
        np.testing.assert_approx_equal(prob_cond_summed, 1.0)
        np.testing.assert_approx_equal(prob_prior_by_summing(0), prior_prob(int(prev_nodeid), 0, num_steps_back))
        np.testing.assert_approx_equal(prob_prior_by_summing(1), prior_prob(int(prev_nodeid), 1 , num_steps_back))

    idl_per_node = []

    network = read_numbered_network(network_fn)

    # does the number of nodes in this network match the number of nodes used to create the data files, <files_data>?
    assert network.number_of_nodes() == numnodes

    max_mi_per_dist_per_node = []

    for nodeid in xrange(numnodes):
        ### compute maximum MI as function of distance for this nodeid and store it in max_mi_per_dist_per_node

        if verbose:
            print 'note: calc_idl_from_data_files: now processing nodeid=' + str(nodeid)

        # max_mi_per_dist_for_node = {dist: 0.0 for dist in xrange(1, numsteps)}
        max_mi_per_dist_for_node = [0.0]*numsteps  # dict seems not necessary
        max_mi_per_dist_for_node[0] = mi_time_delayed(nodeid, nodeid, 0)  # trivial case, at distance 0

        for dt in xrange(1, numsteps):
            # predecessors = nx.predecessor(network, str(nodeid), cutoff=dt)

            all_predecessors = predecessors_within_distance(network, str(nodeid), max_dist=dt)

            # only used to possibly break this loop, in case the max. observed MI drops below some threshold
            max_mi_at_dt = -1.0

            for pred in all_predecessors:
                if int(pred) == nodeid:
                    continue  # not useful to compute this, has distance 0 anyway which is already the minimum IDL

                # note: this must have a finite distance otherwise how can it be a predecessor? cannot fail
                dist = nx.shortest_path_length(network, source=pred, target=str(nodeid), weight=None)

                if not 0 < dist < numnodes:
                    print 'debug: dist =', dist
                    print 'debug: numnodes =', numnodes
                    print 'debug: pred (source) =', pred
                    print 'debug: nodeid =', nodeid
                    print 'debug: N =', network.number_of_nodes()

                    assert False
                elif not dist <= dt:  # that would be weird, I asked for predecessors with cutoff dt right?
                    print 'debug: dist =', dist
                    print 'debug: numnodes =', numnodes
                    print 'debug: pred (source) =', pred
                    print 'debug: nodeid =', nodeid
                    print 'debug: N =', network.number_of_nodes()

                    assert False

                mi_at_dist = mi_time_delayed(cur_nodeid=nodeid, prev_nodeid=pred, num_steps_back=dt)

                max_mi_per_dist_for_node[dist] = max(max_mi_per_dist_for_node[dist], mi_at_dist)

                max_mi_at_dt = max(max_mi_at_dt, mi_at_dist)

                mi_at_dist = None  # make sure not accidentally reused
                dist = None

            if verbose:
                print 'debug: len(all_predecessors) =', len(all_predecessors), 'for dt =', dt, ' (max_mi_at_dt=' \
                                                                                               + str(max_mi_at_dt) + ')'

            # save potentially a lot of computation (most likely useless, though not provably so) when all MIs seem
            # to be so low that it is not epected anymore that
            if not abs_idl:
                if max_mi_at_dt <= tol_factor * max_mi_per_dist_for_node[0] * threshold_frac_mi:
                    if verbose:
                        print 'note: will stop at dt=' + str(dt) + ' because max_mi_at_dt=' + str(max_mi_at_dt) \
                              + ' which is <= ' + str(tol_factor * max_mi_per_dist_for_node[0] * threshold_frac_mi) \
                              + ' = tol_factor * max_mi_per_dist_for_node[0] * threshold_frac_mi'
                    break
            else:
                if max_mi_at_dt <= tol_factor * threshold_abs_mi:
                    if verbose:
                        print 'note: will stop at dt=' + str(dt) + ' because max_mi_at_dt=' + str(max_mi_at_dt) \
                              + ' which is <= ' + str(tol_factor * threshold_abs_mi) \
                              + ' = tol_factor * threshold_abs_mi'
                    break

        max_mi_per_dist_per_node.append(copy.deepcopy(max_mi_per_dist_for_node))

        if verbose:
            if True:
                print 'note: for nodeid=' + str(nodeid) + ' I find the following max. MI per distance (0, 1, ...):', \
                    [max_mi_per_dist_for_node[dist_print]
                     for dist_print in xrange(min(10, len(max_mi_per_dist_for_node)))]

    ### USE THE MAX(MI) CURVES OF EACH NODE TO COMPUTE AN IDL PER NODE

    # preallocate, will store the IDL per node
    idls = [np.nan for nodeid in xrange(numnodes)]

    mi_list_per_node = [[max_mi_per_dist_per_node[nodeid][dist]  # superfluous? same as max_mi_per_dist_for_node now?
                         for dist in xrange(numsteps)]
                        for nodeid in xrange(numnodes)]

    if not abs_idl:
        # note: reuse the function idt calculation since the idea is very much the same: provide a list of MIs per node,
        # where the MI-list decays to some constant, and get back the characteristic length (or time) of decay
        resp = rel_idts_raw(mi_list_per_node, frac=threshold_frac_mi, allow_extrapolation=allow_extrapolation)

        idls = resp.decay_times
        asympts = resp.asymptotic_values
        max_ats = resp.peak_locations
    else:
        # note: reuse the function idt calculation since the idea is very much the same: provide a list of MIs per node,
        # where the MI-list decays to some constant, and get back the characteristic length (or time) of decay
        idls = abs_idts_raw(mi_list_per_node, epsilon=threshold_abs_mi, allow_extrapolation=allow_extrapolation)
        asympts = None
        max_ats = None

    assert len(idls) == numnodes

    idl_resp = IDLResponse()

    idl_resp.idls = idls
    idl_resp.numnodes = numnodes
    idl_resp.numsteps = numsteps
    idl_resp.max_mi_per_dist_per_node = max_mi_per_dist_per_node
    idl_resp.network = network
    idl_resp.asymptotic_values = asympts
    idl_resp.peak_locations = max_ats
    assert len(max_mi_per_dist_per_node) == len(max_ats)
    if not max_ats is None and not max_mi_per_dist_per_node is None:
        idl_resp.peak_MIs = [max_mi_per_dist_per_node[ni][int(max_ats[ni])] for ni in xrange(len(max_ats))]
    else:
        idl_resp.peak_MIs = None

    return idl_resp


### END OF FUNCTIONS ###

'''
Example:


'''


# noinspection PyUnboundLocalVariable
if __name__ == '__main__':

    def partition ( lst, n ):
        return [ lst[i::n] for i in xrange(n) ]

    # test whether a file name has a temperature value in it, like
    # network_scalefree_N1000_T3.2_param1.6.csv does
    def fn_has_temp(fn):
        return re.search(r'_T[0-9\.]+',fn) != None

    def convert2float(x):
        try:
            ret = float(x)
        except:
            exit('error: convert2float: value '+str(x)+' cannot be converted to float.')
        return ret

    # If true, IDT is the time at which the MI curve hits an absolute (small) value epsilon (absolute).
    # If False, IDT is the time at which the MI curve has reached a % of its own maximum (relative).
    abs_idt = False

    dir_out = './Results/'
    # if it is left '' then the loop below will try to reconstruct the output files names
    # from the network filename
    # file_network = '../Data/network_spinprobs_T11_N80_Y1.6.dat'
    # files_network = [file_network]

    # this option controls whether computing IDTs will be skipped if the corresponding results file
    # already exists. This is useful if calling this script needs to be interrupted and resumed later.
    skip_existing_results = False

    # this will be set by the code itself, don't need to set it yourself
    multiple_network_files = False

    # debugging: whenever a node with degree plot_node_k is encountered, plot its
    # mutual information curve as well as additional information, and quit
    # note: if you set plot_node_k to a value that does not occur as a node's degree
    # (like -2) then debugging information will still be passed to e.g. relidt()
    # so that it can still be used to print to screen and plot in case of error,
    # but otherwise the execution will (seem to) go as normal and output will be as normal.
    # plot_node_k = -1

    ### GET ARGUMENTS TO RUNNING AS SCRIPT ###

    argv = sys.argv[1:]

    procid = 0
    numprocs = 1

    ## PARSE ARGUMENTS

    parser = argparse.ArgumentParser(description="This script takes one or more network files, as well as"
                                                 " corresponding output_spinprobs_*.dat"
                                     " files (output from running IsingOnCN on those network file(s)) "
                                     "and computes the (rel./abs.) IDT values for each node, and stores them in "
                                     "correspondingly named degree_[rel/abs][idt/idl]_* files, among others.")

##    parser.add_argument("--network",'-n', help='single network file', type=str, required=False)
    parser.add_argument('--measure', '--measure_to_compute',
                        help='Choose either "idt" for a temporal measure or "idl" for a spatial measure.',
                        choices=['idt', 'idl'], default='idt', type=str)
    parser.add_argument('--networks','-i','-n', help='network file(s) as pattern with wildcards',
                        nargs='+',type=str, required=True)
    parser.add_argument('--data','-d', help='data file(s) as pattern with wildcards',
                        type=str, nargs='+', required=False, default='')
    parser.add_argument('--absolute','--abs','-a', help='calculate absolute IDT values (MI equals small constant value)',
                        action='store_true',required=False)
    parser.add_argument('--relative','--rel','-r', help='calculate relative IDT values (MI equals fraction of initial value)',
                        action='store_true',required=False)
    parser.add_argument('--path_out','--dir_out','-p', help='directory for storing the output files',
                        type=str,required=False,default='same')
    parser.add_argument('--skip','--skip_existing', help='skip calculation if output file already exists',
                        action='store_true',required=False)
    # parser.add_argument('--plot_node_k', help='debugging; plot results for the first node with this degree and quit',
    #                     type=int,required=False,default=-1)
    parser.add_argument('--procid', help='parallel calculation: an integer in the range [0,numprocs-1]',
                        type=int,required=False,default=0)
    parser.add_argument('--numprocs', help='parallel calculation: number of this same calculations you will start',
                        type=int,required=False,default=1)
    parser.add_argument('--tag', help='tag to use in output file names in case (single) network file name is'
                                      ' not structured', type=str, required=False, default='auto')
    parser.add_argument('--plot', help='show plots', action='store_true',required=False)
    parser.add_argument('--method_calc_idt', '--mci', help='Method for calculating IDT given a number of average '
                                                           'conditional probability arrays: either fit an exponential'
                                                           'curve to each probability array, thus estimating (and '
                                                           'extrapolating) a continuous mutual information curve; or,'
                                                           ' using the cond. probability arrays themselves (raw),'
                                                           ' which is more correct because it does not assume an '
                                                           'exponential decay of probability, but it cannot '
                                                           'extrapolate. Default is raw, I think it is '
                                                           'conceptually better (fewer assumptions)',
                        type=str, choices=('fit','raw'), default='raw')
    parser.add_argument('--save_intermediate_results',
                        help='Directory to store intermediate results such as CSV files and plots of the mutual '
                             'information per node over time. The files will be automatically named, using the'
                             ' prefix --prefix_intermediate_results.', type=str, required=False)
    parser.add_argument('--prefix_intermediate_results', help='Prefix for output filenames for --save_intermediate_'
                                                              'results. Default: "idt_intermediate_".',
                        type=str)
    parser.add_argument('--assume_uniform_entropy', '--assume_equilibrium_entropy',
                        help='In calculating I(s_t : S_0) = H(s_t) - H(s_t : S_0) I take ', action='store_true')

    args = parser.parse_args()

    tagname = args.tag

    ## set parameters

    files_network = args.networks
    if type(files_network) == str:
        files_network = [files_network]
    assert type(files_network) in (list, tuple)
    assert all([type(fnn) == str for fnn in files_network])
    assert not type(args.data) == str
    # print 'debug: len(args.data) =', len(args.data)
    # print 'debug: type(args.data) =', type(args.data)
    if len(args.data) == 1:
        files_data_all = glob.glob(args.data[0])
    else:
        files_data_all = args.data
    if args.absolute == True:
        abs_idt = True
    else:
        abs_idt = False
    dir_out = str(args.path_out)
    print('debug: dir_out set to '+str(dir_out))
    skip_existing_results = args.skip
    # plot_node_k = args.plot_node_k
    procid = args.procid
    numprocs = args.numprocs
    method_calc_idt = args.method_calc_idt

    measure_to_compute = args.measure

    assert measure_to_compute in ('idl', 'idt')

    if not args.prefix_intermediate_results is None:
        prefix_intermediate_results = args.prefix_intermediate_results
    else:
        prefix_intermediate_results = measure_to_compute + '_intermediate_'

    # NOTE: parallelizing the network files has also been implemented. You cannot do both, because then for each network
    # file some temperature values will be missing.
    parallelize_temperatures = (len(files_network) == 1)
    parallelize_networks = not parallelize_temperatures

    if files_network == []:
        exit('no network file name found with given expression ')

    multiple_network_files = len(files_network) > 1

    if files_data_all != [] and multiple_network_files == True:
        print('warning: only use --data with a filename expression for the output files in case'
              ' you specify only one network file. You specified '+str(len(files_network))+' network files.'
              ' I\'m setting the data files expression to \'\' and will continue.')
        files_data_all = []

    threshold_frac_mi = 1.0 / np.e  # 1/e
    threshold_abs_mi = 0.01

    ## END OF PARSING

    assert procid < numprocs
    assert files_network != []

    if (dir_out == 'same'):
        dir_out = op.dirname(files_network[0])

    if (glob.glob(dir_out) == []):
        exit('error: the path of the folder to store the output files'
             ' (--path_out '+str(dir_out)+') does not exist.')

    ##if allowLinearExtrapolationCondEnt:
    ##    warnings.warn('warning: linear extrapolation is currently'
    ##                  'allowed in order to find the IDT of a node.'
    ##                  ' Now, do not trust IDT values > \'numsteps\'.'
    ##                  ' For serious and credible results, set the'
    ##                  ' option allowLinearExtrapolationCondEnt=False'
    ##                  ' in this file.', RuntimeWarning)

    # some nodes in the system state snapshot were e.g. 1 whilst they are usually (>50% of the time)
    # -1, and then the MI curve becomes negative. This is in case you only looked at one system state snapshot,
    # while you should be averaging over many. 
    filterAmbiguousNodes = False

    ### END OF ARGUMENTS PARSING ###

    num_files_network = len(files_network)
    ni = 0
    start_time = time.time()

    if (num_files_network > 1):
        multiple_network_files = True

    if parallelize_networks:
        files_network_perproc = partition(files_network,numprocs)
        my_files_network = files_network_perproc[procid]
    else:
        my_files_network = files_network

    if numprocs > 1 and parallelize_networks:
        print('debug: I will do '+str(len(my_files_network))+' files out of '+str(len(files_network))+'.')
        ix_list = partition(range(len(files_network)),numprocs)
        print('debug: I will do the following network files in the list: '+str(ix_list[procid]))
    else:
        assert my_files_network == files_network

    for file_network in my_files_network:
        if (multiple_network_files or files_data_all == []):
            # use the filename for the network structure, with all its parameter values inside,
            # to construct the name of where the state probabilities are stored, as run_idt*.py does it
            assert(file_network.find("network_") != -1)
            assert(file_network.find("_exp") != -1)
            # network_scalefree_N500_T0.2_param1.5_exp1.dat becomes e.g.:
            # output_spinprobs_scalefree_N500_T0.2_param1.5_run8_exp1.dat
            # TODO: in case the _T<something> is not already in the file name, add
            # it as a wildcard?
            if not fn_has_temp(file_network):
                network_size = extract_size(file_network)
                if re.search(r"network_([a-z+])_N(\d+)_", file_network) == None:
                    print('debug: file_network = '+file_network)
                files_data_expr = re.sub(r"network_([a-z]+)_N(\d+)_",r"output_spinprobs_\1_N\2_T*_",file_network).replace("_exp","_*_exp",1)
            else:
                files_data_expr = file_network.replace("network_","output_*",1).replace("_exp","_*_exp",1)
            assert(files_data_expr != file_network)

            files_data = glob.glob(files_data_expr)
        else:
            files_data = files_data_all

        # get the list of neighbor ids for each node, starting numbering at 0
        # (e.g. len(adjlists[i]) is the degree of node i)
        # note: it is assumed that the node ids are integers, but strings, so '0', '1', and so on
        graph = read_numbered_network(file_network)

        if args.tag == 'auto':
            # construct a descriptive little string for use in the output file names automatically from the filename
            # of the network
            tagname = op.splitext(op.basename(file_network))[0]

        print('debug: network file = '+file_network)
        print('debug: number of nodes in network : '+str(graph.number_of_nodes()))
        print('debug: number of edge sides in network : '+str(graph.number_of_edges()))

##        # for writing results to CSV file later:
##        # construct a file name for the degree,idt pairs of the network from the
##        # filename of the network
##        if (abs_idt):
##            prefix_fn_out = "degree_idt_"
##        else:
##            prefix_fn_out = "degree_relidt_"
##        file_degree_idt = file_network.replace("network_",prefix_fn_out,1)

        # print some progress report
        if (multiple_network_files):
            ni = ni + 1
            print('note: starting IDT calculation #' + str(ni) + ' (' + str(len(my_files_network)) +
                  '). Network file: ' + file_network + '; ' + str(len(files_data))+' output files.')
            if ni == 2 and not skip_existing_results:
                print('note: the entire calculation will take about '
                      + str((time.time()-start_time)*(len(my_files_network)-1)/3600.0) + ' hours')

##        if (skip_existing_results):
##            # if output file exists already and I am supposed to skip its calculation, skip it
##            if (glob.glob(file_degree_idt) != []):
##                print('note: skipping IDT calculation #' + str(ni) + ' (' + str(num_files_network) +
##                  '): output file: ' + file_degree_idt + ' already exists.')
##                continue

        if files_data == []:
            exit('error: I cannot find any output files (--data).')

        # if plot_node_k != -1:
        #     # debugging: store all probability curves for each node
        #     # note: I don't know the correct length yet at this point, so just set it to 500 and resize later
        #     # todo: do this in a nicer way?
        #     all_prob_arrays = [[] for x in xrange(500)]

        files_data_per_temp = dict()
        if not fn_has_temp(op.basename(file_network)):
            for fn in files_data:
                if not fn_has_temp(op.basename(fn)):
                    exit('error: data file fn='+str(fn)+' has no temperature value in it?')
                temp = extract_temp(fn)
                assert np.isscalar(temp)
                files_data_per_temp.setdefault(temp,[]).append(fn)
        else:
            # just store it in one (invalid) key value so that the loop below is still valid
            files_data_per_temp[0] = files_data
            # each output file pertains to the same temperature value?
            assert len(set([extract_temp(fn) for fn in files_data])) == 1

        my_temps_set = files_data_per_temp.keys()

        if (parallelize_temperatures and numprocs > 1):
            temps_perproc = partition(my_temps_set,numprocs)
            my_temps_set = temps_perproc[procid]
            print('debug: (parallel) of the '+str(len(files_data_per_temp.keys()))+' temperature values'
                ' I will do '+str(len(my_temps_set))+': '+str(my_temps_set))

        for temp_output in my_temps_set:
            # for writing results to CSV file later:
            # construct a file name for the degree,idt pairs of the network from the
            # filename of the network
            if (abs_idt):
                prefix_fn_out = "degree_abs" + measure_to_compute + '_'

                prefix_fn_asympts_out = "degree_asympts_"

                prefix_fn_resp_out = 'abs' + measure_to_compute + '_results'
            else:
                prefix_fn_out = "degree_rel" + measure_to_compute + '_'

                prefix_fn_asympts_out = "degree_asympts_"

                prefix_fn_resp_out = 'rel' + measure_to_compute + '_results'

            # in this if-block I try to automatically formulate the output file's name based on param values
            if not fn_has_temp(op.basename(file_network)):
                print('debug: will now process '+str(len(files_data_per_temp[temp_output]))+' output files for T='
                      + str(temp_output))
                # if False:
                #     # TODO: remove this case?
                #     file_degree_idt = re.sub(r"network_([a-z]+)_N(\d+)_",prefix_fn_out+r"\1_N\2_T"+str(temp_output)+"_",file_network)
                # else:
                #     file_degree_idt = prefix_fn_out+''+tagname+'_T'+str(temp_output)+'.csv'
                file_degree_idt = prefix_fn_out+''+tagname+'_T'+str(temp_output)+'.csv'

                file_degree_asympts = prefix_fn_asympts_out+''+tagname+'_T'+str(temp_output)+'.csv'

                file_results_pickle = prefix_fn_resp_out+''+tagname+'_T'+str(temp_output)+'.csv'
            else:
                # if 'network_' in file_network:
                #     file_degree_idt = file_network.replace("network_",prefix_fn_out,1)
                # else:
                #     file_degree_idt = prefix_fn_out + str(tagname) + '_T' + str(temp_output) + '.csv'

                # note: the above replacing 'network' substring is old and could now lead to confusion, so I don't do it
                file_degree_idt = prefix_fn_out + str(tagname) + '_T' + str(temp_output) + '.csv'

                file_degree_asympts = prefix_fn_asympts_out+''+tagname+'_T'+str(temp_output)+'.csv'

                file_results_pickle = prefix_fn_resp_out+''+tagname+'_T'+str(temp_output)+'.csv'

                assert temp_output == 0

            # make absolute path
            file_degree_idt = op.join(dir_out, op.basename(file_degree_idt))
            file_degree_asympts = op.join(dir_out, op.basename(file_degree_asympts))
            file_results_pickle = op.join(dir_out, op.basename(file_results_pickle))

            print('debug: output file name for (deg, idt) pairs will be: '+str(file_degree_idt))
            print('debug: output file name for (deg, asympt) pairs will be: '+str(file_degree_asympts))

            if (skip_existing_results):
                # if output file exists already and I am supposed to skip its calculation, skip it
                if (glob.glob(file_degree_idt) != []):
                    print('note: skipping IDT calculation #' + str(ni) + ' (' + str(num_files_network) +
                      '): output file: ' + file_degree_idt + ' already exists.')
                    continue
            else:
                print('debug: output file name not found. Computing...')

            if measure_to_compute == 'idt':
                idt_resp = calc_idt_from_data_files(files_data_per_temp[temp_output], method=method_calc_idt,
                                                    abs_idt=abs_idt, threshold_frac_mi=threshold_frac_mi,
                                                    threshold_abs_mi=threshold_abs_mi)

                idts = idt_resp.idts
                MI_per_node = idt_resp.MI_per_node
                asympts = idt_resp.asymptotic_values
                max_ats = idt_resp.peak_locations
                max_MIs = idt_resp.peak_MIs
            elif measure_to_compute == 'idl':
                idl_resp = calc_idl_from_data_files(network_fn=file_network,
                                                    files_data=files_data_per_temp[temp_output],
                                                    abs_idl=abs_idt, threshold_frac_mi=threshold_frac_mi,
                                                    threshold_abs_mi=threshold_abs_mi)

                idts = idl_resp.idls
                MI_per_node = idl_resp.max_mi_per_dist_per_node
                asympts = idl_resp.asymptotic_values
                max_ats = idl_resp.peak_locations
                max_MIs = idl_resp.peak_MIs
            else:
                raise NotImplementedError('unknown method: ' + str(measure_to_compute))

            assert len(idts) == len(MI_per_node)  # I expect one scalar value per node in the network
            assert len(max_MIs) == len(idts)
            assert len(max_ats) == len(idts)

            #plt.plot(np.arange(numnodes),np.array(idts))

            numnodes = len(idts)

            print 'debug: caldidt_extrapolate: args.save_intermediate_results =', args.save_intermediate_results
            print 'debug: caldidt_extrapolate: file_degree_idt =', file_degree_idt

            # plot also intermediate results, but do not show them, just store them in files in the directory specified
            # by args.save_intermediate_results.
            # by intermediate results I mean the plots of mutual information over time (or max MI over distance: IDL)
            # for each node
            if args.save_intermediate_results:
                print 'debug: caldidt_extrapolate: flag 000'

                idt_type_str = ('rel' if not abs_idt else 'abs')
                intermediate_dir = str(args.save_intermediate_results)  # directory to save files

                idts_finite = [idt_i for idt_i in idts if np.isscalar(idt_i) and np.isfinite(idt_i)]

                # hmm all idts are NaN or infinity? I guess this could happen e.g. in case a single system state is
                # only allowed to be the snapshot, then at t=0 (the snapshot time) then all entropies are zero at t=0,
                # and I think theoretically all MIs must be zero then, because the system state at t=0 is not capable
                # of storing any information whatsoever (H(system) = 0).
                if len(idts_finite) == 0:
                    print 'debug: len(idts_finite) == 0'
                    if len(idts) > 50:
                        print 'debug: idts = ', idts[:50]
                    else:
                        print 'debug: idts = ', idts
                    # print 'debug: heqs[0] = ', heqs[0]
                    # print 'debug: heqs[-1] = ', heqs[-1]
                    print 'debug: MI_per_node[0] = ', MI_per_node[0]

                    warnings.warn('main(): all IDT values are NaN.')

                print 'calcidt_extrapolate: debug: flag 001'

                # estimate of the time-scale where interesting things still occur in the MI curves
                if len(idts_finite) <= 1:
                    time_scale = len(MI_per_node[0])
                else:
                    time_scale = int(np.percentile(idts_finite, 90.0))
                if time_scale < 10:
                    time_scale = min(10, len(MI_per_node[0]))
                time_scale = min(2 * time_scale, len(MI_per_node[0]))
                time_points = range(time_scale)

                avg_mi_per_timestep_per_node = []

                # todo: plot also the scatter plot of IDT versus degree, with color of each point being the state in the
                # snapshot?
                # todo: plot also one figure per node, with the measured MI and the fitted spline and value of IDT/IDL,
                # so it can be visually verified whether strange things happen?

                ### PLOT the MI per node over time in a single plot

                # looking for bug
                print 'calcidt_extrapolate: debug: flag 111'

                # WARNING: this plot becomes huge for larger network sizes (500) and numsteps (2000), I am running
                # out of memory... So I disable it for now. CHECK: brought it back, dependency, but commented out
                # only plotting commands
                # plt.figure()
                for k in xrange(numnodes):
                    # the y-axis data to plot, list of non-negative scalars
                    if method_calc_idt in ('raw', 'direct'):
                        avg_mi_per_timestep = MI_per_node[k][:time_scale]
                    else:
                        # avg_mi_per_timestep = [avgMI(t,[probparams[fid][i]
                        #                                 for fid in xrange(len(files_data_per_temp[temp_output]))],heqs[i],0)
                        #                        for t in xrange(time_scale)]
                        # figure out exactly how to get the avg MI over time per node, I don;t see the index k in the
                        # 3 lines above this comment... So I commented it out to be sure.
                        assert False
                    # plt.plot(np.transpose(MI_per_node))
                    avg_mi_per_timestep_per_node.append(MI_per_node[k])
                # plt.xlabel('Time steps t since snapshot\n(forward or backward, see run_idt_specified.py)')
                # plt.ylabel('Mutual information I(s_t : S_0)')
                # plt.xlim([0, time_scale])
                mi_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_mi-per-node_' + idt_type_str \
                                         + ext)
                #
                # # looking for bug
                # print 'calcidt_extrapolate: debug: flag 222'
                #
                # # plt.savefig(mi_plot_fn('.pdf'))  # test to see if this causes crash?
                # # plt.savefig(mi_plot_fn('.png'))
                # plt.close()

                # looking for bug
                print 'calcidt_extrapolate: debug: flag 333'

                # save the data in the plot as CSV as well
                fout = open(mi_plot_fn('.csv'), 'wb')
                csvw = csv.writer(fout)
                for row in avg_mi_per_timestep_per_node:
                    csvw.writerow(row)
                fout.close()

                print 'note: wrote mutual information curves per node to', mi_plot_fn('.csv'), \
                    '(plots to .pdf and .png)'

                avg_mi_per_timestep = None  # prevent accidental reuse
                avg_mi_per_timestep_per_node = None  # prevent accidental reuse

                ### SAVE asymptotic MI values
                asympt_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_asympt-per-node_' + idt_type_str \
                                         + ext)
                fas = open(asympt_plot_fn('.csv'), 'wb')
                csvw = csv.writer(fas)
                for i in xrange(len(asympts)):
                    csvw.writerow([i, asympts[i]])
                fas.close()

                print 'note: wrote CSV of asymptotic MIs to', asympt_plot_fn('.csv')

                ### SAVE argmax_t MI(t) values
                maxat_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_peaktime-per-node_' + idt_type_str \
                                         + ext)
                fas = open(maxat_plot_fn('.csv'), 'wb')
                csvw = csv.writer(fas)
                for i in xrange(len(max_ats)):
                    csvw.writerow([i, max_ats[i]])
                fas.close()

                print 'note: wrote CSV of peak locations to', maxat_plot_fn('.csv')

                ### SAVE max_t MI(t) values
                maxmi_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_peakmi-per-node_' + idt_type_str \
                                         + ext)
                fas = open(maxmi_plot_fn('.csv'), 'wb')
                csvw = csv.writer(fas)
                for i in xrange(len(max_MIs)):
                    csvw.writerow([i, max_MIs[i]])
                fas.close()

                print 'note: wrote CSV of peak MIs to', maxmi_plot_fn('.csv')

                ### SCATTER PLOT of idt and asymptotic MIs
                scatter_idts_asympt_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_scatter-idts-asympts_'
                                         + idt_type_str + ext)
                plt.figure()
                plt.plot(idts, asympts, 'o')
                plt.xlabel(measure_to_compute)
                plt.ylabel('Asymptotic MI')
                # plt.savefig(scatter_idts_asympt_plot_fn('.png'))
                plt.savefig(scatter_idts_asympt_plot_fn('.pdf'))

                print 'note: wrote scatter plot (idt, asympt) to', scatter_idts_asympt_plot_fn('.pdf')

                ### SCATTER PLOT of idt and peak locations
                scatter_idts_maxat_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_scatter-idts-maxats_'
                                         + idt_type_str + ext)
                plt.close()

                plt.figure()
                plt.plot(idts, max_ats, 'o')
                plt.xlabel(measure_to_compute)
                plt.ylabel('Peak location')
                # plt.savefig(scatter_idts_maxat_plot_fn('.png'))
                plt.savefig(scatter_idts_maxat_plot_fn('.pdf'))

                print 'note: wrote scatter plot (idt, asympt) to', scatter_idts_asympt_plot_fn('.pdf')

                ### SCATTER PLOT of idt and peak locations
                scatter_idts_maxmi_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_scatter-idts-maxmis_'
                                         + idt_type_str + ext)
                plt.close()

                plt.figure()
                plt.plot(idts, max_MIs, 'o')
                plt.xlabel(measure_to_compute)
                plt.ylabel('Peak location')
                # plt.savefig(scatter_idts_maxmi_plot_fn('.png'))
                plt.savefig(scatter_idts_maxmi_plot_fn('.pdf'))

                print 'note: wrote scatter plot (idt, peak MI) to', scatter_idts_maxmi_plot_fn('.pdf')

                # NOTE: appears to make the program crash maybe? maybe memory overflow? So I commented it out
                # ### PLOT mutual information over time (or distance) for each node separately
                # for k in xrange(numnodes):
                #     plt.figure()
                #
                #     # the y-axis data to plot, list of non-negative scalars
                #     plt.plot(time_points, map(float, MI_per_node[k][:time_scale]))
                #
                #     # put some inferred characteristics in the title so that it can be visually verified
                #     plt.title(measure_to_compute + '=' + str(idts[k]) + ', tm.sc.=' + str(time_scale)
                #               + ', asympt=' + str(asympts[k]) + ',\nmax_at=' + str(max_ats[k]) + ', max_MI='
                #               + str(max_MIs[k]))
                #     plt.xlabel('Time steps t since snapshot\n(forward or backward, see run_idt_specified.py)')
                #     plt.ylabel('Mutual information I(s_t : S_0)')
                #     plt.xlim([0, time_scale])
                #     minmi = np.min(MI_per_node[k][:time_scale])
                #     maxmi = np.max(MI_per_node[k][:time_scale])
                #     if not np.isfinite(minmi):
                #         minmi = 0.0
                #     if not np.isfinite(maxmi):
                #         maxmi = 1.0
                #     dy = 0.05 * (maxmi - minmi)
                #     plt.ylim([max(minmi - dy, 0), maxmi + dy])
                #     mi_plot_node_fn = lambda ext: op.join(intermediate_dir,
                #                              prefix_intermediate_results + tagname + '_mi-for-node' +
                #                              str(k) + '_' + idt_type_str \
                #                              + ext)
                #     plt.savefig(mi_plot_node_fn('.pdf'))
                #     # plt.savefig(mi_plot_node_fn('.png'))
                #     plt.close()
                #
                #     ### ALSO PLOT whole time range
                #
                #     plt.figure()
                #
                #     # the y-axis data to plot, list of non-negative scalars
                #     plt.plot(range(len(MI_per_node[k])), map(float, MI_per_node[k]))
                #
                #     plt.title(measure_to_compute + ' = ' + str(idts
                #     [k]) + ', time scale = ' + str(time_scale)
                #               + '\nnum steps = ' + str(len(MI_per_node[k])))
                #     plt.xlabel('Time steps t since snapshot\n(forward or backward, see run_idt_specified.py)')
                #     plt.ylabel('Mutual information I(s_t : S_0)')
                #     plt.xlim([0, len(MI_per_node[k])])
                #     minmi = np.min(MI_per_node[k][:time_scale])
                #     maxmi = np.max(MI_per_node[k][:time_scale])
                #     if not np.isfinite(minmi):
                #         minmi = 0.0
                #     if not np.isfinite(maxmi):
                #         maxmi = 1.0
                #     dy = 0.05 * (maxmi - minmi)
                #     plt.ylim([max(minmi - dy, 0), maxmi + dy])
                #     all_mi_plot_node_fn = lambda ext: op.join(intermediate_dir,
                #                              prefix_intermediate_results + tagname + '_all-mi-for-node' +
                #                              str(k) + '_' + idt_type_str \
                #                              + ext)
                #     plt.savefig(all_mi_plot_node_fn('.pdf'))
                #     plt.savefig(all_mi_plot_node_fn('.png'))
                #     plt.close()

                print 'calcidt_extrapolate: debug: flag 444'

                ### PLOT histogram of equilibrium entropy per node
                plt.figure()
                plt.hist(map(lambda x: x[0], MI_per_node))
                plt.xlabel('Entropy H(X), at snapshot')
                plt.ylabel('Frequency')
                heq_plot_fn = lambda ext: op.join(intermediate_dir,
                                         prefix_intermediate_results + tagname + '_heq-per-node_' \
                                          + idt_type_str \
                                         + ext)
                plt.savefig(heq_plot_fn('.pdf'))
                plt.savefig(heq_plot_fn('.png'))
                plt.close()

                print 'calcidt_extrapolate: debug: flag 555'

                # save the data in the plot as CSV as well
                fout = open(heq_plot_fn('.csv'), 'wb')
                csvw = csv.writer(fout)
                csvw.writerow(map(lambda x: x[0], MI_per_node))
                fout.close()

                print 'note: wrote eq. entropies per node to', heq_plot_fn('.csv'), '(plots to .pdf and .png)'

                ### SAVE the entropy over time per node
                if measure_to_compute == 'idt':
                    ent_plot_fn = lambda ext: op.join(intermediate_dir,
                                             prefix_intermediate_results + tagname + '_entropy-per-node_' \
                                              + idt_type_str \
                                             + ext)
                    fout = open(ent_plot_fn('.csv'), 'wb')
                    csvw = csv.writer(fout)
                    for entropy_over_time in idt_resp.heqs:
                        csvw.writerow(entropy_over_time)
                    fout.close()

                print 'calcidt_extrapolate: debug: flag 666'

            if args.plot: # if multiple network files I would get too many figures
                plt.figure()
        ##        print('idts = '+str(idts))
        ##        print(np.shape(idts))
                if (len(np.shape(idts))>1):
                    # assume that adjlist makes each node id a string, even though in the file it is an integer
                    plt.scatter([graph.out_degree(str(nodeid)) for nodeid in sorted(map(int, graph.nodes()))],
                                np.array([i[0] if i[0]==i[-1] else -3 for i in idts]))
                else:
                    plt.scatter([graph.out_degree(str(nodeid)) for nodeid in sorted(map(int, graph.nodes()))],
                                np.array([i for i in idts]))
                plt.xlabel('Degree (out)')
                if measure_to_compute == 'idt':
                    plt.ylabel('IDT')
                else:
                    plt.ylabel('IDL')
                plt.title(measure_to_compute + ' for T='+str(temp_output))
                plt.close()

            print('debug: going to write ' + measure_to_compute + ' results to file: '+file_degree_idt)
            
            fidt = open(file_degree_idt,'wb')
            csvwriter = csv.writer(fidt, delimiter=',')
            print('debug: idts[:5] = '+str(idts[:min(5, len(idts))]))
            for i in xrange(numnodes):
                if filterAmbiguousNodes:
                    csvwriter.writerow([graph.degree(str(i)), idts[i][0] if idts[i][-1] == idts[i][0] else -1])
                else:
                    csvwriter.writerow([graph.degree(str(i)), idts[i]])
            fidt.close()

            print('debug: going to write asympt results to file: '+file_degree_asympts)

            fidt = open(file_degree_asympts, 'wb')
            csvwriter = csv.writer(fidt, delimiter=',')
            print('debug: asympts[:5] = '+str(asympts[:min(5, len(asympts))]))
            for i in xrange(numnodes):
                csvwriter.writerow([graph.degree(str(i)), asympts[i]])
            fidt.close()

            fres = open(file_results_pickle, 'wb')
            if measure_to_compute == 'idt':
                # todo: make response uniform, so that both idl and idt return the same object type?
                pickle.dump(idt_resp, fres)
            else:
                pickle.dump(idl_resp, fres)
            fres.close()

            file_notes = op.splitext(file_degree_idt)[0] + '.txt'
            fnotes = open(file_notes, 'w')
            csvwriter = csv.writer(fnotes, delimiter=',')
            csvwriter.writerow(['measure_to_compute', measure_to_compute])
            csvwriter.writerow(['abs_idt', abs_idt])
            csvwriter.writerow(['numnodes', graph.number_of_nodes()])
            csvwriter.writerow(['temperature', str(temp_output)]) # args.save_intermediate_results
            csvwriter.writerow(['save_intermediate_results', args.save_intermediate_results])
            csvwriter.writerow(['file_degree_idt', file_degree_idt])
            csvwriter.writerow(['threshold_frac_mi', threshold_frac_mi])
            csvwriter.writerow(['threshold_abs_mi', threshold_abs_mi])
            csvwriter.writerow(['tagname', tagname])
            csvwriter.writerow(['prefix_intermediate_results', prefix_intermediate_results])
            csvwriter.writerow(['file_network', file_network])
            if len(files_data_all) > 0:
                csvwriter.writerow(['files_data_all (first one only)', files_data_all[0]])
            csvwriter.writerow(['len(files_data_all)', len(files_data_all)])
            fnotes.close()

    if args.plot:
        print('Done. Showing plots...')
        plt.show()
