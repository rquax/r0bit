__author__ = 'rquax'


'''
This file is meant to hold functions used to analyse the multiple output files from run_many.sh, to produce
interesting plots etc.
'''

from idt_criticality import AnalysisResultFlipping, AnalysisResultIDT, SimulationResult
import numpy as np
from scipy.ndimage import gaussian_filter1d
import glob
import cPickle as pickle
import idt_for_network as idt

'''
Example usage:

>> analysis_result_list = am.read_analysis_result_objects('./output_flipping_scalefree_n300_y2.0_exp*.pickle')
>> degrees_of_first_flipped_combined = am.degrees_of_first_flipped_combined(analysis_result_list)
>> average_degree_of_first_flipped = am.average_degree_of_first_flipped(degrees_of_first_flipped_combined)
>> average_degree_of_first_flipped_smoothed = am.average_degree_of_first_flipped_smoothed(degrees_of_first_flipped_combined)
>> plt.plot(sorted(analysis_result_list[0].flipped_to_minority_spin_ids_at_time_since_switch.keys()), average_degree_of_first_flipped, 'x', color='gray'); plt.plot(sorted(analysis_result_list[0].flipped_to_minority_spin_ids_at_time_since_switch.keys()), average_degree_of_first_flipped_smoothed, '-'); plt.xlim(plt.xlim()[0], 0); plt.ylim([2, max(average_degree_of_first_flipped_smoothed) * 1.2]); plt.show()
'''

def read_analysis_result_objects(filename_pattern='./output_flipping_random_n*_p*_exp*.pickle',
                                 make_majority_state_1=True, remove_large_unused_attributes=True):
    """


    :param make_majority_state_1:
    :param remove_large_unused_attributes: remove analysis_result.simulation_result.magnetizations (and *_smoothed,
    and .graph_numbered) which I included for completeness but is rarely used in practice. The reason for deleting
    them is that reading in a 100 of these objects turns out to take too much memory.
    :param filename_pattern:
    :rtype: list of AnalysisResultFlipping
    """
    filenames = glob.glob(filename_pattern)

    fins = [open(fn, 'rb') for fn in filenames]

    # analysis_result_list = [pickle.load(fin) for fin in fins]

    analysis_result_list = []

    for fin in fins:
        analysis_result = pickle.load(fin)

        if remove_large_unused_attributes:
            del analysis_result.simulation_result.magnetizations
            del analysis_result.simulation_result.magnetizations_smoothed
            del analysis_result.simulation_result.graph_numbered

        analysis_result_list.append(analysis_result)

    for fin in fins:
        fin.close()

    # if needed, rename all states +1 to -1 and vice versa, so that the majority state preceding the switch is +1
    # this is helpful for mean_state_at_timestep_per_degree and derived analyses
    if make_majority_state_1:
        for anix in xrange(len(analysis_result_list)):
            analysis_result_list[anix].simulation_result.make_majority_state_1()

    return analysis_result_list


# todo: test, is analysis_result_list passed by reference? (and thus are the objects changed for the caller?)
def make_all_majority_state_1(analysis_result_list):

    # if needed, rename all states +1 to -1 and vice versa, so that the majority state preceding the switch is +1
    # this is helpful for mean_state_at_timestep_per_degree and derived analyses
    for anix in xrange(len(analysis_result_list)):
        analysis_result_list[anix].make_majority_state_1()


def majority_states(analysis_result_list):
    """
    Return the majority state (+1 or -1) for each object in the given list analysis_result_list.
    :param analysis_result_list:
    :rtype: list of int
    """

    return [a.simulation_result.majority_state() for a in analysis_result_list]


def degrees_of_first_flipped(analysis_result, node_properties=None):
    """
    Determine per time step the list of degrees of the nodes that flipped *for the first time*, for one of the
    outputs of run_many.sh. You can repeat this for all output files and combine them to obtain a better result
    of average_degree_of_first_flipped().

    Note: if you have multiple analysis_result objects and you wish to combine the result, use
    degrees_of_first_flipped_combined() instead.
    :param analysis_result: obtained by pickle.load on one of the main output files of run_many.sh
    :type analysis_result: AnalysisResultFlipping
    :param node_properties: a dict which contains an alternative quantity than 'degree' for each node to be used
    :rtype list of list
    """

    min_rt = min(analysis_result.flipped_to_minority_spin_ids_at_time_since_switch.keys())

    # note: assumption: per time-since-switch <rt> there is at most 1 node flipped (if not then do something like
    # np.setdiff1d or so)
    if node_properties is None:
        degrees_flipped_first_time = [analysis_result.degrees_of_flipped_spins_at_time_since_switch[rt]
                                      if not analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt]
                                             in [analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt2]
                                                 for rt2 in xrange(min_rt, rt)]
                                         and len(analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt]) <= 1
                                      else []
                                      for rt in sorted(analysis_result.flipped_to_minority_spin_ids_at_time_since_switch.keys())]

        return degrees_flipped_first_time
    else:
        props_flipped_first_time = [[node_properties[id] for id in analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt]]
                                    if not analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt]
                                           in [analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt2]
                                               for rt2 in xrange(min_rt, rt)]
                                       and len(analysis_result.flipped_to_minority_spin_ids_at_time_since_switch[rt]) <= 1
                                    else []
                                    for rt in sorted(analysis_result.flipped_to_minority_spin_ids_at_time_since_switch.keys())]

        return props_flipped_first_time


# first combine the results of multiple degrees_of_first_flipped() calls, then call this
def average_degree_of_first_flipped(degrees_flipped_first_time):

    avgdeg_flipped_first_time = map(np.mean, degrees_flipped_first_time)

    return avgdeg_flipped_first_time


def degrees_of_first_flipped_combined(analysis_result_list, node_properties=None):

    # degrees_flipped_first_time_list = map(degrees_of_first_flipped, analysis_result_list)
    degrees_flipped_first_time_list = [degrees_of_first_flipped(ar, node_properties=node_properties)
                                       for ar in analysis_result_list]

    # note: looking back at this, this looks weird...
    # check: seems to work, np.sum([[3], [], [2]], axis=0) becomes [3, 2]. Don't know how I thought of this.
    # note: degrees_flipped_first_time_list is a list of lists, where the sublists have length 0 or 1 (float)
    degrees_flipped_first_time_combined = [np.sum([degs[ix] for degs in degrees_flipped_first_time_list], axis=0)
                                           for ix in xrange(len(degrees_flipped_first_time_list[0]))]

    return degrees_flipped_first_time_combined


def average_degree_of_first_flipped_smoothed(degrees_flipped_first_time, sigma='auto'):

    if sigma=='auto':
        sigma = int(len(degrees_flipped_first_time) / 10)

    avgdeg_flipped_first_time = map(np.mean, degrees_flipped_first_time)

    # ignore the NaN
    non_nan_avgdeg_rts = [rt for rt in xrange(len(avgdeg_flipped_first_time))
                          if not np.isnan(avgdeg_flipped_first_time[rt])]
    avgdeg_flipped_first_time_nonnan = [a for a in avgdeg_flipped_first_time if not np.isnan(a)]

    interp_avgdegs = np.interp(range(len(avgdeg_flipped_first_time)), non_nan_avgdeg_rts,
                               avgdeg_flipped_first_time_nonnan)

    avgdeg_flipped_first_time_smoothed = gaussian_filter1d(interp_avgdegs, sigma=sigma)

    return avgdeg_flipped_first_time_smoothed


def average_magnetization_over_time(analysis_result_list, method='abs'):
    """

    :type analysis_result_list: list of AnalysisResultFlipping
    :rtype: np.array
    """

    # number of steps leading up to the switch (on average)
    if method == 'abs':
        return np.mean(np.abs([ar.simulation_result.remember_magnetizations for ar in analysis_result_list]), axis=0)
    else:
        num_preceding_steps = int(len(analysis_result_list[0].simulation_result.remember_magnetizations) / 2)

        # flip the magnetization (multiply by -1.0) if it starts at negative side, then take average
        return np.mean([ar.simulation_result.remember_magnetizations
                        if np.mean(ar.simulation_result.remember_magnetizations[:num_preceding_steps]) > 0.0
                        else np.multiply(-1, ar.simulation_result.remember_magnetizations)
                        for ar in analysis_result_list], axis=0)


class IDTResponseOverTime():

    idt_resp_list = []  # list of IDTForNetworkResponse, one for each

    analysis_result_list = []  # list of AnalysisResultFlipping objects, which was used to compute the above fields
    avgmags = []  # list of float, equal length as idt_resp_list, which is computed by average_magnetization_over_time()

    # reminder:
    # class IDTForNetworkResponse():
    #  deg_idt_pairs_dict = None
    #  deg_asympt_pairs_dict = None
    #  annotated_graph = None

    def plot_degree_idt_for_timestep(self, time_since_switch):
        times_since_switch = list(self.analysis_result_list[0].simulation_result.remember_times)

        time_ix = times_since_switch.index(time_since_switch)

        idt_resp = self.idt_resp_list[time_ix]

        degrees, idts = np.transpose(idt_resp.deg_idt_pairs_dict)

        assert False, 'todo: finish'


def states_at_timestep_per_degree(analysis_result_list, time_since_switch):
    """
    Return a dict which has possible degrees (number of connections) as keys and a list of states as values,
    where this list of states will have length X*N_k where N_k is the number of nodes with degree k, and X
    is the length of analysis_result_list (assuming all objects in analysis_result_list used the exact same network
    structure).
    :type analysis_result_list: list of AnalysisResultFlipping
    :type time_since_switch: int
    """

    degree_states_dict = dict()

    for analysis_result in analysis_result_list:
        degree_states_dict = analysis_result.simulation_result.states_at_timestep_per_degree(time_since_switch,
                                                                                             degree_states_dict)

    return degree_states_dict


def prob_state1_at_timestep_per_degree(analysis_result_list, time_since_switch, degree_states_dict=None):
    """
    Same as states_at_timestep_per_degree but with an extra averaging step for each value in the dict
    :type analysis_result_list: list of AnalysisResultFlipping
    :type time_since_switch: int
    """

    if degree_states_dict is None:
        degree_states_dict = dict()

        # compute the dict
        for analysis_result in analysis_result_list:
            degree_states_dict = analysis_result.simulation_result.states_at_timestep_per_degree(time_since_switch,
                                                                                                 degree_states_dict)
    else:
        assert type(degree_states_dict) == dict

        pass  # use the dict that is given

    for degree in degree_states_dict.iterkeys():
        assert np.ndim(degree_states_dict[degree]) == 1, 'flat array right?'

        degree_states_dict[degree] = list(degree_states_dict[degree]).count(1) / float(len(degree_states_dict[degree]))

    return degree_states_dict


def probs_over_time_per_degree(analysis_result_list):  # untested
    numsteps = len(analysis_result_list[0].simulation_result.remember_times)

    prob_state1_kt = {rt: prob_state1_at_timestep_per_degree(analysis_result_list, rt)
                      for rt in xrange(-numsteps/2, numsteps/2)}

    graph = analysis_result_list[0].simulation_result.graph_numbered

    degrees = sorted(set(graph.degree().values()))

    probs_over_time_per_degree_ret = {degree: [prob_state1_kt[rt][degree]
                                           for rt in xrange(-numsteps/2, numsteps/2)]
                                  for degree in degrees}

    return probs_over_time_per_degree_ret


def distribute_elements(array, nprocs, procid):
    return [array[ix] for ix in range(procid, len(array), nprocs)]


def compute_idts(network_fn, analysis_result_list, ntrials=5000, steps=2000, numruns=200, relative=True,
                 nprocs=1, procid=None):
    """

    :param network_fn:
    :param analysis_result_list:
    :param relative:
    :return: results of IDT calculation for each time step
    """

    if nprocs > 1 and procid is None:
        import thread

        for new_procid in xrange(nprocs):
            thread.start_new_thread(compute_idts, {'network_fn': network_fn,
                                                   'analysis_result_list': analysis_result_list,
                                                   'ntrials': ntrials,
                                                   'steps': steps,
                                                   'numruns': numruns,
                                                   'relative': relative,
                                                   'nprocs': nprocs,
                                                   'procid': new_procid})

            assert False, 'not yet implemented: find a way to recombine the results of the threads' \
                          ' (also this thread can be the procid=0 thread). thread 0 should wait for ' \
                          'all others.'

    avgmags = average_magnetization_over_time(analysis_result_list)
    temp = analysis_result_list[0].simulation_result.T

    size = analysis_result_list[0].simulation_result.graph_numbered.number_of_nodes()

    idt_resp_per_magn = dict()
    idt_resp_list = []

    for t in distribute_elements(xrange(avgmags), nprocs, procid):
        magn = avgmags[t]

        if not idt_resp_per_magn.has_key(magn):
            resp = idt.idt_for_network(network_fn,
                                       temp=temp,
                                       equilsteps=0,  # zero because I sample already from the magnetization constraint
                                       steps=steps,
                                       ntrials=ntrials,
                                       numruns=numruns,
                                       trans='single',
                                       dynamics='glauber',
                                       direction='backward',
                                       verbose=False,
                                       states=None,
                                       save_intermediate_results_idt=True,
                                       intermediate_subfolder=('relidt_' if relative else 'absidt_')
                                                              + 'magn' + str(magn),
                                       sample_snapshots_using_magnetization_constraint=magn,
                                       relative_dissipation=relative,
                                       measure_to_compute='idt')

            idt_resp_per_magn[magn] = resp
        else:
            resp = idt_resp_per_magn[magn]

        idt_resp_list.append(resp)

    ret = IDTResponseOverTime()

    ret.idt_resp_list = idt_resp_list
    ret.analysis_result_list = analysis_result_list
    ret.avgmags = avgmags

    return ret