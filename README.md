# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This alpha stage code repo is meant for easily simulating networks of kinetic Ising spins and calculating information dissipation as well as nudging spins to test their reffect.

### How do I get set up? ###

* Download and store somewhere
* Put its path in the environment variable PYTHONPATH
* do "import ising" in a Jupyter Notebook or 'ipython' session to test that it works.