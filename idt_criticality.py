__author__ = 'rquax'

from mpltools import style
style.use('ggplot')
import csv
import pickle
import argparse
from scipy.ndimage.filters import gaussian_filter1d
import numpy as np
import matplotlib.pyplot as plt
import time


import idt_for_network as idt
import ising


def spin_states_over_time(system, numsteps, transition_rule):
    """
    Provide a dynamical system such as returned by ising.IsingNetwork(graph=network_fn, T=T) and this function will
    evolve the system <numsteps> times, and for each time step it will store the sum of all nde states and return this
    list.
    :param system: dynamical system such as returned by ising.IsingNetwork(graph=network_fn, T=T), must have a
     .states member list and a .next() member function
    :type system: ising.IsingNetwork
    :param numsteps: int
    :return: list of tuples
    """

    #history = [tuple(system.states)]
    # pre-alloc and more efficient:
    history = np.zeros([numsteps+1, system.size], dtype=np.int8)
    history[0] = system.states

    for t in xrange(numsteps):
        system.next(trans=transition_rule)

        history[t+1] = system.states

        # if __debug__:
        #     # I observed some strange jump in magnetization of about -700 ...
        #     if t > 0 and transition_rule == 'single':
        #         assert abs(np.subtract(history[-1], history[-2]).sum()) <= 2, \
        #             'only one spin should flip at a time, but apparently more flipped.\n' \
        #             'old state: ' + str(history[-2]) + '\nnew state: ' + str(history[-1]) \
        #             + '\ndifference in sum of spins: ' + str(np.subtract(history[-1], history[-2]).sum())

    return history


def find_switch_times(array, sigma, direction='both', min_num_contiguous_signs=5):
    """
    For a given 1D array of numerical values this function returns a list of switching times (indices). T is a switch
    time in case array[T] and array[T+1] have different signs. Useful to detect magnetization switches of a spin
    system in which case you pass the magnetization per time step as the array.
    :param min_num_contiguous_signs: a switching time is ignored if on either side of it not at least this many numbers
    have equal sign
    :param array: list of numeric
    :param sigma: float
    :param direction: 'both' or 'up' or 'down'. E.g. 'up' means that sign(array[T]) == -1 and sign(array[T+1]) == +1.
    :return: list of switch times (integers in [0, N-1])
    :raise NotImplementedError: unrecognized <direction>
    """

    magnetizations_smoothed = gaussian_filter1d(np.array(array), sigma=sigma, truncate=2.5)

    switch_times = []

    def sign(num):
        if num < 0.0:
            return -1
        else:
            return 1

    for t in xrange(min_num_contiguous_signs - 1, len(magnetizations_smoothed) - 1):
	if np.abs(magnetizations_smoothed[t]) > 2.0:
		continue  # this time step cannot be a switching point anyway...

        #left_contiguous = bool(len(set(map(sign, magnetizations_smoothed[(t-min_num_contiguous_signs+1):(t+1)]))) == 1)
	left_block = set(np.sign(magnetizations_smoothed[(t-min_num_contiguous_signs+1):(t+1)]))
	left_contiguous = (not -1 in left_block or not 1 in left_block)
	right_block = set(np.sign(magnetizations_smoothed[(t+1):(t+min_num_contiguous_signs+1)]))
        #right_contiguous = bool(len(set(map(sign, magnetizations_smoothed[(t+1):(t+min_num_contiguous_signs+1)]))) == 1)
	right_contiguous = (not -1 in right_block or not 1 in right_block)

        if left_contiguous and right_contiguous:
            if direction == 'both':
                if sign(magnetizations_smoothed[t]) != sign(magnetizations_smoothed[t+1]):
                    switch_times.append(t)
            elif direction == 'up':
                if sign(magnetizations_smoothed[t]) == -1 and sign(magnetizations_smoothed[t+1]) == 1:
                    switch_times.append(t)
            elif direction == 'down':
                if sign(magnetizations_smoothed[t]) == 1 and sign(magnetizations_smoothed[t+1]) == -1:
                    switch_times.append(t)
            else:
                raise NotImplementedError('unknown direction value: ' + str(direction))

    return switch_times


class SimulationResult():

    # temperature parameter used to generate the results (using ising.IsingNetwork)
    T = 1.0
    # network structure used to generate the results (using ising.IsingNetwork)
    graph_numbered = ising.nx.empty_graph()

    # note: these initial values are only so that my IDE gets the types right, and as illustration
    remember_times = []  # contiguous indices into e.g. self.magnetizations, to select only time steps around a switch
    remember_magnetizations = []  # magnetizations around the selected switch
    remember_states = []  # list of list
    remember_numsteps_either_side = []  # how many steps is 'remembered' around the selected switch
    magnetizations = []
    magnetizations_smoothed = []
    switch_times = []
    switch_time = 0
    # todo: rename this to remember_switch_time
    new_switch_time = 0

    def init(self, remember_times, remember_magnetizations, remember_states, remember_numsteps_either_side,
             graph_numbered, magnetizations, magnetizations_smoothed, switch_times, switch_time, new_switch_time):

        self.remember_times = remember_times
        self.remember_magnetizations = remember_magnetizations
        self.remember_states = remember_states
        self.remember_numsteps_either_side = remember_numsteps_either_side
        self.graph_numbered = graph_numbered
        self.magnetizations = magnetizations
        self.magnetizations_smoothed = magnetizations_smoothed
        self.switch_time = switch_time
        self.switch_times = switch_times
        self.new_switch_time = new_switch_time


    def duplicate(self, other):

        self.remember_times = other.remember_times
        self.remember_magnetizations = other.remember_magnetizations
        self.remember_states = other.remember_states
        self.remember_numsteps_either_side = other.remember_numsteps_either_side
        self.graph_numbered = other.graph_numbered
        self.magnetizations = other.magnetizations
        self.magnetizations_smoothed = other.magnetizations_smoothed
        self.switch_time = other.switch_time
        self.switch_times = other.switch_times
        self.new_switch_time = other.new_switch_time


    def majority_state(self):
        """
        :return: +1 or -1, depending on which is the majority state leading up to the switch point
        :rtype: int
        """
        avgmag_before_switch = np.mean(self.remember_magnetizations[:int(len(self.remember_magnetizations)/2)])

        if avgmag_before_switch < 0:
            return -1
        else:
            return 1


    def make_majority_state_1(self):

        majority_state = self.majority_state()

        if majority_state < 0:
            self.remember_magnetizations = list(np.multiply(self.remember_magnetizations, -1))
            self.remember_states = list(np.multiply(self.remember_states, -1))

            # just to keep everything consistent:
            self.magnetizations = list(np.multiply(self.magnetizations, -1))
            self.magnetizations_smoothed = list(np.multiply(self.magnetizations_smoothed, -1))
        else:
            pass  # the pre-switch majority state was already +1, don't have to do anything


    def states_at_timestep_per_degree(self, time_since_switch, degree_states_dict={}):
        """
        Return a dict which has possible degrees (number of connections) as keys and a list of states as values,
        where this list of states will have length N_k where N_k is the number of nodes with degree k.
        :param time_since_switch: int in range
        [-int(len(self.remember_magnetizations)/2), int(len(self.remember_magnetizations)/2)-1]
        :param degree_states_dict: pre-existing dictionary to populate (append), if any (I will copy it first)
        :rtype: dict
        """

        time_ix = time_since_switch + int(len(self.remember_magnetizations)/2)

        assert 0 <= time_ix <= len(self.remember_magnetizations), 'invalid timestep'

        degree_states_dict = degree_states_dict.copy()  # prevent pointer-like mistakes

        for node_id in xrange(self.graph_numbered.number_of_nodes()):
            degree = self.graph_numbered.degree(node_id)

            degree_states_dict.setdefault(degree, []).append(self.remember_states[time_ix][node_id])

        return degree_states_dict


# note: an object of this class pertains to a SimulationResult
class AnalysisResultFlipping():

    simulation_result = SimulationResult()  # initialize just to get type right

    # note: spin ids correspond to node ids in the graph, I think SimulationResult.graph_numbered
    minority_spins_ids_at_time_since_switch = dict()
    flipped_to_minority_spin_ids_at_time_since_switch = dict()
    degrees_of_flipped_spins_at_time_since_switch = dict()


    def __init__(self):
        self.simulation_result = SimulationResult()

        self.minority_spins_ids_at_time_since_switch = dict()
        self.flipped_to_minority_spin_ids_at_time_since_switch = dict()
        self.degrees_of_flipped_spins_at_time_since_switch = dict()


# note: an object of this class pertains to a SimulationResult
class AnalysisResultIDT():

    simulation_result = SimulationResult()  # initialize just to get type right

    # the length of these arrays is N, which is the number of nodes in SimulationResult.graph_numbered, and
    # each i'th element corresponds to node id i.
    idts_list = []
    asympts_list = []


    def __init__(self):
        self.simulation_result = SimulationResult()
        self.idts_list = []
        self.asympts_list = []


if __name__ == "__main__":

    numsteps = 10000
    remember_numsteps = 100
    transition_rule = 'single'
    steps_per_idt_calc = 1
    equilsteps = 0

    size = None
    T = 1.0

    network_fn = None

    parser = argparse.ArgumentParser(description='Compute the IDT of a network of Ising spins'
                                                 ' as function of time around a magnetization'
                                                 ' tipping point (sign change).')

    parser.add_argument('--analysis',
                        help='Analysis to perform.',
                        choices=['idt',
                                 'flips', 'flipping_order', 'flips',
                                 'temp'])
    parser.add_argument('--network', '--network_fn', help='Filename containing a network'
                                                          ' in adjacency list format. '
                                                          '(Readable by networkx.read_adjlist)',
                        required=True, type=str)
    parser.add_argument('--temperature', '-T', '--temp', help='Temperature value, default: ' + str(T),
                        default=T, type=float)
    parser.add_argument('--numsteps', '--totalsteps', help='Total number of steps to simulate '
                                                           'the Ising process on the network '
                                                           'in order to find the magnetization'
                                                           ' switch. Default is ' + str(numsteps),
                        default=numsteps, type=int)
    parser.add_argument('--remember_numsteps', '--remember', help='Number of time steps to remember'
                                                                  ' around the switching point,'
                                                                  ' i.e., also how many to compute'
                                                                  ' IDT for. Default is ' +
                                                                  str(remember_numsteps),
                        default=remember_numsteps, type=int)
    parser.add_argument('--numruns', '--num_snapshots', '--num_snapshots_per_timestep',
                        help='Number of \'snapshot\' system states to generate for a given time-since-switch, which'
                             ' satisfy the corresponding magnetization value at that time. From each snapshot I will'
                             ' sample --ntrials state trajectories in order to compute time-dependent probabilities.'
                             ' Default is 200.',
                        default=200, type=int)
    parser.add_argument('--ntrials', '--num_state_trajectories', '--num_state_trajectories_per_snapshot',
                        help='From each snapshot I will sample this many state trajectories in order to '
                                          'compute time-dependent probabilities. Default is 5000.',
                        default=5000, type=int)
    parser.add_argument('--num_steps_per_trajectory',
                        help='From each state trajectory (starting from the snapshot) I will perform this many'
                             ' simulation steps. Default: 10*N where N is network size.',
                        required=False, type=int)
    parser.add_argument('--sigma', '--sigma_magn', help='The sigma for Gaussian smoothing of'
                                                        ' the magnetization curve over time,'
                                                        ' in order to find the magnetization'
                                                        ' switching point in time. The larger'
                                                        ' this value, the more persistent ('
                                                        'long-term) must'
                                                        ' the switch be.')
    parser.add_argument('--transition_rule', '--transition', '--trans',
                        help='Transition rule to go from one time step to the next. Default'
                             ' is "single", which means only a single spin flip is attempted'
                             ' per time step. An alternative is "async", which means all N'
                             ' spins are attempted to flip exactly once, in random order.'
                             ' "sync" is as "async", but every spin uses the same previous'
                             ' system state to attempt to flip.', type=str,
                        default=transition_rule)
    parser.add_argument('--verbose', '-v', help='Turn on debugging messages and notes.',
                        action='store_true')
    parser.add_argument('--equilsteps', help='Run Ising this many time steps before starting'
                                             ' to search for magnetization switches. '
                                             'Default is ' + str(equilsteps),
                        type=int, default=equilsteps)
    parser.add_argument('--reuse_idts', help='Instead of running the computing the IDTs per time step around'
                                             ' the magnetization switch (time-consuming), reuse'
                                             ' a previous result which was stored in a CSV file.'
                                             ' If you set this then you must also set --reuse_simres.',
                        required=False, type=str)
    parser.add_argument('--reuse_simres', '-reuse_simulation_result', '--reuse_sims',
                        help='Instead of running the Ising process on the given'
                                             ' network to find'
                                             ' the magnetization switch (time-consuming), reuse'
                                             ' a previous result which was stored in a .pickle file.',
                        required=False, type=str)
    parser.add_argument('--normalize', '--norm', '--normalized',
                        help='Normalize the mutual information used in calculating the IDTs. [TODO]',
                        action='store_true')
    parser.add_argument('--save_intermediate_results',
                        help='Flag whether to store intermediate results such as CSV files and plots of the mutual '
                             'information per node over time, while performing the IDT calculations. '
                             'The files will be automatically named, in a subfolder '
                             '(something like "./intermediate_results_idt_*/*") in the current working directory.',
                        action='store_true')
    parser.add_argument('--measure', '--measure_to_compute',
                        help='Choose either "idt" for a temporal measure or "idl" for a spatial measure.',
                        choices=['idt', 'idl'], default='idt', type=str)
    parser.add_argument('--skip_existing_simulation_results', '--reuse_ising',
                        help='NOT SUPPORTED. '
                             'I can tell idt_for_network (which tells IsingOnCN) to reuse a simulation result file'
                             ' in case it already exists. Saves some time, like in case the script crashed or so. '
                             'If all went fine but you want to see the plots again, use --reuse_idts, is faster.'
                             ' If you set this flag then you must also set --reuse_simres, otherwise the output files'
                             ' of the simulations do not match 1-on-1 to the magnetization values. NOT SUPPORTED:'
                             ' currently only the current output folder for the currnt time step is kept, all previous'
                             ' are deleted, so there is no way I can reuse them.',
                        action='store_true')
    parser.add_argument('--output', '--output_analysis_fn', '-o',
                        help='Output file for the analysis, which will be a pickled object. Default is '
                             + './last_switching_analysis_result_<analysis>_T<T>_N<N>.pickle', required=False,
                        type=str)
    parser.add_argument('--output_simres_fn', '--output_simulation_result_fn',
                        help='Output file for the simulation results (spin states over time and magnetization)'
                             ', which will be a pickled object. Default is '
                             + './last_simulation_result_T<T>_N<N>.pickle', required=False,
                        type=str)

    # these defaults are set for N=1000 and <k>=5.
    parser.add_argument('--min_temp',
                        help='(Only for "--analysis temp".)', type=float, default=5.0)
    parser.add_argument('--max_temp',
                        help='(Only for "--analysis temp".)', type=float, default=25.0)
    parser.add_argument('--step_temp',
                        help='(Only for "--analysis temp".)', type=float, default=1.0)

    args = parser.parse_args()

    # if I reuse the IDTs then I must also have the corresponding Ising simulation results,
    # otherwise I cannot plot the correct magnetization curve behind the IDT curve over time,
    # for instance.
    if args.reuse_simres:
        # assert args.reuse_idts  # seems wrong to assert this?
        pass
    else:
        assert not args.reuse_idts

    assert not args.skip_existing_simulation_results

    measure_to_compute = args.measure

    numsteps = args.numsteps
    remember_numsteps = args.remember_numsteps
    remember_numsteps_either_side = int(round(args.remember_numsteps / 2))
    T = args.temperature
    network_fn = args.network
    if args.sigma is None:
        sigma = float(remember_numsteps)
    else:
        sigma = args.sigma
    transition_rule = args.transition_rule

    numruns = args.numruns
    ntrials = args.ntrials

    assert not args.normalize, 'I did not implement this yet, need to think about it...'

    system = ising.IsingNetwork(graph=network_fn, T=T,
                                init_random=False)  # start with all states the same; prevent 'switch' at beginning

    size = system.size

    assert size <= 32000, 'I have some np.sum(., dtype=int16) so they could overflow now...'

    # equilsteps = args.equilsteps
    if args.equilsteps > 0:
        print 'warning: you set --equilsteps but I will set it to zero, because I am not sure if it makes ' \
              'sense otherwise...'
    equilsteps = 0

    if not args.num_steps_per_trajectory is None:
        num_steps_per_trajectory = args.num_steps_per_trajectory
    else:
        num_steps_per_trajectory = max(equilsteps, 10 * size)

    if args.analysis in ('temp', 'temperature'):
        num_switch_times = dict()

        temps = np.linspace(args.min_temp, args.max_temp, (args.max_temp - args.min_temp) / args.step_temp)

        for temp in temps:
            system = ising.IsingNetwork(graph=network_fn, T=temp,
                                        init_random=False)  # start with all states the same; no 'switch' at beginning

            #magnetizations = map(sum, spin_states_over_time(system, numsteps, transition_rule))
            magnetizations = np.sum(spin_states_over_time(system, numsteps, transition_rule), axis=1, dtype=np.int16)

            switch_times = find_switch_times(magnetizations, sigma=sigma, direction='both',
                                             min_num_contiguous_signs=2*size)

            num_switch_times[temp] = len(switch_times)

            print 'note: finished computing temp =', temp, ', len(switch_times) =', len(switch_times)

        if not args.output:
            output_analysis_fn = './last_numswitches_vs_temp_result_' + str(args.analysis) + '_T' + str(T) \
                                 + '_N' + str(size) + '_numsteps' + str(numsteps) + '.pickle'
        else:
            output_analysis_fn = args.output

        plt.plot(temps, [num_switch_times[temp] / float(numsteps) for temp in temps], '-')
        plt.title('network: ' + str(network_fn))
        plt.xlabel('Temperature')
        plt.ylabel('Number of magnetization switches per time step')
        plt.show()
        plt.savefig(output_analysis_fn.replace('.pickle', '.pdf'))

        plt.close()

        # save it as .pickle
        fout = open(output_analysis_fn, 'wb')
        pickle.dump(num_switch_times, fout)
        fout.close()

        print 'note: output saved as ' + str(output_analysis_fn)

        exit(0)

    if not args.output:
        output_analysis_fn = './last_switching_analysis_result_' + str(args.analysis) + '_T' + str(T) \
                             + '_N' + str(size) + '.pickle'
    else:
        output_analysis_fn = args.output

    if not args.reuse_simres:
        history = spin_states_over_time(system, numsteps, transition_rule)

        #magnetizations = map(sum, history)
        magnetizations = np.sum(history, axis=1, dtype=np.int16)

        if __debug__:
            # looking for bug, observed once a large drop in magnetization which should not be possible (only jumps
            # of +2 and -2 should be possible
            if transition_rule == 'single':
                mag_jumps = np.subtract(magnetizations[1:], magnetizations[:-1])

                assert np.max(mag_jumps) == 2
                assert np.min(mag_jumps) == -2

        assert np.ndim(magnetizations) == 1

        switch_times = find_switch_times(magnetizations, sigma=sigma, direction='both', min_num_contiguous_signs=2*size)

        switch_time = None  # one specific switch time to focus on will be selected later

        # use the object to store the partial results already, remember_* stuff will be further filled in later
        simulation_result = SimulationResult()
        simulation_result.magnetizations = magnetizations
        simulation_result.switch_times = switch_times
        simulation_result.switch_time = switch_time
        simulation_result.magnetizations_smoothed = gaussian_filter1d(map(float, magnetizations), sigma=sigma)
        simulation_result.graph_numbered = system.graph_numbered
    else:
        # reuse simulation results and the switching times etc. inferred from them

        fin = open(args.reuse_simres, 'rb')

        simres = pickle.load(fin)

        if not isinstance(simres, SimulationResult):
            (remember_times, remember_magnetizations, remember_states, remember_numsteps_either_side, graph_numbered,
                magnetizations, magnetizations_smoothed, switch_times, switch_time, new_switch_time) = simres
            assert len(remember_times) == len(remember_states)
            assert len(remember_times) == len(remember_magnetizations)

            simulation_result = SimulationResult()

            simulation_result.init(remember_times, remember_magnetizations, remember_states,
                                   remember_numsteps_either_side, graph_numbered, magnetizations,
                                   magnetizations_smoothed, switch_times, switch_time, new_switch_time)
        else:
            simulation_result = SimulationResult()
            simulation_result.duplicate(simres)

        if system.graph_numbered:  # don't understand this check... if it is replaced after anyway
            assert graph_numbered.number_of_nodes() == system.graph_numbered.number_of_nodes()

        system.graph_numbered = simulation_result.graph_numbered

        fin.close()

    simulation_result.T = T

    # remnant of past, maybe incorporate more nicely into output format... But it is useful for diagnosing what
    # is going on
    save_magnetization_over_time = True
    if save_magnetization_over_time:
        plt.figure()
        plt.plot(range(len(magnetizations)), simulation_result.magnetizations, '-k')
        # for plotting only, should match how find_switching_times() smoothes, preferably
        plt.plot(range(len(simulation_result.magnetizations_smoothed)),
                 simulation_result.magnetizations_smoothed, '--', color='0.5')
        plt.title('Magnetization over time,\nand smoothed (dashed)')
        plt.xlabel('Time step (single spin flip attempt)')
        plt.ylabel('Magnetization')
        plt.savefig('last_magnetization_T' + str(T) + '.pdf')
        plt.savefig('last_magnetization_T' + str(T) + '.png')

        # just write all the raw magnetization values per time step to a csv file as single comma-separated row
        fout = open('last_magnetization_T' + str(T) + '.csv', 'wb')
        csvw = csv.writer(fout)
        csvw.writerow(simulation_result.magnetizations)
        fout.close()

        if args.verbose:
            print 'note: magnetization over time has been plotted and stored in last_magnetization.pdf. The values' \
                  ' itself is stored as part of the pickled file, probably something like ' \
                  + str('last_simulation_' + measure_to_compute + '_result.pickle')

    if args.verbose:
        print 'note:', len(switch_times), 'switches have occurred during the simulated time.'

    # note: from this point, only results in simulation_result should be used, not the same fields without
    # "simulation_result." prepended, that is old code... But I am not aware of a way to delete those variable names

    if len(switch_times) > 0:

        ### first select only remember_numsteps steps around the central switch point
        if args.verbose:
            print 'note: that is ' + str(numsteps / len(switch_times)) + ' steps per switch.'

        if not args.reuse_simres:  # was the central switch_time already retrieved from a file with previous results?

            # remove switch times that are too close to the start or end, because I cannot take
            # <remember_numsteps_either_side> steps on either side to study how the switch point was crossed
            switch_times = [sw for sw in switch_times
                            if sw > remember_numsteps_either_side + 1 and sw < numsteps - remember_numsteps_either_side]

            if len(switch_times) in (1, 2):
                switch_time = switch_times[0]
            else:
                switch_time = switch_times[int(round(len(switch_times) / 2))]

            if args.verbose:
                print 'note: I pick', switch_time, 'as the central switch time to continue with.'

            if remember_numsteps:
                # you want to remember only <rememer_numsteps> steps centered around the switch

                remember_times = range(switch_time - remember_numsteps_either_side, switch_time) \
                                 + range(switch_time, switch_time + remember_numsteps_either_side)

                # in this new array of magntizations (<remember_magnetizations> below), the switch occurs at
                # index
                new_switch_time = remember_numsteps_either_side
            else:
                remember_times = range(len(history))

                new_switch_time = switch_time

            remember_magnetizations = [magnetizations[rt] for rt in remember_times]

            assert np.ndim(remember_magnetizations) == 1

            remember_states = [history[rt] for rt in remember_times]

            ### put these additional things also in the simulation_result object

            # note: I exploit init() which re-sets some variables but has the nice property that I cannot forget
            # to set one field
            simulation_result.init(remember_times, remember_magnetizations, remember_states,
                                   remember_numsteps_either_side, system.graph_numbered, magnetizations,
                                   simulation_result.magnetizations_smoothed, switch_times, switch_time,
                                   new_switch_time)
        else:
            pass  # all these quantities were already retrieved from a file given by --reuse_simres

        # save simulation results to file (in case maybe IDT calculations crash or so)
        fout = open('./last_simulation_result_T' + str(T) + '_N' + str(size) + '.pickle', 'wb')
        pickle.dump(simulation_result, fout)
        fout.close()

        ### now do IDT calculations
        if args.analysis in ('idt',):
            if args.verbose:
                print 'note: will now start computing the ' + measure_to_compute + 's per', steps_per_idt_calc, 'steps.'

            # todo: implement doing IDT only once per so many steps, currently not coded
            assert steps_per_idt_calc == 1

            valid_asympts = True  # determines if asympt MIs will be plotted or not, for instance

            if not args.reuse_idts:
                idts_list = []
                asympts_list = []

                before = time.time()  # seconds since epoch

                magnetization_to_result_cache = dict()

                for rt in xrange(len(remember_states)):
                    if args.verbose:
                        print 'note: will calculate ' + args.measure + 's for time=' + \
                              str(rt - remember_numsteps_either_side) + ', has been', \
                            time.time() - before, 'seconds since I started ' + args.measure + 's.'

                    # idt_for_network(network_fn, temp, equilsteps, steps, ntrials, numruns, trans='async', dynamics='glauber',
                    #     direction='backward', skip_existing=True, pathCalcIDT=pathCalcIDT, network_name=None, verbose=True,
                    #     skip_existing_idt=True, pathExec=pathExec, procid=0, numprocs=1, ensure_exp_id=0,
                    #     remove_output=True, run_identifier=None, show_plots=True)

                    magn = remember_magnetizations[rt]

                    if not magnetization_to_result_cache.has_key(magn):
                        resp = idt.idt_for_network(network_fn, temp=T, equilsteps=equilsteps,
                                                    steps=num_steps_per_trajectory,
                                                    ntrials=ntrials, numruns=numruns, trans=transition_rule,
                                                    dynamics='glauber', direction='backward',
                                                    verbose=False,
                                                    skip_existing=args.skip_existing_simulation_results,
                                                    skip_existing_idt=False,
                                                    # states=remember_states[rt],
                                                    states=None,  # since I use magnetization as constraint (magn)
                                                    show_plots=False,
                                                    pathCalcIDT='/home/rquax/PycharmProjects/idt/calcidt_extrapolate.py',
                                                    save_intermediate_results_idt=True,
                                                    intermediate_subfolder='timestep_' +
                                                                           str(rt - remember_numsteps_either_side),
                                                    sample_snapshots_using_magnetization_constraint=magn,
                                                    measure_to_compute=args.measure)

                        deg_idt_pairs_dict = resp.deg_idt_pairs_dict
                        deg_asympt_pairs_dict = resp.deg_asympt_pairs_dict

                        magnetization_to_result_cache[magn] = resp
                    else:
                        if args.verbose:
                            print 'note: I already computed ' + measure_to_compute + ' for M=' + str(magn) + ' before, ' \
                                  + 'and since I assume that the magnetization constraint is the only constraint (no ' \
                                    'additional time-dependence for instance) the results should be pretty much the same,' \
                                    ' assuming you have sufficient --ntrials and --numruns. So I will simply substitute' \
                                    ' the previous (degree, ' + measure_to_compute + ') pairs.'

                        resp = magnetization_to_result_cache[magn]

                        deg_idt_pairs_dict = resp.deg_idt_pairs_dict
                        deg_asympt_pairs_dict = resp.deg_asympt_pairs_dict

                    assert type(deg_idt_pairs_dict) == dict
                    assert len(deg_idt_pairs_dict) == 1
                    assert deg_idt_pairs_dict.keys()[0] == T

                    assert type(deg_asympt_pairs_dict) == dict
                    assert len(deg_asympt_pairs_dict) == 1
                    assert deg_asympt_pairs_dict.keys()[0] == T

                    deg_idt_pairs = deg_idt_pairs_dict[T]
                    deg_asympt_pairs = deg_asympt_pairs_dict[T]

                    assert len(deg_idt_pairs) == size
                    assert len(deg_idt_pairs[0]) == 2

                    # only retain the idt values, forget about the degrees of the nodes, don't care
                    idts = map(lambda x: x[-1], deg_idt_pairs)
                    if not deg_asympt_pairs_dict is None:
                        assert len(deg_idt_pairs) == size
                        assert len(deg_idt_pairs[0]) == 2

                        asympts = map(lambda x: x[-1], deg_asympt_pairs)
                    else:
                        if args.verbose:
                            print 'note: idt_criticality: idt_for_network did not find an output file ' \
                                  'with the asymptotic MI values per' \
                                  ' node. So I will simply set all asymptotic MIs to zero...'

                        asympts = [0]*size

                        valid_asympts = False

                    if args.verbose:
                        print 'debug: rel. time = ' + str(rt) + ': ' + measure_to_compute + 's:', idts

                    assert np.ndim(idts) == 1

                    assert len(deg_idt_pairs) == size
                    assert len(deg_idt_pairs[0]) == 2

                    idts_list.append(idts)
                    asympts_list.append(asympts)

                if args.verbose:
                    print 'note: finished all ' + args.measure + ' calculations after', time.time() - before, \
                        'seconds.'
            else:
                if args.verbose:
                    print 'note: no wait, reusing ' + args.measure + 's...'

                # read idts
                fin = open(args.reuse_idts, 'rb')
                csvr = csv.reader(fin)
                idts_list = [map(float, row) for row in csvr]
                assert len(idts_list) > 0
                assert len(idts_list[0]) == size
                fin.close()

                # read asympts
                try:
                    fin = open(str(args.reuse_idts).replace('_' + measure_to_compute + 's_', '_asympts_', count=1), 'rb')
                    csvr = csv.reader(fin)
                    asympts_list = [map(float, row) for row in csvr]
                    assert len(asympts_list) > 0
                    assert len(asympts_list[0]) == size
                    fin.close()
                except:
                    if args.verbose:
                        print 'note: did not find the file', str(args.reuse_idts).replace('_' + measure_to_compute
                                                                                          + 's_', '_asympts_', 1), \
                            'so I will simply set all asymptotic MIs to zero... (But find out why I cannot find that file.)'
                    asympts_list = [[0]*size]*len(idts_list)

                    valid_asympts = False

                # if this fails, did you do --reuse_idts but not --reuse_simres?
                assert remember_numsteps_either_side == int(round(len(idts_list) / 2))

            analysis_result = AnalysisResultIDT()

            analysis_result.simulation_result = simulation_result  # save also what the results pertain to
            analysis_result.idts_list = idts_list
            analysis_result.asympts_list = asympts_list

            fout = open('./last_switching_analysis_result_' + str(args.analysis)
                        + '_T' + str(T) + '_N' + str(size) + '.pickle', 'wb')
            pickle.dump(analysis_result, fout)
            fout.close()

            ### the two files below are old saves

            # write the IDT results per time step (list of lists) to a CSV file, for
            # later analysis or quickly rerunning the plotting of it with this script
            fout = open('./last_' + args.measure + 's_result.csv', 'wb')
            csvw = csv.writer(fout)
            for idts in idts_list:
                assert len(idts) == size
                assert np.isscalar(idts[0])
                csvw.writerow(idts)
            fout.close()

            # also asympt MIs
            fout = open('./last_asympts_result.csv', 'wb')
            csvw = csv.writer(fout)
            for asympts in asympts_list:
                assert len(asympts) == size
                assert np.isscalar(asympts[0])
                csvw.writerow(asympts)
            fout.close()

            ### end of old saves

            if args.verbose:
                print 'note: will now filter for finite ' + measure_to_compute + ' values and compute average and such...'

            # remove nans and infinities so that I can compute average and stddev
            idts_finite_list = map(lambda idts: [idt for idt in idts if np.isfinite(idt)], idts_list)
            asympts_finite_list = map(lambda asympts: [ami for ami in asympts if np.isfinite(ami)], asympts_list)

            # averages and stddevs for idt
            avg_idts = []
            stddev_idts = []
            stderr_idts = []
            for idts_finite in idts_finite_list:
                try:
                    avg_idt = np.average(idts_finite)
                except:
                    avg_idt = np.nan

                avg_idts.append(avg_idt)

                try:
                    stddev_idt = np.std(idts_finite)
                except:
                    stddev_idt = np.nan

                stddev_idts.append(stddev_idt)

                try:
                    stderr_idt = np.std(idts_finite) / np.sqrt(len(idts_finite))
                except:
                    stderr_idt = np.nan

                stderr_idts.append(stderr_idt)

            # averages and stddevs for asympts
            # avg_idts = map(np.average, idts_finite_list)
            avg_asympts = []
            stddev_asympts = []
            stderr_asympts = []
            for asympts_finite in asympts_finite_list:
                try:
                    avg_asympt = np.average(asympts_finite)
                except:
                    avg_asympt = np.nan

                avg_asympts.append(avg_asympt)

                try:
                    stddev_asympt = np.std(asympts_finite)
                except:
                    stddev_asympt = np.nan

                stddev_asympts.append(stddev_asympt)

                try:
                    stderr_asympt = np.std(asympts_finite) / np.sqrt(len(asympts_finite))
                except:
                    stderr_asympt = np.nan

                stderr_asympts.append(stderr_asympt)

            if args.verbose:
                print 'note: the number of finite ' + measure_to_compute + ' idt values per time step:', \
                    map(len, idts_finite_list)

                print 'note: the averages are:', avg_idts

                print 'note: the number of finite ' + measure_to_compute + ' asymppt values per time step:', \
                    map(len, asympts_finite_list)

                print 'note: the averages are:', avg_asympts

            ### Plot the results for IDT/IDL

            # apparently i need this for plotting on two y-axes
            fig, ax1 = plt.subplots()

            # rescaled time points so that t=0 is the switching point. They correspond to the
            # original time points in 'remember_times'
            xvals = np.arange(-remember_numsteps_either_side, remember_numsteps_either_side, step=1)

            # print 'debug: len(xvals) =', len(xvals)
            # print 'debug: xvals =', xvals
            # print 'debug: avg_idts =', avg_idts
            # print 'debug: stderr_idts =', stderr_idts

            if args.verbose:
                print 'note: will now start to plot.'

            assert len(xvals) == len(idts_list)
            assert len(xvals) == len(idts_finite_list)

            for xi in xrange(len(xvals)):
                ax1.plot([xvals[xi]]*len(idts_finite_list[xi]), idts_finite_list[xi], 'x', color='0.6')


            ax1.errorbar(x=xvals, y=avg_idts, yerr=stderr_idts, label='Average IDT')

            assert len(xvals) == len(remember_magnetizations)

            ax2 = ax1.twinx()

            # print 'debug: remember_magnetizations =', remember_magnetizations

            # overlay with magnetization
            ax2.plot(xvals, remember_magnetizations, '-', color='0.25')
            ax2.plot(xvals, [0.0]*len(xvals), '--', color='0.4')

            ax1.set_xlabel('Time since switch')
            ax1.set_ylabel('Average ' + args.measure + '')
            ax2.set_ylabel('Magnetization')

            ax1.set_xlim([-remember_numsteps_either_side, remember_numsteps_either_side])
            ax2.set_xlim([-remember_numsteps_either_side, remember_numsteps_either_side])
            mag_scale = max(-min(magnetizations), max(magnetizations))
            ax2.set_ylim([-mag_scale, mag_scale])
            idt_scale_max = np.max([avgidt for avgidt in avg_idts if np.isfinite(avgidt)]) \
                            + np.max([se for se in stderr_idts if np.isfinite(se)])
            idt_scale_min = np.min([avgidt for avgidt in avg_idts if np.isfinite(avgidt)]) \
                            - np.max([se for se in stderr_idts if np.isfinite(se)])
            idt_scale_max = idt_scale_max + (idt_scale_max - idt_scale_min) * 0.05
            idt_scale_min = idt_scale_min - (idt_scale_max - idt_scale_min) * 0.05
            ax1.set_ylim([idt_scale_min, idt_scale_max])

            plt.savefig('./last_result_plot.pdf')
            plt.savefig('./last_result_plot.png')

            if args.verbose:
                print 'note: result plot saved to ./last_result_plot.pdf (will show it now)'

            plt.show()

            plt.close()

            ### Plot the results for ASYMPTOTIC MI

            if not asympts_list is None and valid_asympts:
                # apparently i need this for plotting on two y-axes
                fig, ax1 = plt.subplots()

                # rescaled time points so that t=0 is the switching point. They correspond to the
                # original time points in 'remember_times'
                xvals = np.arange(-remember_numsteps_either_side, remember_numsteps_either_side, step=1)

                if args.verbose:
                    print 'note: will now start to plot asymptotic MIs.'

                assert len(xvals) == len(asympts_list)
                assert len(xvals) == len(asympts_finite_list)

                for xi in xrange(len(xvals)):
                    ax1.plot([xvals[xi]]*len(asympts_finite_list[xi]), asympts_finite_list[xi], 'x', color='0.6')

                ax1.errorbar(x=xvals, y=avg_asympts, yerr=stderr_asympts, label='Average asymptotic MI')

                assert len(xvals) == len(remember_magnetizations)

                ax2 = ax1.twinx()

                # print 'debug: remember_magnetizations =', remember_magnetizations

                # overlay with magnetization
                ax2.plot(xvals, remember_magnetizations, '-', color='0.25')
                ax2.plot(xvals, [0.0]*len(xvals), '--', color='0.4')

                ax1.set_xlabel('Time since switch')
                ax1.set_ylabel('Average asympt. MI')
                ax2.set_ylabel('Magnetization')

                ax1.set_xlim([-remember_numsteps_either_side, remember_numsteps_either_side])
                ax2.set_xlim([-remember_numsteps_either_side, remember_numsteps_either_side])
                mag_scale = max(-min(magnetizations), max(magnetizations))
                ax2.set_ylim([-mag_scale, mag_scale])
                asympt_scale_max = np.max([avgasympt for avgasympt in avg_asympts if np.isfinite(avgasympt)]) \
                                   + np.max([se for se in stderr_asympts if np.isfinite(se)])
                asympt_scale_min = np.min([avgasympt for avgasympt in avg_asympts if np.isfinite(avgasympt)]) \
                                   - np.max([se for se in stderr_asympts if np.isfinite(se)])
                asympt_scale_max = asympt_scale_max + (asympt_scale_max - asympt_scale_min) * 0.05
                asympt_scale_min = asympt_scale_min - (asympt_scale_max - asympt_scale_min) * 0.05
                ax1.set_ylim([asympt_scale_min, asympt_scale_max])

                plt.title(str(network_fn) + ' for T=' + str(T))

                plt.savefig('./last_asympts_result_plot.pdf')
                plt.savefig('./last_asympts_result_plot.png')

                if args.verbose:
                    print 'note: result plot saved to ./last_asympts_result_plot.pdf (will show it now)'

                plt.show()
        elif args.analysis in ('flip', 'flips', 'flipping_order'):
            ### use <history> and <magnetizations> to find out which nodes flip at which time-since-switch

            minority_spins_ids_at_time_since_switch = dict()
            flipped_to_minority_spin_ids_at_time_since_switch = dict()
            degrees_of_flipped_spins_at_time_since_switch = dict()

            for rt in xrange(len(remember_states)):
                magn = remember_magnetizations[rt]
                states = remember_states[rt]
                if rt == 0:
                    # note: worst-case magn just happens to already be 0, in which case in the first (few) time steps
                    # there will be no minority spins identified, which seems fair
                    prev_nonzero_mag = magn
                else:
                    if remember_magnetizations[rt - 1] != 0:
                        prev_nonzero_mag = remember_magnetizations[rt - 1]
                    else:
                        pass  # keep whatever is already the previous nonzero magnetization

                time_since_switch = rt - int(len(remember_states) / 2)

                if magn != 0:
                    minorities = [nodeid for nodeid in xrange(size) if states[nodeid] * magn < 0.0]
                else:
                    minorities = [nodeid for nodeid in xrange(size) if states[nodeid] * prev_nonzero_mag < 0.0]

                minority_spins_ids_at_time_since_switch[time_since_switch] = minorities

                if rt == 0:
                    flipped_to_minority_spin_ids_at_time_since_switch[time_since_switch] = \
                        minority_spins_ids_at_time_since_switch[time_since_switch]
                else:
                    # these are minority spins which were not already in minority in previous time step
                    flipped_to_minority_spin_ids_at_time_since_switch[time_since_switch] = \
                        list(np.setdiff1d(minority_spins_ids_at_time_since_switch[time_since_switch],
                                          minority_spins_ids_at_time_since_switch[time_since_switch-1],
                                          assume_unique=True))

                degrees_of_flipped_spins_at_time_since_switch[time_since_switch] = \
                    simulation_result.graph_numbered.degree(flipped_to_minority_spin_ids_at_time_since_switch[time_since_switch]).values()

            analysis_result = AnalysisResultFlipping()

            analysis_result.simulation_result = simulation_result
            analysis_result.minority_spins_ids_at_time_since_switch = minority_spins_ids_at_time_since_switch
            analysis_result.flipped_to_minority_spin_ids_at_time_since_switch = \
                flipped_to_minority_spin_ids_at_time_since_switch
            analysis_result.degrees_of_flipped_spins_at_time_since_switch = \
                degrees_of_flipped_spins_at_time_since_switch

            fout = open(output_analysis_fn, 'wb')
            pickle.dump(analysis_result, fout)
            fout.close()
        else:
            raise NotImplementedError('unknown option: ' + str(args.analysis))
    else:
        print 'note: there were no magnetization switches during the simulated ' \
              'system dynamics. Increase the --temperature, or increase the --numsteps?' \
              ' Anyway, I will exit now.'
        # if __debug__:
        #     print 'debug:', num_state_changes, 'state changes happened during', numsteps, \
        #             'steps.'

        plt.plot(range(len(simulation_result.magnetizations)), simulation_result.magnetizations, '-k')
        plt.plot(range(len(simulation_result.magnetizations_smoothed)),
                 simulation_result.magnetizations_smoothed, '--', color='0.5')
        plt.title('Debugging... Magnetization over time,\nand smoothed (dashed)')
        plt.show()
