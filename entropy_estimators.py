#python2.7
#Written by Greg Ver Steeg
#See readme.pdf for documentation
#Or go to http://www.isi.edu/~gregv/npeet.html

import csv
from scipy.interpolate import InterpolatedUnivariateSpline
import scipy.spatial as ss
from scipy.special import digamma,gamma
from math import log,pi
import numpy.random as nr
import numpy as np
import random
from itertools import combinations
import matplotlib.pyplot as plt
import os.path as op
from scipy.ndimage.filters import gaussian_filter
from astroML.plotting import hist  # for Bayesian blocks: automatic determining of variable-size binning of data

#####CONTINUOUS ESTIMATORS

def entropy(x,k=3,base=2):
    """ The classic K-L k-nearest neighbor continuous entropy estimator
        x should be a list of vectors, e.g. x = [[1.3],[3.7],[5.1],[2.4]]
        if x is a one-dimensional scalar and we have four samples
    """
    assert not k is None, 'makes no sense'
    assert k <= len(x)-1, "Set k smaller than num. samples - 1"
    d = len(x[0])
    N = len(x)
    intens = 1e-10 #small noise to break degeneracy, see doc.
    x = [list(p + intens*nr.rand(len(x[0]))) for p in x]
    tree = ss.cKDTree(x)
    nn = [tree.query(point,k+1,p=float('inf'))[0][k] for point in x]
    const = digamma(N)-digamma(k) + d*log(2)

    # note: I broke this up because I am looking for a bug    
    ret = (const + d*np.mean(map(log,nn)))
    ret /= log(base)

    return ret

def mi(x,y,k=3,base=2):
    """ Mutual information of x and y
        x,y should be a list of vectors, e.g. x = [[1.3],[3.7],[5.1],[2.4]]
        if x is a one-dimensional scalar and we have four samples
    """
    assert len(x)==len(y), "Lists should have same length"
    assert k <= len(x) - 1, "Set k smaller than num. samples - 1"
    intens = 1e-10 #small noise to break degeneracy, see doc.
    x = [list(p + intens*nr.rand(len(x[0]))) for p in x]
    y = [list(p + intens*nr.rand(len(y[0]))) for p in y]
    points = zip2(x,y)
    #Find nearest neighbors in joint space, p=inf means max-norm
    tree = ss.cKDTree(points)
    dvec = [tree.query(point,k+1,p=float('inf'))[0][k] for point in points]
    a,b,c,d = avgdigamma(x,dvec), avgdigamma(y,dvec), digamma(k), digamma(len(x))
    return (-a-b+c+d)/log(base)

def cmi(x,y,z,k=3,base=2):
    """ Mutual information of x and y, conditioned on z
        x,y,z should be a list of vectors, e.g. x = [[1.3],[3.7],[5.1],[2.4]]
        if x is a one-dimensional scalar and we have four samples
    """
    assert len(x)==len(y), "Lists should have same length"
    assert k <= len(x) - 1, "Set k smaller than num. samples - 1"
    intens = 1e-10 #small noise to break degeneracy, see doc.
    x = [list(p + intens*nr.rand(len(x[0]))) for p in x]
    y = [list(p + intens*nr.rand(len(y[0]))) for p in y]
    z = [list(p + intens*nr.rand(len(z[0]))) for p in z]
    points = zip2(x,y,z)
    #Find nearest neighbors in joint space, p=inf means max-norm
    tree = ss.cKDTree(points)
    dvec = [tree.query(point,k+1,p=float('inf'))[0][k] for point in points]
    a,b,c,d = avgdigamma(zip2(x,z),dvec),avgdigamma(zip2(y,z),dvec),avgdigamma(z,dvec), digamma(k)
    return (-a-b+c+d)/log(base)

def kldiv(x,xp,k=3,base=2):
    """ KL Divergence between p and q for x~p(x),xp~q(x)
        x,xp should be a list of vectors, e.g. x = [[1.3],[3.7],[5.1],[2.4]]
        if x is a one-dimensional scalar and we have four samples
    """
    assert k <= len(x) - 1, "Set k smaller than num. samples - 1"
    assert k <= len(xp) - 1, "Set k smaller than num. samples - 1"
    assert len(x[0]) == len(xp[0]), "Two distributions must have same dim."
    d = len(x[0])
    n = len(x)
    m = len(xp)
    const = log(m) - log(n-1)
    tree = ss.cKDTree(x)
    treep = ss.cKDTree(xp)
    nn = [tree.query(point,k+1,p=float('inf'))[0][k] for point in x]
    nnp = [treep.query(point,k,p=float('inf'))[0][k-1] for point in x]
    return (const + d*np.mean(map(log,nnp))-d*np.mean(map(log,nn)))/log(base)

#####DISCRETE ESTIMATORS
def entropyd(sx,base=2,numbins=0, add_tiny_noise=1e-6):
    """ Discrete entropy estimator
        Given a list of samples which can be any hashable object
    """

    if np.ndim(sx) == 1:
        sx = vectorize(sx)

    n1,n2 = np.shape(sx)

    if add_tiny_noise > 0.0:
        sx = sx + sx * np.random.normal(0.0, add_tiny_noise, size=(n1,n2))

    # use however many bins as there are unique data values
    if numbins == 0 or numbins == -1 or numbins in ('block', 'blocks', 'bayesian_blocks',
                                                    'auto_variable_width_binning'):
        return entropyfromprobs(probs_by_bayesian_blocks(sx),base=base)
    elif numbins > 0: # use 'numbins' uniformly sized bins (not so good!)
        assert np.isscalar(numbins)

        probs,_ = np.histogram(sx,numbins)
        ##    print('debug: probs: '+str(probs))
        probs = probs / float(sum(probs))
        ##    print('debug: probs: '+str(probs))
        ##    print('debug: hist: '+str(np.histogram(sx,numbins)))
        return entropyfromprobs(probs, base=base)
    else: # use 'numbins' adaptively sized bins (better!)
        assert np.isscalar(numbins)
        assert numbins < 0

        percs = [[np.percentile(sx[:,i], p) for p in np.linspace(0,100,-numbins+1)] for i in xrange(n2)]
        ##    for i in xrange(n2):
        ##      assert percs[i] == sorted(percs[i]), str('error: np.percentile (#'+str(i)+') did not return a sorted list of values:\n'
        ##                                               +str(percs))
        ##      assert len(percs[i]) == len(set(percs[i])), 'error: entropyd: numbins too large, I get bins of width zero.'
        # remove bins of size zero
        # note: bins of size zero would not contribute to entropy anyway, since 0 log 0 is set
        # to result in 0. For instance, it could be that the data only has 6 unique values occurring,
        # but a numbins=10 is given. Then 4 bins will be empty.
        ##    print('percs: = ' + str(percs))
        percs_fixed = [sorted(list(set(percs_i))) for percs_i in percs]
        for i in xrange(n2):
            assert len(percs_fixed[i]) > 1, 'error: at least one bin should be specified, with left and right boundaries'
        ##    probs = np.divide(np.histogramdd(np.hstack(sx), percs)[0], len(sx))
        probs = np.divide(np.histogramdd(sx, percs_fixed)[0], len(sx))
        probs1d = list(probs.flat)
        ##    print('debug: probs = '+str(probs))
        assert abs(sum(probs1d)-1.0) < 0.001, 'error: entropyd: probabilities not properly normalized.'
        return entropyfromprobs(probs1d, base=base)


def midd(x,y,numbins=0, base=2):
    """ Discrete mutual information estimator
        Given a list of samples which can be any hashable object
    """
    #   if (numbins >= 0):
    #     return -entropyd(np.hstack([x,y]),numbins=numbins)+entropyd(x,numbins=numbins)+entropyd(y,numbins=numbins)
    # ##    return -entropyd(zip(x,y),numbins=numbins)+entropyd(x,numbins=numbins)+entropyd(y,numbins=numbins)
    #   else:
    #     return -entropyd(np.hstack([x,y]),numbins=numbins)+entropyd(x,numbins=numbins)+entropyd(y,numbins=numbins)
    if numbins in ('blocks', 'block', 'bayesian_blocks', 'bayesian', 'auto_variable_widths'):
        return -entropyfromprobs(probs_by_bayesian_blocks_2D(x, y), base=base) \
               + entropyfromprobs(probs_by_bayesian_blocks(x), base=base) \
               + entropyfromprobs(probs_by_bayesian_blocks(y), base=base)
    else:
        assert np.isscalar(numbins)

        if len(x) > 0:
            if np.isscalar(x[0]):
                xvec = vectorize(x)
            else:
                xvec = x

        if len(y) > 0:
            if np.isscalar(y[0]):
                yvec = vectorize(y)
            else:
                yvec = y

        try:
            ret = -entropyd(np.hstack([xvec,yvec]),numbins=numbins) \
                   + entropyd(xvec,numbins=numbins) \
                   + entropyd(yvec,numbins=numbins)
        except IndexError as e:
            print 'error: ', e
            print 'error: h =', numbins

            assert False

        return ret

def cmidd(x,y,z,numbins=0):
    """ Discrete mutual information estimator
        Given a list of samples which can be any hashable object
    """
    return entropyd(zip(y,z),numbins)+entropyd(zip(x,z),numbins)-entropyd(zip(x,y,z),numbins)-entropyd(z,numbins)

def probs_from_max_numbin(sx):
    #Histogram from list of samples
    d = dict()
    for s in sx:
        d[s] = d.get(s,0) + 1
    return map(lambda z:float(z)/len(sx),d.values())

def probs_by_bayesian_blocks(array):
    assert len(array) > 0

    if np.isscalar(array[0]):
        bb_bins = hist(array, bins='blocks')
        assert len(bb_bins[0]) > 0
        bb_probs = bb_bins[0] / np.sum(bb_bins[0])

        assert len(bb_probs) > 0
        assert np.isscalar(bb_probs[0])

        return bb_probs
    else:
        assert np.ndim(array) == 2

        vectors = np.transpose(array)

        bb_bin_edges_per_vec = [hist(v, bins='blocks')[1] for v in vectors]

        if __debug__:
            for bin_edges_i in bb_bin_edges_per_vec:
                assert len(bin_edges_i) > 0
                if not np.isscalar(bin_edges_i[0]):
                    print 'debug: bin_edges =', bin_edges_i
                    raise ValueError

        def find_bin(bin_edges, x):
            assert len(bin_edges) > 0
            assert np.isscalar(bin_edges[0])

            if random.randint(0, 4) == 2:
                assert sorted(list(bin_edges)) == list(bin_edges)

            if x < bin_edges[0]:
                # raise ValueError('value ' + str(x) + ' does not fall in a bin (too low) with bin edges '+str(bin_edges))
                return 0  # just assign it to the lowest (leftmost) bin

            for i in xrange(len(bin_edges)):
                if bin_edges[i] > x:
                    return i - 1

            # print 'debug: x larger than all bins, will return', len(bin_edges) - 1
            assert len(bin_edges) - 2 >= 0
            return len(bin_edges) - 2  # just assign it to the rightmost bin
            # raise ValueError('value ' + str(x) + ' does not fall in a bin with bin edges '+str(bin_edges))

        freqs = np.zeros([len(bin_edges_i) - 1 for bin_edges_i in bb_bin_edges_per_vec])

        for i in xrange(len(array)):
            # if not len(array[i]) == len(bb_bin_edges_per_vec[i]):
            #     print 'debug: bb_bin_edges_per_vec[i] =', bb_bin_edges_per_vec[i]
            #     print 'debug: array[i] =', array[i]
            #     print 'debug: len(bb_bin_edges_per_vec[i]) =', len(bb_bin_edges_per_vec[i])
            #     print 'debug: len(array[i]) =', len(array[i])
            #
            #     raise ValueError

            # bin_ix = map(lambda arg: find_bin(bb_bin_edges_per_vec[i], arg), array[i])
            bin_ix = [find_bin(bb_bin_edges_per_vec[j], array[i][j]) for j in xrange(len(array[i]))]

            if __debug__:
                for ix in bin_ix:
                    assert type(ix) == int
                    assert ix >= 0

            freqs[tuple(bin_ix)] += 1

        assert np.sum(freqs) == len(array)

        freqs_flat = freqs.flatten()
        freqs_flat /= np.sum(freqs)

        return freqs_flat

def probs_by_bayesian_blocks_2D(x, y):
    # assert len(x) > 0
    # assert len(y) == len(x)
    # assert np.isscalar(x[0])
    # assert np.isscalar(y[0])
    #
    # binsx = hist(x, bins='blocks')
    # binsy = hist(y, bins='blocks')
    #
    # assert len(binsx[1]) > 0
    # assert len(binsy[1]) > 0
    #
    # bins2d = plt.hist2d(x, y, bins=[binsx[1], binsy[1]])
    #
    # probs = bins2d[0].flatten()
    # probs = probs / np.sum(probs)
    #
    # assert len(probs) > 0
    # assert np.isscalar(probs[0])
    # assert probs[0] >= 0.0
    #
    # return probs
    assert len(x) > 0
    assert len(y) == len(x)

    if np.isscalar(x):
        xvec = vectorize(x)
    else:
        xvec = x

    if np.isscalar(y):
        xvec = vectorize(y)
    else:
        xvec = y

    x_tr = np.transpose(x)
    y_tr = np.transpose(y)
    xy_tr = np.concatenate([x_tr, y_tr])
    xy = np.transpose(xy_tr)

    assert len(xy) == len(y)
    assert len(xy[0]) == len(x[0]) + len(y[0])

    assert np.ndim(xy) == 2

    ret_probs = probs_by_bayesian_blocks(xy)

    if not abs(np.sum(ret_probs) - 1.0) < 0.001:
        print 'debug: sum =', np.sum(ret_probs)

        assert False

    return ret_probs

def entropyfromprobs(probs,base=2):
    #Turn a normalized list of probabilities of discrete outcomes into entropy (base 2)
    if base == 2:
    	probs_nonzero = np.array(probs)
    	probs_nonzero = probs_nonzero[probs_nonzero > 0.0]
    	return -np.sum(np.multiply(probs_nonzero, np.log2(probs_nonzero)))
    else:
    	return -sum(map(elog,probs))/log(base)

def elog(x):
    #for entropy, 0 log 0 = 0. but we get an error for putting log 0
    if x <= 0. or x>=1.:
        return 0
    else:
        return x*log(x)

#####MIXED ESTIMATORS
def micd(x,y,k=3,base=2,warning=True,ignore_insufficient=False):
    """ If x is continuous and y is discrete, compute mutual information
    """
    overallentropy = entropy(x,k,base)

    n = len(y)
    word_dict = dict()
    for sample in y:
        word_dict[sample] = word_dict.get(sample,0) + 1./n
    yvals = list(set(word_dict.keys()))

    # TODO: this procedure below is basically the uniform binning procedure
    # with the number of bins equal to the number of samples. Implement
    # the adaptive binning here too?

    ignored_prob_mass = 0

    mi = overallentropy
    for yval in yvals:
        xgiveny = [x[i] for i in range(n) if y[i]==yval]
        if k <= len(xgiveny) - 1:
            mi -= word_dict[yval]*entropy(xgiveny,k,base)
        else:
            if warning:
                print "Warning, after conditioning, on y=",yval," insufficient data. Assuming maximal entropy in this case."
            if not ignore_insufficient:
                mi -= word_dict[yval]*overallentropy
            else:
                ignored_prob_mass += word_dict[yval]
                mi -= 0
    if ignore_insufficient:
        assert ignored_prob_mass <= 1.0
        assert ignored_prob_mass >= 0.0
        mi = mi / (1-ignored_prob_mass)
    return mi #units already applied

#####UTILITY FUNCTIONS
def vectorize(scalarlist):
    """ Turn a list of scalars into a list of one-d vectors
    """
    # edit: RQ:
    return np.array(scalarlist).reshape(len(scalarlist),1)
##  return [(x,) for x in scalarlist]

def shuffle_test(measure,x,y,z=False,ns=200,ci=0.95,**kwargs):
    """ Shuffle test
        Repeatedly shuffle the x-values and then estimate measure(x,y,[z]).
        Returns the mean and conf. interval ('ci=0.95' default) over 'ns' runs.
        'measure' could me mi,cmi, e.g. Keyword arguments can be passed.
        Mutual information and CMI should have a mean near zero.
    """
    xp = x[:] #A copy that we can shuffle
    outputs = []
    for i in range(ns):
        random.shuffle(xp)
        if z:
            outputs.append(measure(xp,y,z,**kwargs))
        else:
            outputs.append(measure(xp,y,**kwargs))
    outputs.sort()
    return np.mean(outputs),(outputs[int((1.-ci)/2*ns)],outputs[int((1.+ci)/2*ns)])

### PCA-like analysis

# fn: something like r'E:\Results\ResilienceToNoise\Results_N1000_csv_backup\props-clustering-core-degree-bc-pr-nbrd_relrtn_scalefree_N1000_param1.4_numexps1.csv'
#     as output by merged_*.py
def read_merged_results(fn, yid=-1, maxy=200):
    fin = open(fn,'r')
    csvr = csv.reader(fin, delimiter=',')
    ##  merged_rtn = [map(float,row) for row in csvr if row[yid] <= maxy]
    merged_rtn = [map(float,row) for row in csvr]
    print('debug: read '+str(len(merged_rtn))+' data points from file.')
    merged_rtn = [row for row in merged_rtn if row[yid] <= maxy]
    print('debug: after filtering, '+str(len(merged_rtn))+' data points remain.')
    if not len(np.shape(merged_rtn)) == 2:
        print('debug: np.shape(merged_rtn) = '+str(np.shape(merged_rtn)))
        print('debug: merged_rtn[0] = '+str(merged_rtn[0]))
    assert len(np.shape(merged_rtn)) == 2
    merged_rtn = np.transpose(merged_rtn)

    # strings which specify the name of each [input] variable (column in file, counted from left),
    # gleaned from the filename
    propstrs = np.delete(op.basename(fn).split('-'),[0,len(op.basename(fn).split('-'))-1])

    assert len(propstrs) <= len(merged_rtn)

    fin.close()

    return merged_rtn, propstrs

def plot_pca_xy(matrix, xid, yid=-1, sigma=50, xlabel=None, ylabel='Resilience-to-noise (RTN)', show_yperc=0.95):
    mean_dic=dict()
    for i in xrange(len(matrix[xid])):
        mean_dic.setdefault(matrix[xid][i],[]).append(matrix[yid][i])

    yfit = InterpolatedUnivariateSpline(sorted(mean_dic.keys()), [np.mean(mean_dic[p]) for p in sorted(mean_dic.keys())], k=1)
    plt.plot(matrix[xid],matrix[yid],'o',markerfacecolor=[0.8,0.8,0.8])
    xvals=np.linspace(min(matrix[xid]), max(matrix[xid])+0.01, num=1000)
    plt.plot(xvals,gaussian_filter(yfit(xvals),sigma=sigma),'-k',linewidth=2)
    plt.ylim([0,np.percentile(matrix[yid],show_yperc)*1.2])
    xr = max(matrix[xid]) - min(matrix[xid])
    plt.xlim([min(matrix[xid]) - 0.05*xr,max(matrix[xid]) + 0.05*xr])
    if xlabel == None:
        plt.xlabel('Property #'+str(xid))
    else:
        plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()

# matrix: list of lists, each sublist are datapoints (scalars) for a particular (random) variable.
# xids: integer or list/tuple of integers, indices of variables for X in MI(X,Y)
# yids: integer or list/tuple of integers, indices of variables for Y in MI(X,Y)
# discrete: list of booleans to indicate if a variable's values should be computed using discrete algorithm
def pca_mi(matrix, xids, yids, discrete=None, numbins=-5, k=3, ignore_insufficient=False):
    assert len(np.shape(matrix)) == 2

    if discrete == None:
        discrete = [False]*len(matrix)
    elif discrete == False:
        discrete = [False]*len(matrix)
    elif discrete == True:
        discrete = [True]*len(matrix)
    else:
        assert len(matrix) == len(discrete)

    discrete = np.array(discrete)

    ##  print('debug: discrete = '+str(discrete))
    ##  print('debug: xids = '+str(xids))

    if np.isscalar(xids):
        xids = [xids]
    if np.isscalar(yids):
        yids = [yids]

    xids = list(xids)
    yids = list(yids)

    assert len(np.shape(xids)) == 1
    assert len(np.shape(yids)) == 1

    xvec = np.transpose(matrix[xids])
    yvec = np.transpose(matrix[yids])

    dscx = all(discrete[xids])
    dscy = all(discrete[yids])

    if dscx and dscy:
        return midd(xvec, yvec, numbins)
    elif dscx:
        return micd(yvec, xvec, k=k, ignore_insufficient=ignore_insufficient)
    elif dscy:
        return micd(xvec, yvec, k=k, ignore_insufficient=ignore_insufficient)
    else:
        return mi(xvec, yvec, k=k)

# matrix: list of lists, each sublist are datapoints (scalars) for a particular (random) variable.
# xset: integer or list/tuple of integers, set of all indices of variables which are considered "input" X_i in MI(X_i,Y)
# yids: integer or list/tuple of integers, indices of variables for single "output" Y in MI(X,Y)
# discrete: list of booleans to indicate if a variable's values should be computed using discrete algorithm
def pca_mi_all(matrix, xset, yids, discrete=None, numbins=-5, k=3, ignore_insufficient=False):
    if (np.isscalar(xset)):
        xset = [xset]

    pca_mi_d = dict()
    for numvars in xrange(1,len(xset)+1):

        pca_mi_new = {xids : pca_mi(matrix, xids, yids, discrete=discrete,
                                    numbins=numbins, k=k, ignore_insufficient=ignore_insufficient) for xids in combinations(xset,numvars)}
        pca_mi_d.update(pca_mi_new)

    return pca_mi_d

def pca_mi_all_list(fns, xset, yid, discrete=None, numbins=-5, k=3, ignore_insufficient=False):
    assert np.isscalar(yid)
    pca_mi_d_list = []
    propstrs = []
    fi = 0
    for fn in fns:
        print('debug: processing file #'+str(fi+1)+'...')
        merged_rtn, propstrs = read_merged_results(fn, yid=yid)
        pca_mi_d = pca_mi_all(merged_rtn, xrange(len(propstrs)), yid, discrete=discrete,
                              numbins=numbins, k=k, ignore_insufficient=ignore_insufficient)
        pca_mi_d_list.append(pca_mi_d)
        fi += 1

    return pca_mi_d_list

def make_label(ixs, lblstrs): # internal
    if (lblstrs != []):
        if np.isscalar(ixs):
            return str(lblstrs[ixs])
        else:
            return str([lblstrs[ix] for ix in ixs])
    else:
        return str(ixs)

def showbars_pca_mi(pca_mi_d, matrix, yids, numvars=1, require_xids=[], propstrs=[], relative=True,
                    discrete=None, numbins=-5, k=3, ignore_insufficient=False):
    ##  pca_mi_d = pca_mi_all(matrix, xset, yids, discrete=discrete, numbins=numbins, k=k, ignore_insufficient=ignore_insufficient)

    yvals_list = []
    xvals = []
    xlbls = []

    if type(pca_mi_d) == dict:
        pca_mi_d = [pca_mi_d]

    assert type(pca_mi_d) == list or type(pca_mi_d) == tuple

    for pca_mi_d_i in pca_mi_d:
        bc_yx = [[ki,pca_mi_d_i[ki]] for ki in pca_mi_d_i.keys() if len(ki) == numvars]
        for r in require_xids:
            bc_yx = [x for x in bc_yx if r in x[0]]

        if np.isscalar(yids):
            yids = [yids]

        yids = list(yids)

        if len(bc_yx) == 0:
            print('note: no data points! Too many required indices for input?')

            return
        else:
            bc_x, bc_y = np.transpose([[make_label(pair[0],propstrs),float(pair[1])] for pair in sorted(bc_yx, reverse=True)])
            bc_y=map(float,bc_y)

            Hy = 1.
            if relative:
                Hy = entropy(np.transpose(matrix[yids]),k=k)

            yvals_list.append(np.divide(bc_y,Hy))
            assert len(yvals_list[0]) == len(yvals_list[-1])  # all the same length
            if xvals == []:
                xvals = range(len(bc_x))
                xlbls = bc_x
            else:
                if not str(xlbls) == str(bc_x):
                    print('debug: xvals = '+str(xvals))
                    print('debug: range(len(bc_x)) = '+str(range(len(bc_x))))
                    print('debug: xlbls = '+str(xlbls))
                    print('debug: bc_x =  '+str(bc_x))
                    print('debug: yvals_list = '+str(yvals_list))
                assert xvals == range(len(bc_x))
                assert str(xlbls) == str(bc_x)

    avg_mis = np.mean(yvals_list,0)
    std_mis = np.std(yvals_list,0)
    xs = np.array(xvals)

    temp = np.transpose([avg_mis, xs, std_mis])
    temp2 = sorted(temp, reverse=True, key=lambda x: x[0])
    sorted_res = np.transpose(temp2)
    avg_mis = sorted_res[0]
    xs = sorted_res[1]
    std_mis = sorted_res[2]

    plt.bar(xvals, avg_mis, yerr=np.divide(std_mis,np.sqrt(len(yvals_list))), bottom=0, ecolor=[0,0,0])
    plt.xticks(np.add(xs,0.5),xlbls)
    plt.xlabel('Node property')
    plt.ylabel('Predictability (%)')
    plt.show()

#####INTERNAL FUNCTIONS

def avgdigamma(points,dvec):
    #This part finds number of neighbors in some radius in the marginal space
    #returns expectation value of <psi(nx)>
    N = len(points)
    tree = ss.cKDTree(points)
    avg = 0.
    for i in range(N):
        dist = dvec[i]
        #subtlety, we don't include the boundary point,
        #but we are implicitly adding 1 to kraskov def bc center point is included
        num_points = len(tree.query_ball_point(points[i],dist-1e-15,p=float('inf')))
        avg += digamma(num_points)/N
    return avg

def zip2(*args):
    #zip2(x,y) takes the lists of vectors and makes it a list of vectors in a joint space
    #E.g. zip2([[1],[2],[3]],[[4],[5],[6]]) = [[1,4],[2,5],[3,6]]
    return [sum(sublist,[]) for sublist in zip(*args)]

if __name__ == "__main__":
    print "NPEET: Non-parametric entropy estimation toolbox. See readme.pdf for details on usage."
