
"""

Description: supply this script with a merged results file, in CSV format, where each
row corresponds to a node in a network, and where the last column is a computed result per node,
and all other columns store a particular topological property of each node;
this script will do a sort of PCA analyses to see which of the topological properties determine
best the value of the result, which will typically be the IDT or RTN value for the node.

Author: Rick Quax, UvA

"""

import re
import time
import os.path as op
import os
import entropy_estimators as ee  # for entropy and mutual information functions
import csv
import numpy as np
import argparse
from collections import Counter
import scipy.stats as ss
import matplotlib.pyplot as plt
import pickle


def could_be_merged_filename(fn):
    match = re.match(r'props-[\w\-\d]+_[a-z]+_[\w\-\d]+_T[\d]+\.?\d*\.csv', fn)

    if match:
        return True
    else:
        return False


def extract_temp(merged_fn):
    merged_fn = op.basename(merged_fn) # get filename only
    match = re.search(r'_T([\d]+\.?\d*)',merged_fn)
    assert(match)
    return float(match.group(1))


def extract_propstr(fn):
    fn = op.basename(fn) # get filename only
    m = re.search(r'\A([a-z\-A-Z0-9]+)_',fn) # get first part (word) of the filename
    # I am expecting you supply me with a filename like degree_relidt_scalefree_*.dat
    # (did you provide me with a network_* file?)
    assert(m)
    return m.group(1)


def extract_props(fn):
    propstr = extract_propstr(fn)
    props = propstr.split('-')

    if 'absrtn' in fn:
        props.append('absrtn')
    elif 'relrtn' in fn:
        props.append('relrtn')
    else:
        props.append('(result)')

    return props[1:]


def extract_resultstr(fn):
    fn = op.basename(fn) # get filename only
    m = re.search(r'\A[a-z\-A-Z0-9]+_([a-z]+)_',fn) # get second part (word) of the filename
    # I am expecting you supply me with a filename like degree_relidt_scalefree_*.dat
    # (did you provide me with a network_* file?)
    assert(m)
    return m.group(2)


# divide the list into n equally sized-ish chunks (for parallel computation)
def partition ( lst, n ):
    return [lst[i::n] for i in xrange(n)]


def subsets(elems, length, prefix=[]):
    """
Return all subsets of the list elems which have exactly <length> elements.
    :param elems: list of elements, can be anything
    :param length: length of subsets
    :param prefix: whatever you would want to prefix to the subsets (if unsure, leave empty)
    :return: list
    """
    if length > 1:
        subs = []
        for s in elems:
            rest = elems[elems.index(s)+1:]
            assert len(rest) < len(elems)
            subs.extend(subsets(rest, length-1, prefix=prefix + [s]))
    elif length == 1:
        subs = [prefix + [s] for s in elems]
    else:
        subs = []
        assert False

    return subs

def entropy(ar, base=2):
    cts = np.array(Counter(ar).values())
    cts = cts / float(len(cts))

    return ss.entropy(cts, base=base)


def mutual_information(ar1, ar2, base=2):
    """
    Mutual information between (sets of) discrete variables.
    :param ar1:
    :param ar2:
    :param base:
    :return:
    """

    if np.ndim(ar1) == 1:
        ar1n = [(a,) for a in ar1]
    else:
        ar1n = map(tuple, ar1)

    if np.ndim(ar2) == 1:
        ar2n = [(a,) for a in ar2]
    else:
        ar2n = map(tuple, ar2)

    H_X = entropy(ar1n)
    H_Y = entropy(ar2n)

    H_XY = entropy(zip(map(tuple, ar1), map(tuple, ar2)))

    return H_X + H_Y - H_XY

#note: for history/literature on adaptive binning (h < 0), see bottom part of page 8 of
# http://users.auth.gr/~agpapana/PL/IJBC2009 for instance
def mutual_information_cols(columns, colids1, colids2=(-1,), k=None, h=None, tol=0.2, finite_only=True):
    props = np.transpose([columns[i] for i in colids1])
    res = np.transpose([columns[i] for i in colids2])

    if h == 'discrete':
        mi = mutual_information(props, res)

        return mi
    elif finite_only:
        try:
            finite_ixs = [valid for valid in xrange(len(res)) if all(np.isfinite(res[valid]))]
        except TypeError, e:
            print 'debug: columns =', columns
            print 'debug: res =', res

            assert False

        frac_finite_res = len(finite_ixs) / len(res)

        #if frac_finite_res < 0.5:
        #    print 'warning: mutual_information_cols: more than 50% of result values contained np.inf or np.nan'

        props = [props[pi] for pi in finite_ixs]
        res = [res[ri] for ri in finite_ixs]
    else:
        if __debug__:
            finite_ixs = [valid for valid in xrange(len(res)) if all(np.isfinite(res[valid]))]

            frac_finite_res = len(finite_ixs) / len(res)

    if k is None and h is None:
        #h = -10  # use discrete MI estimation, using |h| bins in histogramming each dimension (auto-resized if h<0)
        k = 3  # use continuous MI estimation using Gaussian kernel estimator

    if not k is None and not h is None:
        raise ValueError('mutual_information_cols: cannot specify both k and h arguments. Specifying k means I will'
                         ' do a k-nearest-neighbors (knn) algorithm; specifying h means I will do an adaptive'
                         ' binning method.')

    if not k is None:
        assert h is None
        mi1 = ee.mi(props, res, k=k)
        mi2 = ee.mi(res, props, k=k)
    elif not h is None:
        assert k is None
        if h == 0:
            h = 'bayesian_blocks'
        mi1 = ee.midd(props, res, numbins=h)
        mi2 = ee.midd(props, res, numbins=h)
    else:
        raise RuntimeError('mutual_information_cols: specify either a k or a h parameter (positive integers)')

    if __debug__:
        if not np.isfinite(mi1) or not np.isfinite(mi2):
            print 'mi1 = ', mi1
            print 'mi2 = ', mi2
            print 'num infinities in props = ', sum([(0 if all(np.isfinite(vals)) else 1) for vals in props])
            print 'num infinities in res = ', sum([(0 if all(np.isfinite(vals)) else 1) for vals in res])
            print 'props = ', [props[i] for i in colids1]
            print 'len(set(props)) = ', len(set(map(tuple, props)))
            print 'set(props) = ', set(map(tuple, props))
            print 'props = ', props
            print 'len(set(res))', len(set(map(tuple, res)))
            # print 'res = ', res
            assert False

    if __debug__ and not tol is None:
        if not abs(mi1 - mi2) / mi1 < tol and frac_finite_res > 0.5: # error of less than 10%?
            print 'warning: error in mutual information calculation larger than tol=', tol*100.0, '%.'
            print 'warning: mi1 = ', mi1
            print 'warning: mi2 = ', mi2
            print 'warning: frac_finite_res = ', frac_finite_res
            print 'warning: error = ', abs(mi1 - mi2) / mi1, ' * 100.0%'
            assert False

    if abs(mi1 - mi2) / mi1 < tol or tol is None:
        return (mi1 + mi2) / 2.0
    else:
        print 'warning: mutual_information_cols: will return NaN; swapping error is ', abs(mi1 - mi2) / mi1 * 100.0, \
              '% (tol=',tol,'). Fraction infinities in results vector: ', frac_finite_res * 100.0,'%'
        return np.nan  # error too big, basically I have no clue what the MI is (too many NaN of infinities in 'res'?)


def pca(columns, numcols, colids_result=(-1,), procid=0, numprocs=1, verbose=True, k=None, h=None, mi_tol=0.2,
        keep_only_top_subsets=None):
    """
    Find the optimal subset of 'numcols' column vectors in terms of MI with the result vector(s).
    :param procid:
    :param numprocs:
    :param verbose:
    :param k:
    :param h:
    :param mi_tol:
    :param keep_only_top_subsets: Specify a positive integer N to let this function keep only the best N subsets (i.e.,
     highest mutual information value with the result column) and forget the rest. Implemented to prevent memory error.
    :param columns: list of lists
    :param numcols: how many columns (variables) should be used to calculate mutual information with the result column?
    :param colids_result: which column(s) stores the response variable(s)?
    :return:
    """

    assert keep_only_top_subsets is None or type(keep_only_top_subsets) == int

    prop_col_ids = range(len(columns))
    for i in colids_result:
        if i < 0:
            i2 = len(columns) + i
        else:
            i2 = i

        assert i2 >= 0
        assert i2 < len(prop_col_ids)
        prop_col_ids.remove(i2)
    subs = subsets(prop_col_ids, numcols)

    if numprocs > 1:
        assert procid < numprocs
        assert procid >= 0

        subs = partition(subs, numprocs)
        assert len(subs) == numprocs
        subs = subs[procid]

        if verbose:
            print 'debug: process #', procid, ' will start PCA for subset size', numcols, '. I have ', len(subs), \
                'subsets for which I have to compute mutual information with the result vector (', colids_result, \
                ')'

    sub_mi_pairs = []

    for subset in subs:
        mi = mutual_information_cols(columns, subset, colids_result, k=k, h=h, tol=mi_tol)

        sub_mi_pairs.append((subset, mi))

        if keep_only_top_subsets:
            if len(sub_mi_pairs) > 2 * keep_only_top_subsets:
                sub_mi_pairs_sorted = sorted(sub_mi_pairs, key=lambda x: x[-1], reverse=True)

                sub_mi_pairs = sub_mi_pairs_sorted[:int(keep_only_top_subsets)]

                assert sub_mi_pairs[0][-1] >= 0
                assert np.isscalar(sub_mi_pairs[0][-1])

    if keep_only_top_subsets:
        sub_mi_pairs_sorted = sorted(sub_mi_pairs, key=lambda x: x[-1], reverse=True)

        if len(sub_mi_pairs_sorted) >= int(keep_only_top_subsets):
            sub_mi_pairs = sub_mi_pairs_sorted[:int(keep_only_top_subsets)]
        else:
            sub_mi_pairs = sub_mi_pairs_sorted

        assert sub_mi_pairs[0][-1] >= 0
        assert np.isscalar(sub_mi_pairs[0][-1])

        return sub_mi_pairs
    else:
        return sorted(sub_mi_pairs, key=lambda x: x[-1], reverse=True)


def pca_list(columns, lengths, colid_res=-1, k=None, h=None):
    pca_dics = []

    for length in lengths:
        assert type(length) == int

        # support also e.g. len=-1, which means "all property columns", so that the command-line call does not
        # change for different merged files.
        if length < 0:
            length = len(columns) + length

        # at least one column (yi) will be the result column, so # of property columns must be less
        assert length < len(columns)

        sub_mi_pairs = pca(columns, numcols=length, colids_result=(colid_res,), k=k, h=h)

        # dic_sub_mi = {tuple(sub): mi for sub, mi in sub_mi_pairs}
        #
        # pca_dics.append(dic_sub_mi)
        pca_dics.append(sorted(sub_mi_pairs, key=lambda x: x[1], reverse=True))

    assert len(pca_dics) == len(lengths)

    return pca_dics


def pca_list_fn(merged_fn, lengths, colid_res=-1, replace_colids=False):
    """

    :param merged_fn:
    :param lengths: List of subset lengths, e.g., [1,2,3] means: first find the principle component (and a top 10 or so)
    , then find the two principle components, and then three. For each subset length a list of pairs of all subsets with
     their MI with the result vector will be returned, sorted descending on the MI (so PC is first).
    :param colid_res:
    :param replace_colids:
    :return:
    """
    columns = read_columns(merged_fn)

    if replace_colids:
        column_names = extract_props(merged_fn)
    else:
        column_names = range(len(columns))

    return pca_matrix(columns, lengths, column_names, colid_res)

def pca_matrix(columns, lengths, column_names, colid_res=-1, verbose=True, procid=0, numprocs=1, k=None, h=None,
               mi_tol=0.2, keep_only_top_subsets=None):
    """
    This function can also easily be used by external scripts to do a PCA analysis using mutual information...
    :param merged_fn:
    :param lengths: List of subset lengths, e.g., [1,2,3] means: first find the principle component (and a top 10 or so)
    , then find the two principle components, and then three. For each subset length a list of pairs of all subsets with
     their MI with the result vector will be returned, sorted descending on the MI (so PC is first).
    :param colid_res: which column id holds the 'result' vector, or 'output' vector
    :return: a list of lists of pairs, one list per length in the 'lengths' argument, each list containing pairs of
    (subset, I[subset, result_column]), sorted descending on the mutual information I[.,.]
    :rtype: list
    """

    pairs_subset_mi_per_length = []

    now = time.time()

    for length in lengths:
        assert type(length) == int

        # support also e.g. len=-1, which means "all property columns", so that the command-line call does not
        # change for different merged files.
        if length < 0:
            length = len(columns) + length

        # at least one column (yi) will be the result column, so # of property columns must be less
        assert length < len(columns)

        sub_mi_pairs = pca(columns, numcols=length, colids_result=(colid_res,), procid=procid, numprocs=numprocs,
                           k=k, h=h, mi_tol=mi_tol, keep_only_top_subsets=keep_only_top_subsets)

        if True:
            sub_mi_pairs = [([column_names[s] for s in sub], mi) for sub, mi in sub_mi_pairs]

        # dic_sub_mi = {tuple(sub): mi for sub, mi in sub_mi_pairs}
        #
        # pca_dics.append(dic_sub_mi)
        pairs_subset_mi_per_length.append(sorted(sub_mi_pairs, key=lambda x: x[1], reverse=True))

        if verbose:
            print 'debug: pca_matrix: finished PCA for subset length', length,', PC is:', \
                pairs_subset_mi_per_length[-1][0]

    assert len(pairs_subset_mi_per_length) == len(lengths)

    return pairs_subset_mi_per_length


def pca_list_per_temp(path, lengths, colid_res=-1, replace_colids=False, verbose=True):
    if not type(path) in [list, tuple, np.array]:
        print 'debug: pca_list_per_temp: assuming you specified to me a path.'
        merged_files = [fn for fn in os.listdir('.') if could_be_merged_filename(fn)]
    else:
        print 'debug: pca_list_per_temp: assuming you specified to me a list of file names (not a path).'
        merged_files = path

    temps = map(extract_temp, merged_files)

    temp_pca_dic = dict()

    for fi in xrange(len(merged_files)):
        merged_fn = merged_files[fi]
        temp = temps[fi]

        if verbose:
            print 'debug: will now compute PCA for file #', fi, ' out of ', len(merged_files), '  -- filename = ', \
                    op.basename(merged_fn)

        pca_list_for_temp = pca_list_fn(merged_fn, lengths, colid_res=colid_res, replace_colids=replace_colids)

        temp_pca_dic[temp] = pca_list_for_temp

    return temp_pca_dic


def read_column_names(merged_fn, delim=',', column_ids=None):
    """
I will try to read the column names from the file, which I do by searching for the first row for which I fail to
convert all values to a float. Otherwise I return None.
    :param merged_fn:
    :param delim:
    :param column_ids:
    :return:
    """
    fin = open(merged_fn, 'rbU')

    if delim == '\t':
        csvr = csv.reader(fin, dialect=csv.excel_tab)
    else:
        csvr = csv.reader(fin, delimiter=delim)
    # data = [map(float, row) for row in csvr]

    column_names = None

    for row in csvr:
        if column_ids:
            row_subset = [row[c] for c in column_ids]
        else:
            row_subset = row

        try:
            row_numbers = map(float, row_subset)
        except ValueError:
            column_names = map(unicode, row_subset)

            break

    fin.close()

    return column_names


def read_columns(merged_fn, delim=',', column_ids=None):
    fin = open(merged_fn, 'rbU')

    if delim == '\t':
        csvr = csv.reader(fin, dialect=csv.excel_tab, delimiter=delim)
    else:
        csvr = csv.reader(fin, delimiter=delim)
    # data = [map(float, row) for row in csvr]

    data = []
    for row in csvr:
        if column_ids:
            try:
                row_subset = [row[c] for c in column_ids]
            except IndexError:
                print 'debug: column_ids =', column_ids
                print 'debug: length of row:', len(row)
                print 'debug: row =', row
                print 'debug: did you specify the correct --delim D? Tab is \\t, comma is , without quotes'

                assert False
        else:
            row_subset = row

        try:
            row_numbers = map(float, row_subset)

            data.append(row_numbers)
        except ValueError:
            pass

    columns = np.transpose(data)

    fin.close()

    return columns


if __name__ == '__main__':

    merged_fn = './props-pr-core-degree-bc-indegree-nbrd-outout' \
                '-outdegree-nbrd-inin-nbrd-inout-nbrd-outin-nbrd_absrtn_relidt_Y2H-union.csv'
    merged_files = [fn for fn in os.listdir('.') if could_be_merged_filename(fn)]

    parser = argparse.ArgumentParser(description='supply this script with a merged results file, in CSV format, where '
                                                 'each'
                                                 'row corresponds to a node in a network, and where the last column is'
                                                 ' a computed result per node,'
                                                 'and all other columns store a particular topological property of each'
                                                 ' node; this script will do a sort of PCA analyses to see which of the'
                                                 ' topological properties determine best the value of the result, which'
                                                 ' will typically be the IDT or RTN value for the node.')

    parser.add_argument('--files', '--file_mergeds', '--inputs',
                        help='Supply multiple file names (--analysis pca_many).',
                        type=str, required=False, nargs='+')
    parser.add_argument('--file', '--file_merged', '--input', help='A file in CSV format with numeric values. Each row'
                                                                   ' and column should be equally long. The last '
                                                                   'column is interpreted as the \'result\', and all'
                                                                   'other columns as input parameters. I assume that'
                                                                   ' the file will be called something like '
                                                   './props-pr-core-degree-bc-indegree-nbrd-outout-outdegree-nbrd-' \
                                                    'inin-nbrd-inout-nbrd-outin-nbrd_absrtn_relidt_Y2H-union.csv; from'
                                                    'this I will parse the names of the input parameter columns (the'
                                                    'list after "props-") and the output (result) name.', type=str,
                        required=False)
    parser.add_argument('--path', '--folder', '--directory', help='Instead of specifying a fingle filename (--file)'
                                                                      ', specify a directory containing multiple merged'
                                                                      ' files. Use this with '
                                                                      '--analysis pca_many; it is ignored with others.'
                                                                      ' Currently the file names MUST contain a temper'
                                                                      'ature value, like "_T8.5".',
                        required=False, type=str, nargs='+', default='.')

    parser.add_argument('--params', '--param', '--prop', '--props', '--property', '--properties',
                        help='Supply me with two identifiers (space separated) of columns in the file (--file). They'
                             ' can either be integer (0,...,N-1; also negative to count from the end, like -1) or'
                             ' a string, where the column names are identified in the file name (like in a filename'
                             ' ./props-pr-core-degree-bc-..., column 0 is named \'pr\', column 1 is named \'core\','
                             ' etc.).', nargs="+", required=False, default=[1, -1], type=str)

    parser.add_argument('--analysis', '--algorithm', help='The analysis to perform', type=str, choices=['plot',
                                                    'mutual_information', 'mi', 'pca', 'pca_many'],
                        required=True)
    parser.add_argument('--size_subsets', '--len', '--sizes_subsets', '--lens', '--length', '--lengths',
                        help='For PCA analysis: number of columns to consider for '
                                                        'computing mutual information with the result vector (last).',
                        default=[1], nargs='+', type=int)

    parser.add_argument('--procid', help='Process id in the range [0,numprocs)', type=int, default=0)
    parser.add_argument('--numprocs', help='Total number of processes', type=int, default=1)

    args = parser.parse_args()

    # note: the bit of code below for columns and props is not used in case --analysis pca_many. But here I just fill
    # in some file name for the merged_fn anyway, so that the code does not crash. And maybe props is useful after all
    if args.file:
        merged_fn = args.file
    elif args.files:
        assert len(args.files) > 0
        merged_fn = args.files[0]
    elif args.path:
        merged_files = [fn for fn in os.listdir(args.path) if could_be_merged_filename(fn)]
        merged_fn = merged_files[0]
    else:
        assert False  # no clue which files you want to use

    ### load the data

    columns = read_columns(merged_fn)

    # parameters for the estimation of mutual information between (sets of) columns. Both None will be replaced
    # by some default
    k = None
    h = 0  # setting this to 0 would mean: use bayesian_blocks algorithm (automatic choice for # bins, and resized)

    ### load the names of the input characteristics (parameters)
    props = extract_props(merged_fn)

    if not len(props) == len(columns):
        print 'debug: props = ', props
        print 'debug: columns = ', columns
        print 'debug: props = ', props
        assert False

    xi = 0
    yi = -1

    if args.params:
        if len(args.params) >= 1:
            try:
                xi = int(args.params[0])
            except ValueError:
                try:
                    xi = props.index(args.params[0])
                except ValueError:
                    if args.params[0] in ('result', 'idt', 'relidt', 'relrtn', 'absrtn', 'rtn', '(result)'):
                        xi = -1
                    else:
                        print 'warning: I could not figure out what you meant by param1=', args.params[0], \
                                ' so I will just assume param1=0.'
                        xi = 1

        if len(args.params) >= 2:
            try:
                yi = int(args.params[1])
            except ValueError:
                try:
                    yi = props.index(args.params[1])
                except ValueError:
                    if args.params[1] in ('result', 'idt', 'relidt', 'relrtn', 'absrtn', 'rtn', '(result)'):
                        yi = -1
                    else:
                        print 'warning: I could not figure out what you meant by param2=', args.params[1], \
                                ' so I will just assume param2=-1.'
                        yi = -1

    # print 'debug: xi = ', xi, ', yi = ', yi

    ### plot or analyze

    if args.analysis in ('plot',):
        x = columns[xi]
        y = columns[yi]

        plt.plot(x,y,'ok')
        plt.xlabel(props[xi])
        plt.ylabel(props[yi])
        plt.title('Scatter plot for file:\n\''+str(merged_fn)+'\'')

        plt.show()
    elif args.analysis in ('mi', 'mutual_information'):
        x = columns[xi]
        y = columns[yi]

        entx = ee.entropy(ee.vectorize(x))
        enty = ee.entropy(ee.vectorize(y))
        mi = ee.mi(ee.vectorize(x), ee.vectorize(y))
        mi_rev = ee.mi(ee.vectorize(y), ee.vectorize(x))

        print 'result: entropy(', props[xi], ') = ', entx
        print 'result: entropy(', props[yi], ') = ', enty
        print 'result: mutual_information(', props[xi], ', ', props[yi], ') = ', mi
        print 'result: mutual_information(', props[yi], ', ', props[xi], ') = ', mi_rev, '  [checking for consistency]'
    elif args.analysis in ('pca'):
        if len(args.size_subsets) == 1:
            num_print_max = 20
        elif len(args.size_subsets) <= 3:
            num_print_max = 10
        else:
            num_print_max = 4

        show_plots_pca = True  # plot?
        save_pca_data = True  # save mutual information per subset to a file?
        save_pca_fn = './last_pca_result.pickle'

        sub_mi_pairs_per_len = []

        for length in args.size_subsets:
            assert type(length) == int

            # support also e.g. len=-1, which means "all property columns", so that the command-line call does not
            # change for different merged files.
            if length < 0:
                length = len(columns) + length

            # at least one column (yi) will be the result column, so # of property columns must be less
            assert length < len(columns)

            sub_mi_pairs = pca(columns, numcols=length, colids_result=(yi,), k=k, h=h)

            pi = 0
            for sub_mi in sub_mi_pairs:
                pi += 1
                if pi > num_print_max:
                    print 'results: (I will stop printing, exceeding num_print_max=', num_print_max, ')'
                    break

                sub = sub_mi[0]
                mi = sub_mi[1]

                print 'result: for properties\t', [props[ci] for ci in sub], '\tI find mutual information\t=\t', mi

            sub_mi_pairs_per_len.append(sub_mi_pairs)

        if save_pca_data:
            print 'note: will save the results to', save_pca_fn
            fout = open(save_pca_fn, 'wb')
            pickle.dump(sub_mi_pairs_per_len, fout)
            fout.close()

        # show plts
        if show_plots_pca:
            for sub_mi_pairs in sub_mi_pairs_per_len:
                sub_mi_pairs_sorted = sorted(sub_mi_pairs, key=lambda x: x[-1], reverse=True)
                max_num_subsets = 10
                if not max_num_subsets is None:
                    if len(sub_mi_pairs_sorted) > max_num_subsets:
                        sub_mi_pairs_sorted = sub_mi_pairs_sorted[:max_num_subsets]

                lefts = range(len(sub_mi_pairs_sorted))

                plt.figure()

                plt.bar(lefts, map(lambda x: x[-1], sub_mi_pairs_sorted))
                plt.xticks(lefts, map(lambda x: x[0], sub_mi_pairs_sorted))

                plt.xlabel('Subset of network properties')
                plt.ylabel('Mutual information I(subset : IDT)')

                plt.show()

    elif args.analysis in ('pca_many'):
        # assert len(args.size_subsets) == 1  # otherwise not supported yet (todo: make multiple bars in same plot?)

        # NOTE: this only works for IDT, not for RTN, since that is only one file probably, and there is no temperature
        # in the filename

        total_width_bars = 0.4
        bar_width = total_width_bars / len(args.size_subsets)

        if not args.files:
            print 'debug: using --path option to set files.'
            temp_pca_dic = pca_list_per_temp(args.path, args.size_subsets, replace_colids=False)
        else:
            print 'debug: using --files option to set files.'
            temp_pca_dic = pca_list_per_temp(args.files, args.size_subsets, replace_colids=False)

        plt.figure()

        temps = map(float, temp_pca_dic.keys())

        print 'debug: temps = ', temps

        for t in temps:
            assert temp_pca_dic.has_key(t)

        xticks_positions = []
        xticks_labels = []

        li = 0
        for length in args.size_subsets:
            # temp_pca_dic = pca_list_per_temp(args.path, args.size_subsets, replace_colids=False)

            x_offset = li * bar_width
            x_array = np.add(temps, 0 - total_width_bars / 2.0 + x_offset)
            # temp_pca_dic[temp] is a list of lists, one list per length in args.size_subsets
            # temp_pca_dic[temp][li] is a list of pairs (subset-params, mutual_info), ordered descending on the latter
            # temp_pca_dic[temp][li][0] is the 'most explanatory' subset with its MI
            # temp_pca_dic[temp][li][0][-1] is the MI of this subset
            y_array = [float(temp_pca_dic[temp][li][0][-1]) for temp in temps]
            x_subsets = [list(temp_pca_dic[temp][li][0][0]) for temp in temps]

            plt.bar(x_array, y_array, width=bar_width)

            spread_xtick_labels = 0.2  # the higher this value, the more xtick labels will be separated horizontally

            xticks_positions.extend(np.add(x_array, (float(li) - (len(args.size_subsets)-1) / 2.0) *
                                           spread_xtick_labels * bar_width))
            xticks_labels.extend(x_subsets)

            li += 1

        plt.title('PCA analysis of local network properties\nversus '+str(props[-1])+' (predictive power).')
        plt.xlabel('Temperature T')
        plt.ylabel('Mutual information between set of property values\nand the '+str(props[-1])+' values.')

        plt.xticks(xticks_positions, map(str, xticks_labels), rotation=-90)  # todo: more readable xticks

        plt.show()
    else:
        print 'error: unsupported analysis: ', args.analysis
