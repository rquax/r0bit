#!/bin/bash

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell. (Rick: no idea what it does)

size=1000
p=`echo "scale=5; 5.0 / ${size}" | bc`  # choose a p to fit some specified average degree (only if networktype=random)
T=17.0
numsteps=`echo "3000 * 1000" | bc`
#networktype="scalefree"  # can be "random" or "scalefree"
networktype="random"  # can be "random" or "scalefree"
gamma=2.0  # only if networktype=scalefree
sigma=`echo "${size}" | bc`
numexps=1
directed=1
# whether or not a new network structure is generated before each run (used for e.g.
#newnets=0  # todo: implement

while getopts "h?n:t:y:x:s:p:c:" opt; do
    case "$opt" in
    h|\?)
        echo "Just open this file in a text editor and you will see."
        exit 0
        ;;
    n)  size=$OPTARG
        ;;
    t)  T=$OPTARG
        ;;
    y)  gamma=$OPTARG
        ;;
    x)  numexps=$OPTARG
        ;;
    s)  numsteps=$OPTARG
        ;;
    p)  p=$OPTARG
        ;;
    c)  networktype=$OPTARG
        ;;
    d)  directed=$OPTARG
        ;;
    esac
done

remembersteps=`echo "100 * ${size}" | bc`

echo "note: parameters are N=${size}, T=${T}, gamma=${gamma}, numexps=${numexps}, networktype=${networktype}, numsteps=${numsteps}"

# old way of parsing args:
#if [ $# > 1 ]; then
#	numexps=$1
#else
#	echo "note: you did not specify a number (int) of how many experiments to run, so I pick 10."
#	numexps=10
#fi

# generate a network structure, write it to a file
if [ "$networktype" = "random" ]; then
    if [ $directed = 0 ]; then
        networkfile="./${networktype}_N${size}_p${p}.adjlist"

        if ! [ -e "${networkfile}" ]; then
            python -c "import networkx as nx; graph = nx.random_graphs.erdos_renyi_graph(${size}, ${p}); graph_cc = nx.connected_component_subgraphs(graph).next(); digraph = graph_cc.to_directed(); nx.write_adjlist(digraph, \"${networkfile}\")"

            echo "note: generated ONCE an UNDIRECTED network file ${networkfile}. (But still edge edge is implemented as two reciprocal directed edges.) All subsequent loops will operate on this network."
        else
            echo "note: network file $networkfile already exists, so I will not replace it. If you do want a new network then remove it before you call this script."
        fi
    else
        networkfile="./${networktype}_N${size}_p${p}_directed.adjlist"

        if ! [ -e "${networkfile}" ]; then
            python -c "import networkx as nx; digraph = nx.random_graphs.erdos_renyi_graph(${size}, ${p}, directed=True); digraph.subgraph(nx.connected_components(digraph.to_undirected()).next()); nx.write_adjlist(digraph, \"${networkfile}\")"

            echo "note: generated ONCE a DIRECTED network file ${networkfile}. All subsequent loops will operate on this network."
        else
            echo "note: network file $networkfile already exists, so I will not replace it. If you do want a new network then remove it before you call this script."
        fi
    fi
elif [ "$networktype" = "scalefree" ]; then
    if [ $directed = 0 ]; then
        networkfile="./${networktype}_N${size}_y${gamma}.adjlist"

        if ! [ -e "${networkfile}" ]; then
            python -c "import networkx as nx; from ising import generate_powerlaw_network; graph_cc = generate_powerlaw_network(${size}, ${gamma}); digraph = graph_cc.to_directed(); nx.write_adjlist(digraph, \"${networkfile}\")"

            echo "note: generated ONCE an UNDIRECTED network file ${networkfile}. (But still edge edge is implemented as two reciprocal directed edges.) All subsequent loops will operate on this network."
        else
            echo "note: network file $networkfile already exists, so I will not replace it. If you do want a new network then remove it before you call this script."
        fi
    else
        networkfile="./${networktype}_N${size}_y${gamma}_directed.adjlist"

        if ! [ -e "${networkfile}" ]; then
            python -c "import networkx as nx; from ising import generate_directed_powerlaw_network; digraph = generate_directed_powerlaw_network(${size}, ${gamma}); nx.write_adjlist(digraph, \"${networkfile}\")"

            echo "note: generated ONCE a DIRECTED network file ${networkfile}. All subsequent loops will operate on this network."
        else
            echo "note: network file $networkfile already exists, so I will not replace it. If you do want a new network then remove it before you call this script."
        fi
    fi
else
    exit "unrecognized network type"
fi

sleep 0.5  # give the network file some time to appear in the file system... just to be sure

if [ -e "${networkfile}" ]; then
    timeBefore=`date +%s`

    for i in `seq 1 $numexps`; do
        # file where an object of class AnalysisResultFlipping will be pickled (stored)
        if [ "$networktype" = "random" ]; then
            outputfile="./output_flipping_${networktype}_n${size}_t${T}_p${p}_exp${i}.pickle"
        else
            outputfile="./output_flipping_${networktype}_n${size}_t${T}_y${gamma}_exp${i}.pickle"
        fi

        python ~/repos/r0bit/idt_criticality.py --network $networkfile -T ${T} --numsteps ${numsteps} --remember ${remembersteps} --sigma ${sigma} --save_intermediate_results -v --numruns 350 --ntrials 4000 --analysis flips -o ${outputfile}

        echo "note: finished loop ${i} of ${numexps}. Main output file is: ${outputfile}"
        echo "note: this took the following number of seconds since the start of loops:"

        echo "`date +%s` - ${timeBefore}" | bc
    done

    # note: use analyze_many.py to process the output files of this script

    # note: the network files (.adjlist) can be removed, they are also stored as part of the output files.

    exit 0
else
    echo "The network file does not exist, I guess the network generation part above failed."

    exit 1
fi