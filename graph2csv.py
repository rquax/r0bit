__author__ = 'rquax'

import networkx as nx
import csv

def to_numbered_graph(graph):
    """
Convert a networkx graph containing named nodes (like "user_profile0599345" or (98,45) or whatever) to the same network
where each node is identified by an integer, from the contiguous range 0,...,N-1 inclusive.
    :param graph:
    :return:
    """
    names2num = dict()
    next_num = 0

    graph_numbered = graph.copy()
    graph_numbered.clear()

    assert graph_numbered.number_of_nodes() == 0
    assert graph_numbered.number_of_edges() == 0

    mapping_dict = dict()  # maps each new (numeric) node id to the original one

    for node in graph.nodes_iter():
        if not names2num.has_key(node):
            names2num[node] = next_num
            graph_numbered.add_node(next_num)
            mapping_dict[next_num] = node
            next_num += 1

    for edge in graph.edges_iter():
        node_id1 = names2num[edge[0]]
        node_id2 = names2num[edge[1]]

        assert 0 <= node_id1 < graph.number_of_nodes()
        assert 0 <= node_id2 < graph.number_of_nodes()

        graph_numbered.add_edge(node_id1, node_id2)

    if not graph_numbered.number_of_nodes() == graph.number_of_nodes():
        print 'debug: graph_numbered.number_of_nodes() =', graph_numbered.number_of_nodes()
        print 'debug: graph.number_of_nodes() =', graph.number_of_nodes()
        print 'debug: type(graph) =', type(graph)
        print 'debug: type(graph_numbered) =', type(graph_numbered)

    assert graph_numbered.number_of_nodes() == graph.number_of_nodes()
    assert graph_numbered.number_of_edges() == graph.number_of_edges()

    nx.set_node_attributes(graph_numbered, 'original_node_label', mapping_dict)

    return graph_numbered


def is_numbered_graph(graph):
    nodes = graph.nodes()

    for node in nodes:
        if not type(node) == int:
            return False

    # there could be two 'nudger' nodes present
    if graph.has_node(-1) and graph.has_node(-2):
        if not max(nodes) == len(nodes) - 1 - 2:
            print 'debug1: nodes =', nodes
            return False

        if not min(nodes) == -2:
            print 'debug2: nodes =', nodes
            return False

        if not len(set(nodes)) == len(nodes):
            print 'debug3: nodes =', nodes
            return False
    else:
        if not max(nodes) == len(nodes) - 1:
            print 'debug4: nodes =', nodes
            return False

        if not min(nodes) == 0:
            print 'debug5: nodes =', nodes
            return False

        if not len(set(nodes)) == len(nodes):
            print 'debug6: nodes =', nodes
            return False

    return True


def write_adjlists_csv(graph_numbered, fn):
    """
Write the network to a csv file, with each row i=0,1,2,... corresponding to node-id i, and each row consisting of
the list of neighbor node-ids. For undirected graphs each edge's reciprocal will be listed as well. For directed graphs,
only the outgoing edges, so edge (1,3) will be listed as a '3' on row 1 (second row in the file). If you want the
incoming edges instead, pass me graph_numbered.reverse().
    :param graph_numbered: networkx graph; nodes should be identified by contiguous integers, starting from 0.
    :param fn: filename for the csv output
    """
    fout = open(fn, 'wb')

    csvw = csv.writer(fout, delimiter=',')

    for node_id in xrange(graph_numbered.number_of_nodes()):
        # you should give me a graph with integers 0,...,N-1 as nodes, like you get from to_numbered_graph()
        assert graph_numbered.has_node(node_id)

        # write list of neighbor ids to row 'node_id' of the csv output file
        csvw.writerow(graph_numbered.neighbors(node_id))

    fout.close()