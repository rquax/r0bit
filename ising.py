__author__ = 'rquax'

import pandas as pd
import copy
import csv
import time
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os.path as op
import os
from subprocess import call
import glob
import pickle
from scipy.optimize import brentq
from scipy.interpolate import UnivariateSpline, InterpolatedUnivariateSpline
from collections import Counter
import scipy.stats as ss
from numbers import Number
import pathos.multiprocessing as mp


import calcidt_extrapolate as ce
import graph2csv as g2c
import pca_nodeprop_result as pca


_statespace = np.array([1, -1])
_state_type = np.int8

_possible_dynamics = ('glauber', 'voter_model')
# _dynamics = 'glauber'
_dynamics = 'glauber'


def total_network_interaction_energy_node(graph, node, states):
    # if type(states) == np.ndarray:
    #     # J = graph[nbr][node]['weight']
    #     # warning: here I presume the ordering of the weights are the same as that of the predecessors!
    #     weights = np.array(map(lambda x: x['weight'], zip(*graph.in_edges(1, data=True))[-1]))
    #     preds = graph.predecessors(node)
    #     energy_sum = np.sum(weights * states[preds] * states[node])
    #
    #     # consider also external magnetization, if specified (then equivalent to h=0)
    #     node_data = graph.node[node]
    #     if 'h' in node_data.keys():
    #         h = float(node_data['h'])
    #         energy_sum += -states[node] * h
    #
    #     return energy_sum
    # else:
    energy_sum = 0

    for nbr in graph.predecessors_iter(node):
        # edge_data = graph[nbr][node]
        # assert not type(edge_data) == float, 'edge_data=%s' % edge_data
        # assert 'weight' in edge_data.keys(), 'keys are %s while I expected "weight"' % str(edge_data.keys())

        # J = graph[nbr][node]['weight']

        energy_sum += -states[node] * states[nbr] * graph[nbr][node]['weight']

    # consider also external magnetization, if specified (then equivalent to h=0)
    node_data = graph.node[node]
    if 'h' in node_data.keys():
        h = float(node_data['h'])
        energy_sum += -states[node] * h

    return energy_sum


def generate_powerlaw_network(size, gamma=2.0, only_giant_component=True, create_using=nx.Graph(),
                              remove_selfloops=True):

    """
    Generate a network with a powerlaw degree distribution

    Note: I put this function here but just out of convenience, should maybe move it to somewhere else.
    :param size:
    :param gamma:
    :param only_giant_component:
    :param create_using:
    :param remove_selfloops:
    :rtype: nx.Graph
    """

    try:
        degs = nx.utils.powerlaw_sequence(size, gamma)

        degs = map(int, map(round, degs))

        if int(np.sum(degs)) % 2 == 1:  # odd?
            degs[np.random.randint(len(degs))] += 1  # break the unevenness of the sum of degrees

        G = nx.configuration_model(degs, create_using=create_using)
    except nx.NetworkXError as e:  # probably because the sum of degrees is not even, so cannot satisfy it
        # degs[np.random.randint(len(degs))] += 1.0  # break the unevenness of the sum of degrees
        #
        # G = nx.configuration_model(map(int, map(round, degs)), create_using=create_using)
        assert False, 'error: e = ' + str(e)

    if only_giant_component:
        G = nx.connected_component_subgraphs(G).next()

    if remove_selfloops:
        G.remove_edges_from(G.selfloop_edges())

    return G


# untested
def generate_directed_powerlaw_network(size, gamma=2.0, only_giant_component=True, remove_selfloops=True):

    # generate two scalefree networks independently
    G1 = generate_powerlaw_network(size, gamma, only_giant_component-only_giant_component,
                                   remove_selfloops=remove_selfloops)
    G2 = generate_powerlaw_network(size, gamma, only_giant_component-only_giant_component,
                                   remove_selfloops=remove_selfloops)

    digraph = nx.empty_graph(create_using=nx.DiGraph())

    for edge in G1.edges_iter():
        if np.random.randint(2) == 0:
            digraph.add_edge(edge[0], edge[1])
        else:
            digraph.add_edge(edge[1], edge[0])

    for edge in G2.edges_iter():
        if np.random.randint(2) == 0:
            digraph.add_edge(edge[0], edge[1])
        else:
            digraph.add_edge(edge[1], edge[0])

    return digraph


def energy_node(graph, node, states, T, state=None):
    # global _debug_calls_energy_T_scalar
    # global _debug_calls_energy_T_list

    assert not _dynamics == 'voter_model'

    if np.isscalar(T):
        temp = T

        # if __debug__:
        #     _debug_calls_energy_T_scalar += 1
    else:
        temp = T[node]

        # if __debug__:
        #     _debug_calls_energy_T_list += 1

    # assert np.isscalar(temp)

    if not state is None:
        # make a copy of the `states` array where the `node`th entry is changed to specified state
        # states = copy.deepcopy(states)  # deepcopy seems slow
        prev_state = states[node]
        states[node] = state
        # node_state = state
        enrg = total_network_interaction_energy_node(graph, node, states) / float(temp)
        states[node] = prev_state
    else:
        # node_state = states[node]
        enrg = total_network_interaction_energy_node(graph, node, states) / float(temp)

    # return sum([interaction_energy(node_state, states[nbr])
    #             for nbr in graph.predecessors_iter(node)]) / float(temp)
    return enrg


def energy(graph, states, T):
    total_energy = sum([energy_node(graph, node, states, T) for node in graph.nodes()])

    assert np.isscalar(total_energy)
    assert np.isfinite(total_energy)

    return total_energy


def likelihood(graph, states, T):
    lh = np.exp(-energy(graph, states, T))

    assert np.isscalar(lh)
    try:
        assert np.isfinite(lh)
    except AssertionError as e:
        print 'debug: energy(graph, states, T) =', energy(graph, states, T)
        print 'debug: lh =', lh

        raise AssertionError(e)

    return lh


def sample_state_given_magnetization(graph, magnetization, T, num_samples=1000):
    """
This function generates 'num_samples' system states which satisfy the magnetization, calcultes the eqilibrium
likelihood of each, and draws one system state using the likelihoods as weights, and returns that state.
    :param graph:
    :param magnetization: float or int, sum of spin states (-1, +1), so in range [-size, size]
    :param T: float
    :param num_samples:
    :return: list of node states which satisfies the magnetization, drawn using equilibrium likelihoods
    :rtype: list
    """
    assert np.isscalar(magnetization)
    assert np.isfinite(magnetization)

    size = graph.number_of_nodes()

    assert len(_statespace) == 2  # code below assumes this

    num_1 = int(round((magnetization + size) / 2))
    assert 0 <= num_1 <= size
    num_0 = size - num_1

    states = [_statespace[0]]*num_1 + [_statespace[1]]*num_0

    assert len(states) == size

    samples_states = []
    samples_likelihoods = []

    for i in xrange(num_samples):
        # generate some spin states completely random, but which satisfies the given magnetization exactly
        states = list(np.random.permutation(states))

        lh = likelihood(graph, states, T)

        if lh > 0.0:
            samples_states.append(states)
            samples_likelihoods.append(lh)

    sum_lhs = np.sum(samples_likelihoods)

    assert np.isscalar(sum_lhs)
    assert np.isfinite(sum_lhs)

    sample_state_ix = np.random.choice(range(len(samples_states)), p=np.divide(samples_likelihoods, sum_lhs))

    # this state should satisfy the given magnetization, modulo a minor round-off
    assert abs(sum(samples_states[sample_state_ix]) - magnetization) < 2

    return samples_states[sample_state_ix]


def prob_states(graph, node, states, T):
    # state_ix = list(_statespace).index(state)

    if _dynamics in ('glauber',):
        if np.isscalar(T):
            assert T >= 0.0

        likelihoods = np.array([np.exp(-energy_node(graph, node, states, T, s)) for s in _statespace])
        # lh_state = np.exp(-energy_node(graph, node, states, T, state))

        return np.divide(likelihoods, np.sum(likelihoods))
    elif _dynamics in ('voter_model', 'random_choice', 'random_neighbor', 'random_nbr'):
        if np.isscalar(T):
            assert T >= 0.0

        nbr_states = [states[nbr] for nbr in graph.predecessors(node)]
        likelihoods = [nbr_states.count(s) for s in _statespace]
        temp = (T if np.isscalar(T) else T[node])
        assert np.isscalar(temp)
        likelihoods[0] *= np.exp(temp)
        # if state == _statespace[0]:
        #     lh_state = nbr_states.count(state) * np.exp(temp)
        # else:
        #     lh_state = nbr_states.count(state)

        return np.divide(likelihoods, np.sum(likelihoods))
    else:
        raise RuntimeError('unsupported dynamics function: '+str(_dynamics))


# glauber
def prob_state(graph, node, states, T, state):
    if _dynamics in ('glauber',):
        if np.isscalar(T):
            assert T >= 0.0

        state_ix = list(_statespace).index(state)
        likelihoods = [np.exp(-energy_node(graph, node, states, T, s)) for s in _statespace]
        # lh_state = np.exp(-energy_node(graph, node, states, T, state))
        lh_state = likelihoods[state_ix]  # prevent double computation

        return float(lh_state) / float(sum(likelihoods))
    elif _dynamics in ('voter_model', 'random_choice', 'random_neighbor', 'random_nbr'):
        if np.isscalar(T):
            assert T >= 0.0

        nbr_states = [states[nbr] for nbr in graph.predecessors(node)]
        likelihoods = [nbr_states.count(s) for s in _statespace]
        temp = (T if np.isscalar(T) else T[node])
        assert np.isscalar(temp)
        likelihoods[0] *= np.exp(temp)
        if state == _statespace[0]:
            lh_state = nbr_states.count(state) * np.exp(temp)
        else:
            lh_state = nbr_states.count(state)

        return float(lh_state) / float(sum(likelihoods))
    else:
        raise RuntimeError('unsupported dynamics function: '+str(_dynamics))


def mutual_information_step(graph, node_id_now, node_id_next, states, T):
    probs_states_now = [prob_state(graph, node_id_now, states, T, s) for s in _statespace]

    assert abs(1.0 - sum(probs_states_now)) <= 0.001

    new_states = states.copy()
    cond_probs_states_next = []

    for s1 in _statespace:
        new_states[node_id_now] = s1

        cond_probs_states_next_i = [prob_state(graph, node_id_next, new_states, T, s2) for s2 in _statespace]

        assert abs(1.0 - sum(cond_probs_states_next_i)) <= 0.001

        cond_probs_states_next.append(cond_probs_states_next_i)

    prior_probs_states_next = np.sum([np.multiply(probs_states_now[si], cond_probs_states_next[si])
                                      for si in xrange(len(_statespace))], axis=0)

    assert len(prior_probs_states_next) == len(_statespace)
    assert abs(1.0 - sum(prior_probs_states_next)) <= 0.001

    mi = 0.0

    for si_now in xrange(len(_statespace)):
        for si_next in xrange(len(_statespace)):
            assert np.isscalar(probs_states_now[si_now])
            assert np.isscalar(cond_probs_states_next[si_now][si_next])
            joint_prob = probs_states_now[si_now] * cond_probs_states_next[si_now][si_next]
            assert np.isscalar(joint_prob)
            assert np.isfinite(joint_prob)
            if joint_prob != 0.0:
                term_for_mi = joint_prob * \
                              np.log2(joint_prob / (probs_states_now[si_now] * prior_probs_states_next[si_next]))
                assert np.isscalar(term_for_mi)
                if not np.isfinite(term_for_mi):
                    print 'debug: prior_probs_states_next[si_next] = ', prior_probs_states_next[si_next]
                    print 'debug: probs_states_now[si_now] = ', probs_states_now[si_now]
                    print 'debug: joint_prob = ', joint_prob
                    assert False
                mi += term_for_mi
            else:
                mi += 0.0  # assume 0 log 0 == 0

    assert np.isscalar(mi)
    assert np.isfinite(mi)

    return mi


_filename_network = './.temporary_network_r0bit.csv'
_dir_outputs = 'outputs'


def H(p):
    if 0.0 < p < 1.0:
        return -p * np.log2(p) - (1.0 - p) * np.log2(1.0 - p)
    elif p == 0.0 or p == 1.0:
        return 0.0
    else:
        raise ValueError

def Hset(ps):
    logterm = lambda p: -p * np.log2(p) if 0.0 < p < 1.0 else (0.0 if p in (1.0, 0.0) else np.nan)

    ent = sum(map(logterm, ps))

    assert abs(sum(ps) - 1.0) <= 0.0001

    assert ent >= 0.0
    assert ent <= np.log2(len(ps)) + 0.0001  # consider some small roundoff error
    assert np.isscalar(ent)

    return ent


def asymptotic_average_state(init_system_state, steps, ntrials=1000):
    """
This is one possible macroscopic characterization of a networked system's behavior. Could be used as the 'result'
(last) column in the matrix passed to the PCA analysis (see main code, where analysis == 'pca' for instance).

Note: for glauber/ising-like dynamics, the system will eventually be ergodic. This means that the state at some far-away
time point does not depend on the initial condition anymore. Therefore, for such (ergodic) systems, you should pick
a small enough 'steps' in order to still capture the dependence on the initial condition, but not too small in order
to still capture the influence of the global network structure. (Once or twice the diameter of the network?)
    :param init_system_state:
    :param steps:
    :param ntrials:
    :return: float M, -1 <= M <= +1
    """
    output_files_pattern = 'output*.dat'
    # these files (patterns) below are generated by the call to the run_idt_specified.py script, and altough they
    # are not needed by this function, I will clean up anyway...
    init_output_files_pattern = 'init_*.dat'
    descr_output_files_pattern = 'description_*.dat'

    assert g2c.is_numbered_graph(graph_numbered)

    g2c.write_adjlists_csv(graph_numbered, _filename_network)

    print 'debug: mutual_informations_node_nodes: current working directory: ', os.getcwd()
    # call(['mkdir', _dir_outputs])
    # call(['cd', _dir_outputs])

    # there should not be already output files in this directory, because who knows, maybe the next call will not
    # exactly replace all of them, and then when I start reading in files that satisfy the pattern
    # 'output_files_pattern' then I may read in old results...
    assert glob.glob(output_files_pattern) == []

    if type(init_system_state) == dict:
        state_string = ''.join(map(str, init_system_state.values()))
    else:
        state_string = ''.join(map(str, init_system_state))
    state_string = state_string.replace('-1', '0')

    if not len(state_string) == len(init_system_state):
        print 'error: ', len(state_string), ' != ', len(init_system_state)
        print 'error: state_string =', state_string
        print 'error: init_system_state =', init_system_state
        assert False

    call(['python ~/PycharmProjects/idt/run_idt_specified.py '
          ' --equilsteps 0 '
          ' --steps ' + str(steps) +
          ' --ntrials ' + str(ntrials) +
          ' --numruns 1'
          ' --state ' + str(state_string) +  # particular initial state specified
          ' --network ' + str(_filename_network) +
          ' --direction backward '
          ' --trans async '
          ' --tag generated '
          ' --temp ' + str(T) +
          ' --ensure_exp_id 0 '
          ' --path_exec ~/repos/ising/IsingOnCN '
          ' --no_skip'], shell=True)

    output_files = glob.glob(output_files_pattern)

    assert len(output_files) == 1

    out_fn = output_files[0]

    matrix = ce.read_mathematica_matrix(out_fn)
    # first is the 'snapshot, i.e., a sampled next (t=1) system state; second is probs for t=0
    assert len(matrix) == steps + 1

    init_state_probs = matrix[0]
    future_state_probs = matrix[-1]

    assert len(init_state_probs) == len(init_system_state)
    assert len(future_state_probs) == len(init_system_state)

    if __debug__:
        for init_prob in init_state_probs:
            assert init_prob in (1.0, 0.0)

        for fut_prob in future_state_probs:
            assert 0.0 <= fut_prob <= 1.0

    # remove output files again, to avoid confusion in the next call
    # call(['rm', output_files_pattern])
    print 'note: removing output files... (', output_files_pattern, ')'
    for outfile in output_files:
        os.remove(outfile)
    print 'note: removing init output files... (', init_output_files_pattern, ')'
    for outfile in glob.glob(init_output_files_pattern):
        os.remove(outfile)
    print 'note: removing description output files... (', descr_output_files_pattern, ')'
    for outfile in glob.glob(descr_output_files_pattern):
        os.remove(outfile)

    average_future_magn = np.average(map(lambda x: 2.0 * x - 1.0, future_state_probs))

    assert np.isscalar(average_future_magn)

    return average_future_magn


def mutual_information_node_system(graph_numbered, T, num_runs=1000, ntrials=1000):
    """
The amount of mutual information between the current node state (random node states) and the next system state, i.e.,
I(s_0 : S_1). It is assumed that S_0 is randomly initialized, i.e., each node state is uniformly random chosen.
    :param graph_numbered: network of interactions, with numbered node labels as ints in [0, size)
    :type graph_numbered: nx.Graph
    :param node: node identifier, part of graph.nodes()
    :param states: list
    :param T: temperature
    """

    output_files_pattern = 'output*.dat'
    # these files (patterns) below are generated by the call to the run_idt_specified.py script, and altough they
    # are not needed by this function, I will clean up anyway...
    init_output_files_pattern = 'init_*.dat'
    descr_output_files_pattern = 'description_*.dat'

    assert g2c.is_numbered_graph(graph_numbered)

    g2c.write_adjlists_csv(graph_numbered, _filename_network)

    print 'debug: mutual_informations_node_nodes: current working directory: ', os.getcwd()
    # call(['mkdir', _dir_outputs])
    # call(['cd', _dir_outputs])

    # there should not be already output files in this directory, because who knows, maybe the next call will not
    # exactly replace all of them, and then when I start reading in files that satisfy the pattern
    # 'output_files_pattern' then I may read in old results...
    assert glob.glob(output_files_pattern) == []

    call(['python ~/PycharmProjects/idt/run_idt_specified.py '
          ' --equilsteps 1 '
          ' --steps 1 '
          ' --ntrials ' + str(ntrials) +
          ' --numruns ' + str(num_runs) +
          ' --network ' + str(_filename_network) +
          ' --direction backward '
          ' --trans async '
          ' --tag generated '
          ' --temp ' + str(T) +
          ' --ensure_exp_id 0 '
          ' --path_exec ~/repos/ising/IsingOnCN '
          ' --no_skip'], shell=True)

    output_files = glob.glob(output_files_pattern)

    assert len(output_files) == num_runs

    entropies_cur_node_state_per_node = [0.0]*graph_numbered.number_of_nodes()

    for out_fn in output_files:
        matrix = ce.read_mathematica_matrix(out_fn)
        # first is the 'snapshot, i.e., a sampled next (t=1) system state; second is probs for t=0
        assert len(matrix) == 2

        probs_cur_states_given_next_system_state = matrix[-1]
        assert len(probs_cur_states_given_next_system_state) == graph_numbered.number_of_nodes()

        ents = map(H, probs_cur_states_given_next_system_state)

        entropies_cur_node_state_per_node = np.add(entropies_cur_node_state_per_node, ents)

        assert len(entropies_cur_node_state_per_node) == graph_numbered.number_of_nodes()

    avg_entropies_cur_node_state_per_node = np.divide(entropies_cur_node_state_per_node, float(len(output_files)))

    assert len(avg_entropies_cur_node_state_per_node) == graph_numbered.number_of_nodes()

    mis = np.subtract(H(1.0 / len(_statespace)), avg_entropies_cur_node_state_per_node)

    assert len(mis) == graph_numbered.number_of_nodes()

    for mi in mis:
        assert 0.0 <= mi <= H(1.0 / len(_statespace))

    # remove output files again, to avoid confusion in the next call
    # call(['rm', output_files_pattern])
    print 'note: removing output files... (', output_files_pattern, ')'
    for outfile in output_files:
        os.remove(outfile)
    print 'note: removing init output files... (', init_output_files_pattern, ')'
    for outfile in glob.glob(init_output_files_pattern):
        os.remove(outfile)
    print 'note: removing description output files... (', descr_output_files_pattern, ')'
    for outfile in glob.glob(descr_output_files_pattern):
        os.remove(outfile)

    return mis


def entropy_nextstate_nodes_given_system_initstate(graph_numbered, T, initial_system_state=None):
    if initial_system_state is None:
        initial_system_state_list = [np.random.choice(_statespace) for nodelbl in graph_numbered.nodes_iter()]
    else:
        if type(initial_system_state) == dict:
            initial_system_state_list = initial_system_state.values()
        else:
            initial_system_state_list = initial_system_state

    if not type(initial_system_state_list) in (list, tuple):
        print 'error: init state =', initial_system_state
        print 'error: type = ', type(initial_system_state)

        raise ValueError

    next_state_probs = update_states_step_probs(graph_numbered, initial_system_state_list, T)

    assert len(next_state_probs) == graph_numbered.number_of_nodes()
    assert np.isscalar(next_state_probs[0])

    return map(H, next_state_probs)


def mutual_informations_node_nodes(graph_numbered, node, T, num_runs=100):
    """
The amount of mutual information between the current node state (random node states) and each next node state, i.e.,
I(s_0 : sj_1). It is assumed that S_0 is randomly initialized, i.e., each node state is uniformly random chosen.
    :param graph_numbered: networks Graph object
    :type graph_numbered: nx.Graph
    :param node: node identifier, part of graph.nodes()
    :param states: list
    :param T: temperature
    """

    output_files_pattern = 'output*.dat'
    # these files (patterns) below are generated by the call to the run_idt_specified.py script, and altough they
    # are not needed by this function, I will clean up anyway...
    init_output_files_pattern = 'init_*.dat'
    descr_output_files_pattern = 'description_*.dat'

    assert g2c.is_numbered_graph(graph_numbered)

    g2c.write_adjlists_csv(graph_numbered, _filename_network)

    print 'debug: mutual_informations_node_nodes: current working directory: ', os.getcwd()
    # call(['mkdir', _dir_outputs])
    # call(['cd', _dir_outputs])

    # there should not be already output files in this directory, because who knows, maybe the next call will not
    # exactly replace all of them, and then when I start reading in files that satisfy the pattern
    # 'output_files_pattern' then I may read in old results...
    assert glob.glob(output_files_pattern) == []

    call(['python ~/PycharmProjects/idt/run_idt_specified.py '
          ' --equilsteps 0 '
          ' --steps 1 '
          ' --ntrials 4000 '
          ' --numruns ' + str(num_runs) +
          ' --network ' + str(_filename_network) +
          ' --direction forward '
          ' --trans async '
          ' --tag generated '
          ' --temp ' + str(T) +
          ' --ensure_exp_id 0 '
          ' --path_exec ~/repos/ising/IsingOnCN '
          ' --no_skip'], shell=True)

    output_files = glob.glob(output_files_pattern)

    assert len(output_files) == num_runs

    # entropies_next_node_states_per_node_state = {state: [] for state in _statespace}

    cond_probs_next = {state: [] for state in _statespace}

    probs_next = []

    for out_fn in output_files:
        matrix = ce.read_mathematica_matrix(out_fn)
        # first is the 'snapshot, i.e., a sampled now (t=0) system state; second is probs for t=1
        assert len(matrix) == 2

        probs_next_states_given_current_system_state = matrix[-1]
        probs_cur_states = matrix[0]
        assert len(probs_next_states_given_current_system_state) == graph_numbered.number_of_nodes()

        # ents = map(H, probs_next_states_given_current_system_state)
        probs_next.append(probs_next_states_given_current_system_state)
        assert len(_statespace) == 2
        node_state = _statespace[0] if probs_cur_states[node] > 0.5 else _statespace[1]

        # entropies_next_node_states_per_node_state[node_state].append(ents)

        cond_probs_next[node_state].append(probs_next_states_given_current_system_state)

    entropies_next_node_states_per_node_state = {state: map(H, np.average(cond_probs_next[state], axis=0))
                                                 for state in _statespace}

    prob_node_state = {state: float(len(cond_probs_next[state])) / float(len(output_files))
                       for state in _statespace}

    prior_probs_next = np.average(probs_next, axis=0)

    print 'debug: prob_node_state.values() =', prob_node_state.values()
    if not sum(prob_node_state.values()) == 1.0:
        print 'debug: prob_node_state.values() =', prob_node_state.values()
        print 'debug: sum(prob_node_state.values()) =', sum(prob_node_state.values())
    assert sum(prob_node_state.values()) == 1.0

    equil_ents_next = map(H, prior_probs_next)

    assert len(equil_ents_next) == graph_numbered.number_of_nodes()

    print 'debug: min prior_probs_next =', min(prior_probs_next)

    mis = sum([np.multiply(prob_node_state[state], np.average(cond_probs_next[state], axis=0)) *
               np.log2(np.multiply(prob_node_state[state], np.average(cond_probs_next[state], axis=0))
                       / np.multiply(prob_node_state[state], prior_probs_next))
                for state in _statespace]) + \
        sum([np.multiply(prob_node_state[state], 1.0 - np.average(cond_probs_next[state], axis=0)) *
               np.log2(np.multiply(prob_node_state[state], 1.0 - np.average(cond_probs_next[state], axis=0))
                       / np.multiply(prob_node_state[state], 1.0 - prior_probs_next))
                for state in _statespace])

    # mis = sum([prob_node_state[state] *
    #            np.subtract(equil_ents_next,
    #                        np.average(entropies_next_node_states_per_node_state[state], axis=0))
    #         for state in _statespace])

    assert len(mis) == graph_numbered.number_of_nodes()
    for mi in mis:
        if not 0.0 <= mi <= H(1.0 / len(_statespace)):
            print 'debug: mi =', mi
        assert 0.0 <= mi <= H(1.0 / len(_statespace))

    # remove output files again, to avoid confusion in the next call
    # call(['rm', output_files_pattern])
    # call(['rm', other_output_files_pattern])
    print 'note: removing output files... (', output_files_pattern, ')'
    for outfile in output_files:
        os.remove(outfile)
    print 'note: removing init output files... (', init_output_files_pattern, ')'
    for outfile in glob.glob(init_output_files_pattern):
        os.remove(outfile)
    print 'note: removing description output files... (', descr_output_files_pattern, ')'
    for outfile in glob.glob(descr_output_files_pattern):
        os.remove(outfile)

    return mis


def fit_temperature_to_flip_prob(graph_numbered, target_flip_prob, Ts=None, num_equil_steps=1000, num_equils=10,
                                 num_trials=100, verbose=True):
    """
Fit the temperature value to a given fraction of flips per time step (target_flip_prob).
    :param graph_numbered:
    :param target_flip_prob: float, 0.0 <= target_flip_prob <= 0.5
    :param Ts: list of floats, temperature values to compute the flipping fraction for, to be interpolated and then
    searched for its root
    :param num_equil_steps: how many steps to perform starting from a random configuration in order to equilibrate
    :param num_equils: how many times to generate a random configuration and equilibrate it, to find multiple
    equilibrium states and average the fraction over them as well
    :param num_trials: how many times to perform a single step from an equilibrium state in order to estimate the number
    of flips that occurs (average).
    :return: temperature value
    :rtype: float
    """
    if Ts is None:
        Ts = np.linspace(0.1, 10.0, 15)

    assert 0.0 <= target_flip_prob <= 0.5

    flip_probs = []

    for T in Ts:
        if verbose:
            print 'debug: fit_temperature_to_flip_prob: computing flipping fraction for T=', T, 'of set', Ts

        (avg_flip_prob, stderr_flip_prob) = equil_flip_probability_per_spin(graph_numbered, T,
                                                                            num_equil_steps=num_equil_steps,
                                                                            num_equils=num_equils,
                                                                            num_trials=num_trials)

        assert 0.0 <= avg_flip_prob <= 1.0

        flip_probs.append(avg_flip_prob)

    flip_prob_around_zero = InterpolatedUnivariateSpline(Ts, np.subtract(flip_probs, target_flip_prob), k=2)

    t_left = Ts[0]
    t_right = Ts[-1]

    t = brentq(flip_prob_around_zero, t_left, t_right, rtol=1e-4, maxiter=500)

    assert t_left <= t <= t_right

    if verbose:
        print 'debug: flipping fraction curve with points (T, flip_prob):', np.transpose([Ts, flip_probs])

    return t


def entropy(ar, base=2):
    """
    Entropy of an array of hashable objects.
    :param ar:
    :param base:
    :return: float
    """
    cts = np.array(Counter(ar).values())
    cts = cts / float(len(cts))

    return ss.entropy(cts, base=base)


def mutual_information(ar1, ar2, base=2):
    """
    Mutual information between two paired arrays of hashable objects.
    :param ar1:
    :param ar2:
    :param base:
    :return: float
    """

    if np.ndim(ar1) == 1:
        ar1n = [(a,) for a in ar1]
    else:
        ar1n = map(tuple, ar1)

    if np.ndim(ar2) == 1:
        ar2n = [(a,) for a in ar2]
    else:
        ar2n = map(tuple, ar2)

    H_X = entropy(ar1n)
    H_Y = entropy(ar2n)

    H_XY = entropy(zip(map(tuple, ar1), map(tuple, ar2)))

    return H_X + H_Y - H_XY


def idt_per_node_sink(ising_net, trans='async', num_cur_states=100, num_steps=None, num_repeats=100,
                      assume_symmetry=False, epsilon=0.001, frac=1./np.e, include_double_exp=False, nprocs=1):
    """
    This will estimate by Monte-Carlo the expression I(x[t] : X[0]) where t=[0..numsteps] and X[0] is the system
    'snapshot' which will be sampled `num_cur_states` times.
    :param num_repeats:
    :param assume_symmetry:
    :param epsilon: absolute amount of mutual information above the baseline to consider for
    the absolute decay, should be a small amount
    :param frac: fraction of initial entropy to consider for relative decay, like 0.5 or 1/e
    :return:
    :type ising_net: IsingNetwork
    :param trans:
    :param num_cur_states:
    :param num_steps:
    :return: a ce.DecayTimeResponse object augmented with a decay_times_abs field
    :rtype: ce.DecayTimeResponse
    """
    if num_steps is None:
        num_steps = min(max(5, ising_net.graph_numbered.number_of_nodes()), 100)

    # get `num_cur_states` state sequences to start from (forming entropy H[X(t=0)])
    cur_states = []
    ising_net2 = copy.deepcopy(ising_net)
    for cix in range(num_cur_states):
        if not assume_symmetry:
            ising_net2.reset_states()  # first randomize states again so that half the time I explore magnet. < 0
        eq_st = ising_net2.equilibrate(trans=trans)
        if assume_symmetry:
            # make sure the magnetization is non-negative, since the negative side is symmetric anyway; helps
            # get more samples effectively (twice more)
            if np.sum(eq_st) < 0:
                eq_st = list(np.multiply(eq_st, -1))
        cur_states.append(eq_st)
    del ising_net2

    # TODO: if assume_symmetry then shouldn't you duplicate states_sequences with its negated copy? so that H(x_0)=1?
    # (also cur_states then)

    # if nprocs == 1:
    #     # shape: (num_cur_states, num_repeats, num_steps+1, len(ising_net.states))
    #     states_sequences = [[ising_net.go(num_steps, cur_states=cs, trans=trans)
    #                                 for _ in range(num_repeats)]
    #                         for cs in cur_states]
    # else:
    if True:
        pool = mp.Pool(nprocs)

        def worker_mi_over_time(cs):
            return [ising_net.go(num_steps, cur_states=cs, trans=trans)
                    for _ in range(num_repeats)]

        if nprocs != 1:
            states_sequences_t = pool.map(worker_mi_over_time, cur_states)
        else:
            # for finding a bug:
            states_sequences_t = [worker_mi_over_time(cs) for cs in cur_states]

        pool.close()
        pool.terminate()

    # note: already named _t above here so that it simply overwrites and saves space

    # shape: (len(ising_net.states), num_steps+1, num_cur_states, num_repeats)
    states_sequences_t = np.transpose(states_sequences_t, axes=[3, 2, 0, 1])

    # shape: (len(ising_net.states), num_steps+1, num_cur_states * num_repeats)
    states_sequences_t_eq = np.reshape(states_sequences_t,
                                       states_sequences_t.shape[:2] + (np.product(states_sequences_t.shape[2:]),))

    # shape: (size, num_steps)
    H_eq = [[entropy(states_sequences_t_eq[nix][six]) for six in xrange(num_steps+1)] for nix in xrange(ising_net.size)]

    # shape: (num_cur_states, size, num_steps)
    # this is H[x_i(t) | X[0]]
    H_cond_X0 = [[[entropy(states_sequences_t[nix][six][cix])
                  for six in xrange(num_steps+1)]
                 for nix in xrange(ising_net.size)]
                for cix in xrange(num_cur_states)]

    # shape: (size, _statespace, # cur states where x0=x)
    cur_state_ids_where_node_is = [[[cix for cix in xrange(num_cur_states) if cur_states[cix][nix] == x0]
                                    for x0 in _statespace]
                                   for nix in xrange(ising_net.size)]

    # shape: (_statespace, size)
    P_node_x0 = [[float(len(cur_state_ids_where_node_is[nix][xix])) / len(cur_states)
                  for nix in xrange(ising_net.size)]
                 for xix in xrange(len(_statespace))]

    # shape: (_statespace, size, num_steps)
    # note: the whole "np.reshape(...)" is essentially `states_sequences_t_eq` but then conditioned on node `nix` having
    # state x0 (xix) in the 'current' state
    H_cond_x0 = [[[entropy(np.reshape(np.take(states_sequences_t, cur_state_ids_where_node_is[nix][xix], axis=2),
                                      states_sequences_t.shape[:2] + (len(cur_state_ids_where_node_is[nix][xix]) * num_repeats,))[nix][six])
                  if len(cur_state_ids_where_node_is[nix][xix]) > 0 else 0
                   for six in xrange(num_steps+1)]
                  for nix in xrange(ising_net.size)]
                 for xix in xrange(len(_statespace))]

    # # NOTE: useful? not exactly what I meant I think...
    # # shape: (size, num_steps)
    # I_xt_x0 = np.subtract(H_eq, np.sum([P_node_x0[xix] * np.transpose(H_cond_x0[xix])
    #                                     for xix in xrange(len(_statespace))], axis=0))

    tot_L = np.sum(
        [likelihood(ising_net.graph_numbered, cur_states[cix], ising_net.T) for cix in xrange(len(cur_states))])
    I_xt_X0 = np.subtract(H_eq, np.transpose(np.sum(
        [likelihood(ising_net.graph_numbered, cur_states[cix], ising_net.T) / tot_L * np.transpose(H_cond_X0[cix])
         for cix in xrange(len(cur_states))], axis=0)))

    # H_cond_x0 = [[[states_sequences_t[nix][] for six in xrange(num_steps)] for x0 in _statespace] for nix in xrange(ising_net.size)]

    # if method in ('rel', 'relative'):
    idt_resp = ce.rel_idts_raw(I_xt_X0, frac=frac)
    # elif method in ('abs', 'absolute'):
    idt_resp_abs = ce.abs_idts_raw(I_xt_X0, epsilon=epsilon)
    # else:
    #     raise NotImplementedError('unknown method')

    idt_resp.decay_times_abs = idt_resp_abs.decay_times

    idt_resp.mi_over_time = I_xt_X0

    if include_double_exp:  # include also analysis from assuming double exponential decay (fast+slow)
        double_exp_params = []
        double_exp_idts = []
        # as reminder to the caller in what order the IDTs are given in double_exp_idts
        double_idt_labels = ('relIDTfast', 'relIDTslow', 'absIDTfast', 'absIDTslow')

        for mis in idt_resp.mi_over_time:
            # for curve a*exp(-b*x) + c*exp(-d*x) + plateau it is the list of parameters (a, b, c, d, plateau)
            params = ce.fit_double_exp_decay(mis)
            # expanded for readability:
            relIDT1, relIDT2, absIDT1, absIDT2 = ce.convert_double_exp_params_to_idts(params, epsilon=epsilon)
            double_exp_params.append(params)
            double_exp_idts.append((relIDT1, relIDT2, absIDT1, absIDT2))

        idt_resp.double_exp_params = double_exp_params
        idt_resp.double_exp_idts = double_exp_idts
        idt_resp.double_idt_labels = double_idt_labels

    return idt_resp


def idt_per_node_source(ising_net, trans='async', num_cur_states=100, num_steps=None, num_repeats=100,
                 assume_symmetry=False, epsilon=0.001, frac=1./np.e, include_double_exp=False, nprocs=1):
    """
    This will *naively* estimate by Monte-Carlo the expression I(x[0] : X[t]) where t=0..numsteps and x[0] is from
    X[0] which is the system 'snapshot' which will be sampled `num_cur_states` times.
    :param num_repeats:
    :param assume_symmetry:
    :param epsilon: absolute amount of mutual information above the baseline to consider for
    the absolute decay, should be a small amount
    :param frac: fraction of initial entropy to consider for relative decay, like 0.5 or 1/e
    :return:
    :type ising_net: IsingNetwork
    :param trans:
    :param num_cur_states:
    :param num_steps:
    :return: a ce.DecayTimeResponse object augmented with a decay_times_abs field
    :rtype: ce.DecayTimeResponse
    """
    if num_steps is None:
        num_steps = min(max(5, ising_net.graph_numbered.number_of_nodes()), 100)

    # get `num_cur_states` state sequences to start from (forming entropy H[X(t=0)])
    cur_states = []
    ising_net2 = copy.deepcopy(ising_net)
    for cix in range(num_cur_states):
        if not assume_symmetry:
            ising_net2.reset_states()  # first randomize states again so that half the time I explore magnet. < 0
        eq_st = ising_net2.equilibrate(trans=trans)
        if assume_symmetry:
            # make sure the magnetization is non-negative, since the negative side is symmetric anyway; helps
            # get more samples effectively (twice more)
            if np.sum(eq_st) < 0:
                eq_st = list(np.multiply(eq_st, -1))
        cur_states.append(eq_st)
    del ising_net2

    # TODO: if assume_symmetry then shouldn't you duplicate states_sequences with its negated copy? so that H(x_0)=1?
    # (also cur_states then)

    # if nprocs == 1:
    #     # shape: (num_cur_states, num_repeats, num_steps+1, len(ising_net.states))
    #     states_sequences = [[ising_net.go(num_steps, cur_states=cs, trans=trans)
    #                                 for _ in range(num_repeats)]
    #                         for cs in cur_states]
    # else:
    if True:
        pool = mp.Pool(nprocs)

        def worker_mi_over_time(cs):
            return [ising_net.go(num_steps, cur_states=cs, trans=trans)
                    for _ in range(num_repeats)]

        if nprocs != 1:
            # shape: (num_cur_states, num_repeats, numsteps+1, size)
            states_sequences_t = pool.map(worker_mi_over_time, cur_states)
        else:
            # for finding a bug:
            states_sequences_t = [worker_mi_over_time(cs) for cs in cur_states]

        pool.close()
        pool.terminate()

    # note: already named _t above here so that it simply overwrites and saves space

    # note: len(ising_net.states) == size (of the network, so number of nodes)
    # shape: (len(ising_net.states), num_steps+1, num_cur_states, num_repeats)
    states_sequences_t = np.transpose(states_sequences_t, axes=[3, 2, 0, 1])

    # shape: (len(ising_net.states), num_steps+1, num_cur_states * num_repeats)
    states_sequences_t_eq = np.reshape(states_sequences_t,
                                       states_sequences_t.shape[:2] + (np.product(states_sequences_t.shape[2:]),))

    # shape: (num_steps+1, num_cur_states * num_repeats, len(ising_net.states))
    states_sequences_eq = np.transpose(states_sequences_t_eq, axes=[1, 2, 0])

    # shape: (size,)
    # H(x_i[0])
    H_x0 = [entropy(states_sequences_t_eq[nix][0]) for nix in xrange(ising_net.size)]

    # shape: (num_steps+1,)
    # H(x[0])
    H_Xt = [entropy(map(tuple, states_sequences_eq[six])) for six in xrange(num_steps+1)]

    # shape: (num_steps+1, len(ising_net.states), num_cur_states * num_repeats)
    states_sequences_2_eq = np.transpose(states_sequences_eq, axes=[0, 2, 1])

    # note: states_sequences_t_eq[six] has shape (num_cur_states * num_repeats, size)
    # note: states_sequences_2_eq[0][nix] has shape (num_cur_states * num_repeats,)
    # shape: (num_steps+1, size)
    H_Xt_x0 = [[entropy(map(tuple,
                            np.concatenate([states_sequences_eq[six],
                                        np.reshape(states_sequences_2_eq[0][nix], (num_cur_states * num_repeats,1))],
                                       axis=1)))
                for nix in xrange(ising_net.size)]
               for six in xrange(num_steps + 1)]

    # all shape: (num_steps+1, size), matching H_Xt_x0
    H_Xt = np.repeat(H_Xt, ising_net.size).reshape((num_steps+1, ising_net.size))
    H_x0 = np.transpose(np.repeat(H_x0, num_steps + 1).reshape((ising_net.size, num_steps+1)))

    # shape: (num_steps+1, size)
    I_x0_Xt = H_x0 + H_Xt - H_Xt_x0
    # shape: (size, num_steps+1)
    I_x0_Xt = np.transpose(I_x0_Xt)

    # # shape: (num_cur_states, size, num_steps)
    # # this is H[x_i(0) | X[t]]
    # H_cond_X0 = [[entropy(states_sequences_t[nix][0])
    #              for nix in xrange(ising_net.size)]
    #             for cix in xrange(num_cur_states)]
    #
    # # shape: (size, _statespace, # cur states where x0=x)
    # cur_state_ids_where_node_is = [[[cix for cix in xrange(num_cur_states) if cur_states[cix][nix] == x0]
    #                                 for x0 in _statespace]
    #                                for nix in xrange(ising_net.size)]
    #
    # # shape: (_statespace, size)
    # P_node_x0 = [[float(len(cur_state_ids_where_node_is[nix][xix])) / len(cur_states)
    #               for nix in xrange(ising_net.size)]
    #              for xix in xrange(len(_statespace))]
    #
    # # shape: (_statespace, size, num_steps)
    # # note: the whole "np.reshape(...)" is essentially `states_sequences_t_eq` but then conditioned on node `nix` having
    # # state x0 (xix) in the 'current' state
    # H_cond_x0 = [[[entropy(np.reshape(np.take(states_sequences_t, cur_state_ids_where_node_is[nix][xix], axis=2),
    #                                   states_sequences_t.shape[:2] + (len(cur_state_ids_where_node_is[nix][xix]) * num_repeats,))[nix][six])
    #               if len(cur_state_ids_where_node_is[nix][xix]) > 0 else 0
    #                for six in xrange(num_steps+1)]
    #               for nix in xrange(ising_net.size)]
    #              for xix in xrange(len(_statespace))]
    #
    # # # NOTE: useful? not exactly what I meant I think...
    # # # shape: (size, num_steps)
    # # I_xt_x0 = np.subtract(H_eq, np.sum([P_node_x0[xix] * np.transpose(H_cond_x0[xix])
    # #                                     for xix in xrange(len(_statespace))], axis=0))
    #
    # tot_L = np.sum(
    #     [likelihood(ising_net.graph_numbered, cur_states[cix], ising_net.T) for cix in xrange(len(cur_states))])
    # I_xt_X0 = np.subtract(H_eq, np.transpose(np.sum(
    #     [likelihood(ising_net.graph_numbered, cur_states[cix], ising_net.T) / tot_L * np.transpose(H_cond_X0[cix])
    #      for cix in xrange(len(cur_states))], axis=0)))

    # H_cond_x0 = [[[states_sequences_t[nix][] for six in xrange(num_steps)] for x0 in _statespace] for nix in xrange(ising_net.size)]

    # if method in ('rel', 'relative'):
    idt_resp = ce.rel_idts_raw(I_x0_Xt, frac=frac)
    # elif method in ('abs', 'absolute'):
    idt_resp_abs = ce.abs_idts_raw(I_x0_Xt, epsilon=epsilon)
    # else:
    #     raise NotImplementedError('unknown method')

    idt_resp.decay_times_abs = idt_resp_abs.decay_times

    idt_resp.mi_over_time = I_x0_Xt

    if include_double_exp:  # include also analysis from assuming double exponential decay (fast+slow)
        double_exp_params = []
        double_exp_idts = []
        # as reminder to the caller in what order the IDTs are given in double_exp_idts
        double_idt_labels = ('relIDTfast', 'relIDTslow', 'absIDTfast', 'absIDTslow')

        for mis in idt_resp.mi_over_time:
            params = ce.fit_double_exp_decay(mis)
            # expanded for readability:
            relIDT1, relIDT2, absIDT1, absIDT2 = ce.convert_double_exp_params_to_idts(params, epsilon=epsilon)
            double_exp_params.append(params)
            double_exp_idts.append((relIDT1, relIDT2, absIDT1, absIDT2))

        idt_resp.double_exp_params = double_exp_params
        idt_resp.double_exp_idts = double_exp_idts
        idt_resp.double_idt_labels = double_idt_labels

    return idt_resp


def equil_flip_probability_per_spin(graph_numbered, T, num_equil_steps=1000, num_equils=10, num_trials=100):
    """
Estimate the fraction of nodes whose state flips in a single transition step in an equilibrated Ising spin network, at
the given temperature. This can be computed for a range of temperatures to estimate the temperature for a given fraction
of flips per time step, in order to correct for size effects for different networks.
    :param graph_numbered:
    :param T: temperature >= 0.0
    :param num_equil_steps: how many steps to perform starting from a random configuration in order to equilibrate
    :param num_equils: how many times to generate a random configuration and equilibrate it, to find multiple
    equilibrium states and average the fraction over them as well
    :param num_trials: how many times to perform a single step from an equilibrium state in order to estimate the number
    of flips that occurs (average).
    :return: average fraction of nodes whose states flips during a single time step in equilibrium
    :rtype: float
    """
    output_files_pattern = 'output*.dat'
    # these files (patterns) below are generated by the call to the run_idt_specified.py script, and although they
    # are not needed by this function, I will clean up anyway...
    init_output_files_pattern = 'init_*.dat'
    descr_output_files_pattern = 'description_*.dat'

    assert g2c.is_numbered_graph(graph_numbered)

    g2c.write_adjlists_csv(graph_numbered, _filename_network)

    print 'debug: equil_flip_probability_per_spin: current working directory: ', os.getcwd()
    # call(['mkdir', _dir_outputs])
    # call(['cd', _dir_outputs])

    num_flips_list = []  # result list

    for r in xrange(num_equils):

        # there should not be already output files in this directory, because who knows, maybe the next call will not
        # exactly replace all of them, and then when I start reading in files that satisfy the pattern
        # 'output_files_pattern' then I may read in old results...
        assert glob.glob(output_files_pattern) == []

        call(['python ~/PycharmProjects/idt/run_idt_specified.py '
              ' --equilsteps ' + str(num_equil_steps) +
              ' --steps 0 '
              ' --ntrials 1 '
              ' --numruns 1'
              ' --network ' + str(_filename_network) +
              ' --direction forward '
              ' --trans async '
              ' --tag generated '
              ' --temp ' + str(T) +
              ' --ensure_exp_id 0 '
              ' --path_exec ~/repos/ising/IsingOnCN '
              ' --no_skip'], shell=True)

        output_files = glob.glob(output_files_pattern)

        assert len(output_files) == 1

        out_fn = output_files[0]

        matrix = ce.read_mathematica_matrix(out_fn)
        equil_state = matrix[-1]

        if __debug__:
            for node_state in equil_state:
                assert node_state in (0.0, 1.0)

        assert len(equil_state) == graph_numbered.number_of_nodes()

        equil_state = map(int, equil_state)
        equil_state = map(lambda x: 2*x - 1, equil_state)

        if __debug__:
            for node_state in equil_state:
                assert node_state in _statespace

        for trial in xrange(num_trials):
            next_state = update_states_step(graph_numbered, equil_state, T)

            num_flips = int((len(equil_state) - int(sum(np.multiply(equil_state, next_state))))/2)

            num_flips_list.append(num_flips)

        # remove output files again, to avoid confusion in the next call
        # call(['rm', output_files_pattern])
        # print 'note: removing output files... (', output_files_pattern, ')'
        for outfile in output_files:
            os.remove(outfile)
        # print 'note: removing init output files... (', init_output_files_pattern, ')'
        for outfile in glob.glob(init_output_files_pattern):
            os.remove(outfile)
        # print 'note: removing description output files... (', descr_output_files_pattern, ')'
        for outfile in glob.glob(descr_output_files_pattern):
            os.remove(outfile)


    return (np.average(num_flips_list) / float(graph_numbered.number_of_nodes()),
            np.std(num_flips_list) / np.sqrt(len(num_flips_list)))



class InformationProcessingMeasures(object):
    """
    Class to store mutual information and entropy quantities of nodes and between nodes.

    :param mi_pairwise_initstate_nextstate: [renamed mis] if given_initial_state==False: list of lists,
    where each list corresponds to a node in the network, and
    each float in each sublist is the mutual information between the corresponding node's initial state and the next
    state of the corresponding other node. That is, mis[node_id][other_node_id] = mutual information between node_id's
    initial state (t=0) and other_node_id's next state (t=1). If given_initial_state==True, on the other hand, a list
    of two lists (i.e. depth 3) will be returned. mis[node_id][given_node_state][other_node_id] = mutual information
    between node_id's initial state (t=0) and other_node_id's next state (t=1), given the fact that the node_id's
    initial state (t=0) equals given_node_state.
    :param given_initstate_system: system state used to compute entropy_nextstate_node_given_initstate_system
    :param given_initial_state: boolean value indicating whether mi_pairwise_initstate_nextstate is computed with
    the conditional MI (given initial state of a node) or not. See the explanation for mi_pairwise_initstate_nextstate
    as well.
    """

    mi_pairwise_initstate_nextstate = None
    entropy_initstate_node = None
    entropy_nextstate_node = None
    entropy_nextstate_node_given_initstate_system = None

    given_initstate_system = None

    given_initial_state = None


def mutual_informations_node_nodes_nosampling(graph_numbered, T, num_runs=100, verbose=True,
                                              given_initial_state_node=False):
    """
The amount of mutual information between the current node state (random node states) and each next node state, i.e.,
I(s_0 : sj_1). It is assumed that S_0 is randomly initialized, i.e., each node state is uniformly random chosen.
    :rtype : InformationProcessingMeasures
    :param num_runs: int
    :param verbose: bool
    :param given_initial_state_node: if False, return I(s_0 : sj_1) for each node; if True, return the pair
    [I(s_0 : sj_1 | s_0=1), I(s_0 : sj_1 | s_0=-1)], i.e., the mutual information between two nodes' consecutive states
    given the initial state of the first node.
    :param graph_numbered: networks Graph object
    :type graph_numbered: nx.Graph
    :param node: node identifier, part of graph.nodes()
    :param states: list
    :param T: temperature
    :type T: float
    :return: if given_initial_state==False: list of lists, where each list corresponds to a node in the network, and
    each float in each sublist is the mutual information between the corresponding node's initial state and the next
    state of the corresponding other node. That is, mis[node_id][other_node_id] = mutual information between node_id's
    initial state (t=0) and other_node_id's next state (t=1). If given_initial_state==True, on the other hand, a list
    of two lists (i.e. depth 3) will be returned. mis[node_id][given_node_state][other_node_id] = mutual information
    between node_id's initial state (t=0) and other_node_id's next state (t=1), given the fact that the node_id's
    initial state (t=0) equals given_node_state.
    """

    assert g2c.is_numbered_graph(graph_numbered)

    # entropies_next_node_states_per_node_state = {state: [] for state in _statespace}

    cond_probs_next_per_node = [{state: [] for state in _statespace}
                                for nodeid in xrange(graph_numbered.number_of_nodes())]

    probs_next = []

    for runid in xrange(num_runs):
        if verbose:
            print 'debug: performing run #', runid, 'out of', num_runs

        # initialize random state of the system, in terms of probabilities of having state +1
        init_state = {node: np.random.choice(_statespace) for node in graph_numbered.nodes_iter()}

        # n+ - n- = M
        # n+ + n- = N
        # (N + M)/2 = n+
        # next_state_probs = ((float(ntrials) + next_state_sum)/2.0) / float(ntrials)

        next_state_probs = update_states_step_probs(graph_numbered, init_state, T)

        for p in next_state_probs:
            assert 0.0 <= p <= 1.0

        matrix = [map(lambda x: 1.0 if x == 1 else (0.0 if x == -1 else np.nan), init_state.values()), next_state_probs]

        # matrix = ce.read_mathematica_matrix(out_fn)
        # first is the 'snapshot, i.e., a sampled now (t=0) system state; second is probs for t=1
        assert len(matrix) == 2

        probs_next_states_given_current_system_state = matrix[-1]
        probs_cur_states = matrix[0]
        assert len(probs_next_states_given_current_system_state) == graph_numbered.number_of_nodes()

        # ents = map(H, probs_next_states_given_current_system_state)
        probs_next.append(probs_next_states_given_current_system_state)
        assert len(_statespace) == 2

        for nodelbl in graph_numbered.nodes_iter():
            node_state = _statespace[0] if probs_cur_states[nodelbl] > 0.5 else _statespace[1]

            cond_probs_next_per_node[nodelbl][node_state].append(probs_next_states_given_current_system_state)

    assert np.isscalar(len(cond_probs_next_per_node[0][_statespace[0]]))
    assert np.isscalar(len(cond_probs_next_per_node[0][_statespace[-1]]))

    prior_prob_initstate_per_node = [{state: float(len(cond_probs_next_per_node[nodelbl][state])) / float(num_runs)
                       for state in _statespace}
                       for nodelbl in graph_numbered.nodes_iter()]

    assert len(prior_prob_initstate_per_node) == graph_numbered.number_of_nodes()
    assert np.isscalar(prior_prob_initstate_per_node[0][_statespace[0]])
    assert np.isscalar(prior_prob_initstate_per_node[0][_statespace[-1]])
    assert np.isfinite(prior_prob_initstate_per_node[0][_statespace[0]])
    assert np.isfinite(prior_prob_initstate_per_node[0][_statespace[-1]])

    prior_probs_next = np.average(probs_next, axis=0)
    prior_probs_next_dict = {_statespace[0]: prior_probs_next,
                                     _statespace[1]: np.subtract(1.0, prior_probs_next)}

    # print 'debug: prob_node_state.values() =', prob_node_state_per_node[0].values()
    if not sum(prior_prob_initstate_per_node[0].values()) == 1.0:
        print 'debug: prob_node_state.values() =', prior_prob_initstate_per_node[0].values()
        print 'debug: sum(prob_node_state.values()) =', sum(prior_prob_initstate_per_node[0].values())
    assert sum(prior_prob_initstate_per_node[0].values()) == 1.0

    equil_ents_next = map(H, prior_probs_next)

    assert len(equil_ents_next) == graph_numbered.number_of_nodes()

    # print 'debug: min prior_probs_next =', min(prior_probs_next)

    if __debug__:
        cond_dbg = np.average(cond_probs_next_per_node[0][_statespace[0]], axis=0)

        assert len(cond_dbg) == graph_numbered.number_of_nodes()
        assert np.isscalar(cond_dbg[0])
        assert np.isfinite(cond_dbg[0])

    if given_initial_state_node == False:
        def log_term_state(nodelbl, state, other_state):
            return np.multiply(prior_prob_initstate_per_node[nodelbl][state], np.average(cond_probs_next_per_node[nodelbl][other_state], axis=0)) * \
                   np.log2(np.multiply(prior_prob_initstate_per_node[nodelbl][state], np.average(cond_probs_next_per_node[nodelbl][other_state], axis=0))
                           / np.multiply(prior_prob_initstate_per_node[nodelbl][state], prior_probs_next_dict[other_state]))

        # def log_term_state2(nodelbl, state, other_state, prior_probs_next):
        #     return np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - np.average(cond_probs_next_per_node[nodelbl][state], axis=0)) * \
        #            np.log2(np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - np.average(cond_probs_next_per_node[nodelbl][state], axis=0))
        #                    / np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - prior_probs_next))

        # mis[node_id][other_node_id] = mutual information between node_id's initial state (t=0) and
        # other_node_id's next state (t=1)
        mis = [sum([sum([log_term_state(nodelbl, state, other_state=other_next_state) for other_next_state in _statespace])
                    for state in _statespace])
               for nodelbl in graph_numbered.nodes_iter()]

        # safety, prevent these helper functions from being used somewhere else; they are very specific
        log_term_state1 = None
        log_term_state2 = None

        # mis = [sum([np.multiply(prob_node_state_per_node[nodelbl][state], np.average(cond_probs_next_per_node[nodelbl][state], axis=0)) *
        #            np.log2(np.multiply(prob_node_state_per_node[nodelbl][state], np.average(cond_probs_next_per_node[nodelbl][state], axis=0))
        #                    / np.multiply(prob_node_state_per_node[nodelbl][state], prior_probs_next))
        #             for state in _statespace]) + \
        #     sum([np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - np.average(cond_probs_next_per_node[nodelbl][state], axis=0)) *
        #            np.log2(np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - np.average(cond_probs_next_per_node[nodelbl][state], axis=0))
        #                    / np.multiply(prob_node_state_per_node[nodelbl][state], 1.0 - prior_probs_next))
        #             for state in _statespace])
        #        for nodelbl in graph_numbered.nodes_iter()]
    else:
        def log_term_state_given_state(nodelbl, state, other_state, given_state):
            if given_state == state:
                prior_state_prob = 1.0
                return np.multiply(prior_state_prob, np.average(cond_probs_next_per_node[nodelbl][other_state], axis=0)) * \
                       np.log2(np.multiply(prior_state_prob, np.average(cond_probs_next_per_node[nodelbl][other_state], axis=0))
                               / np.multiply(prior_state_prob, prior_probs_next_dict[other_state]))
            else:
                return 0.0  # 0 log 0 == 0 (assumed, as is usual)

        # mis[node_id][given_node_state][other_node_id] = mutual information between node_id's initial state (t=0) and
        # other_node_id's next state (t=1), given the fact that the node_id's initial state (t=0) equals
        # given_node_state
        mis = [[sum([sum([log_term_state_given_state(nodelbl, state, other_state=other_next_state, given_state=given_state)
                          for other_next_state in _statespace])
                    for state in _statespace])
                for given_state in _statespace]
               for nodelbl in graph_numbered.nodes_iter()]

    assert len(mis) == graph_numbered.number_of_nodes()
    if __debug__ and not given_initial_state_node:
        for mi in mis:
            if not np.isscalar(mi[0]):
                print 'error: mi =', mi
                print 'error: lens =', map(len, mis)
                assert False
            assert np.isfinite(mi[0])
            if not 0.0 <= mi[0] <= H(1.0 / len(_statespace)):
                print 'debug: mi =', mi
            assert 0.0 <= mi[0] <= H(1.0 / len(_statespace))

    ipm = InformationProcessingMeasures()

    ipm.mi_pairwise_initstate_nextstate = mis
    ipm.entropy_initstate_node = [Hset(prior_prob_initstate_per_node[nodelbl].values())
                             for nodelbl in graph_numbered.nodes_iter()]
    ipm.entropy_nextstate_node = [Hset([prior_probs_next_dict[s][nodelbl] for s in _statespace])
                             for nodelbl in graph_numbered.nodes_iter()]

    ipm.given_initial_state = given_initial_state_node

    return ipm


def update_states_steps(graph, states, T, numsteps, trans='async', inplace=True):
    """

    :param graph:
    :param states:
    :param T:
    :param numsteps:
    :param trans:
    :param inplace:
    :return: list of system states, from next to newest  (so `numsteps` system states of length len(states))
    :rtype: list of list
    """
    if not inplace:
        if type(states) == dict:
            cur_states = states.copy()
        else:
            cur_states = list(states)
    else:
        cur_states = states

    # states_sequence = []
    #
    # for i in xrange(numsteps):
    #     new_st = update_states_step(graph, cur_states, T, trans=trans, inplace=True)
    #     states_sequence.append(copy.deepcopy(new_st))
    states_sequence = np.zeros((numsteps+1, len(cur_states)), dtype=_state_type)
    states_sequence[0] = np.array(cur_states, dtype=_state_type)
    # states_sequence[1] = np.array(cur_states, dtype=_state_type)
    # update_states_step(graph, states_sequence[1], T, trans=trans, inplace=True)

    for ix in xrange(1, len(states_sequence)):
        states_sequence[ix] = states_sequence[ix-1]
        update_states_step(graph, states_sequence[ix], T, trans=trans, inplace=True)

    if inplace:
        # copy state through by-ref argument
        for ix in xrange(len(cur_states)):
            cur_states[ix] = states_sequence[-1][ix]

    return states_sequence


if __debug__:
    _debug_last_node_flipped = None

def update_states_step(graph, states, T, trans='async', inplace=False):
    """
Note: you can pass a list as 'states' if and only if you pass a 'numbered' graph as 'graph', which means the node ids
in that graph are integers in [0, size), like graph2csv.to_numbered_graph would return. Otherwise, pass a dict as
'states'.
    :param graph:
    :param states: in/out
    :param T:
    :param trans:
    :param inplace: change 'states' by reference, or copy it and return a new dict/list (preserving 'states')
    :return: dict or list of new states
    """

    assert trans in ('single', 'sync', 'async')

    if type(states) == dict:
        if not inplace or trans == 'sync':
            new_states = states.copy()
        else:
            new_states = states  # copy by reference
    else:
        if not inplace or trans == 'sync':
            new_states = list(states)  # copy by value
        else:
            new_states = states  # copy by reference

        if __debug__:
            if np.random.randint(10) == 1:
                assert g2c.is_numbered_graph(graph)

    if trans == 'sync':
        nodeids = graph.nodes()

        # # makes no sense because how can I change intermediate steps in 'states' but still avoid doing 'async'?
        # assert inplace == False
        # note: fixed this with an intermediate copy...
    elif trans == 'async':
        nodeids = graph.nodes()
        nodeids = np.random.permutation(nodeids)
    else:
        nodeids = [np.random.choice([n for n in graph.nodes() if n >= 0])]

    # negative node ids are assumed to have fixed states
    nodeids = [n for n in nodeids if n >= 0]

    assert len(nodeids) > 0, 'nothing to update?? strange'

    for node in nodeids:
        if trans == 'sync':
            # probs_states_new = [prob_state(graph, node, states, T, s) for s in _statespace]
            probs_states_new = prob_states(graph, node, states, T)
        elif trans == 'async' or trans == 'single':
            # probs_states_new = [prob_state(graph, node, new_states, T, s) for s in _statespace]
            probs_states_new = prob_states(graph, node, new_states, T)

        # # note: never hit these checks, but cost 10% time, so comment them out
        # if not np.all(np.isfinite(probs_states_new)):
        #     assert node in (-1, -2), 'only the \'nudger\' nodes are expected to have h=(+/-)inf'
        #
        #     # assume that the NaNs are actually infinities so make that prob=1.0 and
        #     # the rest 0 (also I assume only one NaN is present)
        #     probs_states_new = map(float, np.logical_not(np.isfinite(probs_states_new)))
        #
        #     assert abs(1.0 - sum(probs_states_new)) < 0.001, 'debug: node=%s, probs=%s' % (node, str(probs_states_new))
        # else:
        #     assert abs(1.0 - sum(probs_states_new)) < 0.001, 'debug: node=%s, probs=%s' % (node, str(probs_states_new))

        if __debug__:
            _debug_old_state = new_states[node]

        new_states[node] = np.random.choice(_statespace, p=probs_states_new)

        # if __debug__:
        #     if new_states[node] != _debug_old_state:
        #         _debug_last_node_flipped = True
        #     else:
        #         _debug_last_node_flipped = False

        if inplace and not trans == 'sync':
            states[node] = new_states[node]

    if trans in ('sync',) and inplace == True:
        for i in xrange(len(states)):
            states[i] = new_states[i]

    # looking for specific bug...
    # if type(new_states) == dict:
    #     assert min(new_states.values()) in _statespace
    #     assert max(new_states.values()) in _statespace
    # else:
    #     assert min(new_states) in _statespace
    #     assert max(new_states) in _statespace

    return new_states


def update_states_step_probs(graph, states, T, verbose=True, trans='async'):
    """
Note: you can pass a list as 'states' if and only if you pass a 'numbered' graph as 'graph', which means the node ids
in that graph are integers in [0, size), like graph2csv.to_numbered_graph would return. Otherwise, pass a dict as
'states'.
    :param graph:
    :param states:
    :param T:
    :param verbose:
    :return: list of probabilities
    """
    new_state_probs = [-1.0]*graph.number_of_nodes()
    node_labels = graph.nodes()

    # if verbose:
    #     print 'debug: # edges = ', graph.number_of_edges()
    #     print 'debug: # nbrs =', [len(graph.predecessors(n)) for n in graph.nodes()]

    if trans == 'sync':
        nodeids = range(len(node_labels))
    elif trans == 'async':
        nodeids = range(len(node_labels))
        nodeids = np.random.permutation(nodeids)

        # I stopped this because makes little sense, how can I compute the probabilities in the async case, sum over
        # all possible orderings of node updates? Maybe sticking with synchronous is easier...
        assert False
    else:
        assert False

    for nodeid in nodeids:
        assert type(nodeid) == int
        assert 0 <= nodeid < graph_numbered.number_of_nodes()

        probs_states_new = [prob_state(graph, node_labels[nodeid], states, T, s) for s in _statespace]

        # if verbose:
        #     print 'debug: update_states_step_probs: probs_states_new =', probs_states_new

        assert abs(1.0 - sum(probs_states_new)) < 0.001

        assert len(_statespace) == 2  # otherwise the next line is too much of a simplification
        new_state_probs[nodeid] = probs_states_new[0]

    return new_state_probs


def impact_of_nudge(graph, states, T, numsteps, added_energy, verbose=True, trans='async', ntrials=1000):

    # for now, I only pass a list as nudged temperature; otherwise, also support dict so that T[node] works
    assert g2c.is_numbered_graph(graph)

    # normal_probs_at_step = np.zeros(numsteps)
    # nudged_probs_at_step = np.zeros(numsteps)

    impact_at_step = [-1]*numsteps

    if __debug__:
        prev_normal_probs_at_step = []
        prev_nudged_probs_at_step = []

    # if __debug__:
    #     prev_debug_calls_energy_T_scalar = _debug_calls_energy_T_scalar
    #     prev_debug_calls_energy_T_list = _debug_calls_energy_T_list

    print 'debug: T =', T
    print 'debug: T + nudge =', np.add(T, added_energy)

    for step in xrange(numsteps):
        normal_probs_at_step = update_states_steps_probs(graph, states, T, numsteps=step, verbose=verbose,
                                                               trans=trans, ntrials=ntrials)
        nudged_probs_at_step = update_states_steps_probs(graph, states, np.add(T, added_energy), numsteps=step,
                                                         verbose=verbose, trans=trans,
                                                         ntrials=ntrials)

        # would be really weird if the probabilities obtained through sampling just so happen to ALL be EXACTLY
        # the same... (looking for possible bug, because the average nudge impacts in the output CSV are all
        # exactly the same as function of 'step')
        if __debug__:
            if prev_nudged_probs_at_step != [] and prev_normal_probs_at_step != [] and 10 < ntrials < 20000 and \
                    len(states) > 10:
                assert normal_probs_at_step != prev_normal_probs_at_step
                assert nudged_probs_at_step != prev_nudged_probs_at_step

        assert len(nudged_probs_at_step) > 0
        assert np.isscalar(nudged_probs_at_step[0])
        assert len(normal_probs_at_step) > 0
        assert np.isscalar(normal_probs_at_step[0])

        impact_at_step[step] = np.average(np.power(np.subtract(normal_probs_at_step, nudged_probs_at_step), 2))
        assert np.isscalar(impact_at_step[step])

        print 'debug: at step =', step, 'the nudge impact is', impact_at_step[step]

        # finding bug
        normal_probs_at_step = np.nan
        nudged_probs_at_step = np.nan

        if __debug__:
            prev_normal_probs_at_step = normal_probs_at_step
            prev_nudged_probs_at_step = nudged_probs_at_step

    # if __debug__:
    #     assert _debug_calls_energy_T_scalar - prev_debug_calls_energy_T_scalar > 0
    #     assert _debug_calls_energy_T_list - prev_debug_calls_energy_T_list > 0

        # see if this holds
        # if not _debug_calls_energy_T_list - prev_debug_calls_energy_T_list == \
        #        _debug_calls_energy_T_scalar - prev_debug_calls_energy_T_scalar:
        #     print 'debug: _debug_calls_energy_T_list =', _debug_calls_energy_T_list
        #     print 'debug: _debug_calls_energy_T_scalar =', _debug_calls_energy_T_scalar
        #     print 'debug: prev_debug_calls_energy_T_list =', prev_debug_calls_energy_T_list
        #     print 'debug: prev_debug_calls_energy_T_scalar =', prev_debug_calls_energy_T_scalar
        #
        #     assert False

    return impact_at_step


# TODO: in some other function, perturb a node's state, get the 'changed' probs dist for each node at time t+d,
# do KL-div for each node in the unperturbed and perturbed case, sum them up, and voila, impact of nudging :-).
def update_states_steps_probs(graph, states, T, numsteps, verbose=True, trans='sync', ntrials=1000):
    assert numsteps >= 0

    # print 'debug: update_states_steps_probs: numsteps =', numsteps, ', ntrials =', ntrials, ', type(T) =', type(T)

    new_state_problists = []

    for trial in xrange(ntrials):
        if type(states) == dict:
            new_states = states.copy()
        else:
            new_states = list(states)

        # first sample a system state at numsteps-1 timesteps, starting from 'states'
        for i in xrange(numsteps - 1):
            update_states_step(graph, new_states, T, trans=trans, inplace=True)

        new_state_problists.append(update_states_step_probs(graph, states, T, verbose=True, trans='sync'))

    new_state_problists = np.transpose(new_state_problists)

    assert len(new_state_problists) == graph.number_of_nodes()
    if __debug__:
        if graph.number_of_nodes() > 0:
            assert len(new_state_problists[0]) == ntrials

    new_state_problists = map(np.average, new_state_problists)

    return new_state_problists


def hellinger_distance(probs1, probs2):
    return np.linalg.norm(np.sqrt(probs1) - np.sqrt(probs2)) / np.sqrt(2)


_possible_analyses = ('single_temp', 'many_temps', 'pca', 'fit_temp', 'nudge')


class IsingNetwork:
    T = 1.0
    size = 100
    states = [_statespace[0]]*size
    graph_numbered = None
    # # -1 is the positive node, -2 is the negative node

    def __init__(self, graph=0.3, size=None, T='auto', states='random', weights=None, hs=None, directed=True):
        # self.T = T

        if size is None and (isinstance(graph, Number) or isinstance(graph, basestring)):
            size = 100

        if not isinstance(graph, Number) and not isinstance(graph, basestring):
            self.size = graph.number_of_nodes()  # I presume a graph is supplied
            # note: set_graph() will overwrite this
        else:
            self.size = size

        self.set_graph(graph, size, weights=weights, hs=hs, directed=directed)

        self.reset_states(states=states)

        self.T = self.temp_matching_entropy(1.0/np.e)  # not too low (frozen), not too high (totally random)

        return


    def set_graph(self, graph, size, directed=True, weights=None, hs=None):

        assert directed, 'I think that further code assumes nx.DiGraph because of use of .predecessors(), which is' \
                         ' apparently not defined for nx.Graph() [so either everywhere use either redecessors ' \
                         'or neighbors, or insert an undirected graph by copying each edge in both directions]'

        # self.nudgers = []  # reset nudgers too

        # if type(graph) in (int, float, np.float64, np.float32, np.float128, np.float16):
        if isinstance(graph, Number):
            ### create a random, binomial graph with specified probability per edge

            assert 0.0 <= float(graph) <= 1.0

            graph = nx.binomial_graph(size, float(graph), directed=directed)
            self.graph_numbered = g2c.to_numbered_graph(graph)
        elif type(graph) in (str, unicode):
            ### read the network structure from a file, in adjacency list format

            if directed:
                graph = nx.read_adjlist(graph, create_using=nx.DiGraph())
            else:
                graph = nx.read_adjlist(graph, create_using=nx.Graph())

            self.graph_numbered = g2c.to_numbered_graph(graph)
        else:
            ### assume a network structure is specified, copy it as-is

            self.graph_numbered = graph

            assert type(graph) in (nx.Graph, nx.DiGraph)

        # add weights to each edge
        if not weights is None:
            for n1 in self.graph_numbered.edge.iterkeys():
                for n2 in self.graph_numbered.edge[n1].iterkeys():
                    if np.isscalar(weights):
                        if weights == 'normal':
                            self.graph_numbered.edge[n1][n2]['weight'] = np.random.randn()
                        else:
                            self.graph_numbered.edge[n1][n2]['weight'] = float(weights)
                    elif len(weights) == 2:  # assume [min, max]
                        self.graph_numbered.edge[n1][n2]['weight'] = np.random.uniform(*weights)
                    else:
                        raise NotImplementedError('I do not understand weights=%s'
                                                  % str(weights))
        else:
            pass  # equivalent to weights=1

        # add external magnetization to each node
        if not hs is None:
            for node in self.graph_numbered.nodes_iter():
                if np.isscalar(hs):
                    if hs == 'normal':
                        self.graph_numbered.node[node]['h'] = np.random.randn()
                    else:
                        self.graph_numbered.node[node]['h'] = float(hs)
                elif len(hs) == 2:
                    self.graph_numbered.node[node]['h'] = np.random.uniform(*hs)
                else:
                    raise NotImplementedError('I do not understand hs=%s'
                                              % str(hs))
        else:
            pass  # equivalent to hs=0

        self.size = self.graph_numbered.number_of_nodes()

        if self.size != len(self.states):
            self.reset_states()

        return


    def reset_states(self, states='random'):
        if not type(self.states) == dict:
            if isinstance(states, basestring):
                if states == 'random':
                    self.states = [np.random.choice(_statespace) for node in xrange(self.size)]
                elif states == 'uniform':
                    self.states = [_statespace[0] for node in xrange(self.size)]
                else:
                    raise NotImplementedError()
            else:
                assert len(states) == self.size
                self.states = states
        else:
            if states == 'random':
                self.states = {nix: np.random.choice(_statespace) for nix in self.states.iterkeys() if nix >= 0}
                self.states.update({nix: np.random.choice(_statespace) for nix in self.states.iterkeys() if nix < 0})
            elif states == 'uniform':
                self.states.update({nix: _statespace[0] for nix in self.states.iterkeys() if nix >= 0})
            elif hasattr(states, '__iter__'):
                self.states.update({nix: states[nix] for nix in self.states.iterkeys() if nix >= 0})
            else:
                self.states = dict(states)

        if self.graph_numbered:
            assert self.graph_numbered.number_of_nodes() == self.size \
                   or self.graph_numbered.number_of_nodes() == self.size + 2


    def remove_all_nudges(self):
        '''
        Removes the two nudger nodes -1 and -2 and all their edges to the regular nodes.
        :return:
        '''
        if not self.graph_numbered.has_node(-1):
            raise UserWarning('no nudges have been applied to the system')
        else:
            self.graph_numbered.remove_node(-1)
            self.graph_numbered.remove_node(-2)


    def add_nudge_to(self, node, weights):
        '''
        Adds a permanent nudge to a node. This means that the energy levels (probabilities)
        of the possible states of a node are slightly changed by adding a fixed total amount
        of energy to the states.

        This is implemented by adding two 'nudger' nodes (ids -1 and -2) which have h=np.inf
        and h=-np.inf as their local external magnetization, so they are fixated, and then
        adding directed edges from these nodes to `node` with the specified edge `weights`
        in the order (-1, -2). These weights are scalars and are expected to be non-negative. When
        adding nudges to multiple nodes, the same 'nudger' nodes -1 and -2 are used.
        :param node: a node identifier into self.graph_numbered
        :param weights: tuple of two scalars, in the order ('+1 energy', '-1 energy')
        :return:
        '''
        # if len(self.nudgers) == 0:  # indicates no nudger nodes have been created yet
        #     assert not self.graph_numbered.has_node(-1), \
        #         '-1 id is reserved for positive nudger node'
        #     assert not self.graph_numbered.has_node(-2), \
        #         '-1 id is reserved for negative nudger node'

        # make this a dict so that -1 and -2 are also indexed
        # (kind of defeats the [efficiency] purpose of making the graph numbered, but ok... worry about that later)
        if type(self.states) == dict:
            # self.states = {nix: s for nix, s in enumerate(self.states.itervalues())}
            pass  # already converted
        else:
            # convert list (or list-like) to dict
            self.states = {nix: s for nix, s in enumerate(self.states)}

        # create nudger nodes if they do not yet exist
        if not self.graph_numbered.has_node(-1):
            self.graph_numbered.add_node(-1, attr_dict={'h': np.inf})
            self.states[-1] = 1
        if not self.graph_numbered.has_node(-2):
            self.graph_numbered.add_node(-2, attr_dict={'h': -np.inf})
            self.states[-2] = -1

        assert self.graph_numbered.has_node(node), 'node=%s does not exist' % node

        assert len(weights) == 2, "expected tuple of two scalars, in the order ('+1 energy', '-1 energy')"

        self.graph_numbered.add_edge(-1, node, attr_dict={'weight': weights[0]})
        self.graph_numbered.add_edge(-2, node, attr_dict={'weight': weights[1]})


    def entropy_next_vs_temp(self, Ts='auto', equilibrate=True):

        avg_in_weight = np.mean(np.abs(np.transpose(list((self.graph_numbered.in_degree_iter(weight='weight'))))[-1]))

        if avg_in_weight <= 0.0:
            avg_in_weight = 1.0

        if isinstance(Ts, basestring):
            Ts = np.linspace(avg_in_weight / 100., 4.0 * avg_in_weight, 20)

        if equilibrate:
            self.equilibrate(10)

        H_per_temp = []
        for temp in Ts:
            probs = [[prob_state(self.graph_numbered, nix, self.states, T=temp, state=s)
                      for s in _statespace]
                     for nix in self.graph_numbered.nodes_iter() if nix >= 0]
            H_per_temp.append(np.mean(map(Hset, probs)))

        return np.array(Ts), np.array(H_per_temp)


    def temp_matching_entropy(self, H=0.5, Ts='auto'):
        """
        Returns the minimum temperature such that the average entropy of a node's next state (from current
        system state) equals `H`.
        :param H: 0 <= H <= 1
        :param Ts: 'auto' or a list of temperature values
        :rtype: float
        """
        Ts, Hs = self.entropy_next_vs_temp(Ts=Ts)

        return min(np.array(Ts)[np.greater_equal(Hs, H)])


    def lifetime_of_nudge(self, node, weights, num_steps_per_node=5, num_repeats=100, num_cur_states=100,
                          trans='async', include_self=True,
                          sort_probs=False, nprocs=1):
        """
        Performs a nudge according to ``weights``, equilibrates, then releases the nudge, then measures the impact
        between the nudged and un-nudged system as function of time steps since release of the nudge.
        :param node:
        :param weights:
        :param steps_per_node:
        :param trans:
        :param include_self:
        :param sort_probs:
        :return:
        """

        # note: do not do a nudge and then equilibrate, but give a 'pulse' of energy just before the 'current' state?
        assert False, 'work in progress'

        states_before = copy.deepcopy(self.states)

        self_nudged = copy.deepcopy(self)
        self_nudged.add_nudge_to(node, weights)  # make a copy with the nudge applied

        # # 'forget' the initial state where the nudge has had no impact yet on other nodes
        # self.equilibrate()
        # self_nudged.equilibrate()

        # TODO: prevent duplicate current states with something like: np.any(np.all(self.states == cur_states_self, axis=1))
        # but only if you account for the likelihood of each system state, otherwise each system state gets implicitly
        # the same likelihood

        assert num_cur_states >= 1

        ### SAMPLE 'current' states

        cur_states_self = []
        cur_states_nudged = []

        for csix in xrange(num_cur_states):
            self.equilibrate()
            self_nudged.equilibrate()

            cur_states_self.append(self.states)
            cur_states_nudged.append(self_nudged.states)

        ### SIMULATE self (not nudged)

        num_steps = num_steps_per_node * self.size

        pool = mp.Pool(nprocs)

        def worker_mi_over_time(cs):
            return [self.go(num_steps, cur_states=cs, trans=trans)
                    for _ in range(num_repeats)]

        # shape: (num_cur_states, num_repeats, num_steps+1)
        states_sequences_self = pool.map(worker_mi_over_time, cur_states_self)

        ### SIMULATE nudged case

        # NOTE: here I use self instead of self_nudged

        states_sequences_self = pool.map(worker_mi_over_time, range(num_repeats))

        pool.close()
        pool.terminate()

        ### analyze


    def impact_of_nudge(self, node, weights, steps_per_node=20000, trans='async', include_self=True,
                        sort_probs=False):
        """
        Measures the impact of adding a nudge to `node`. The nudge is characterized by `weights` (see `add_nudge_to()`).
        This nudge is applied to a copy and then discarded; thus `self` remains unchanged.

        The impact is measured as the sum of hellinger distances between the state distributions of each node. It lies
        in the interval [0, N] where N is the number of nodes in the system.
        :param node:
        :param weights:
        :param steps_per_node:
        :param trans:
        :param sort_probs: in the Ising model without external magnetization the two states -1 and +1 are
        symmetric and interchangeable, i.e., relabeling all the node states results in the same system. Sorting the
        probabilities takes this into account, meaning fewer steps can be used to achieve the same accuracy.
        Especially in the presence of bi-modality it takes a large `steps_per_node` to ensure that both
        node state distributions are fully mixed.
        :return: sum of Hellinger distances
        :rtype: float
        """
        states_before = copy.deepcopy(self.states)

        self_nudged = copy.deepcopy(self)
        self_nudged.add_nudge_to(node, weights)  # make a copy with the nudge applied

        # 'forget' the initial state where the nudge has had no impact yet on other nodes
        self.equilibrate()
        self_nudged.equilibrate()

        # now try to estimate the stationary distribution of each node's state
        probs1 = self.state_distributions(steps_per_node=steps_per_node, trans=trans, return_also_variance=False)
        probs2 = self_nudged.state_distributions(steps_per_node=steps_per_node, trans=trans, return_also_variance=False)

        if sort_probs:
            impact = sum([hellinger_distance(sorted(probs1[nix]), sorted(probs2[nix]))
                          for nix in xrange(len(probs1)) if include_self or nix != node])
        else:
            impact = sum([hellinger_distance(probs1[nix], probs2[nix])
                          for nix in xrange(len(probs1)) if include_self or nix != node])

        # todo: support also returning the uncertainty of impact, maybe by letting state_distributions
        # also return variance and assuming that the probs are normally distributed? Then renormalize...

        self.states = states_before  # restore the state of the system that I started with

        return impact


    def next(self, trans='async'):

        assert len(self.states) == self.size and not self.graph_numbered.has_node(-1) \
               or len(self.states) == self.size + 2 and self.graph_numbered.has_node(-1), \
            'check if presence of nudger nodes does not screw this'

        new_states = update_states_step(self.graph_numbered, self.states, self.T, trans=trans, inplace=True)

        if type(new_states) == dict:
            return {nix: s for nix, s in new_states.iteritems() if nix >= 0}
        else:
            return new_states

        # tests go fine for a very long time, skip them most of the time...
        # if not __debug__ or np.random.randint(100) < 95:
        #     update_states_step(self.graph_numbered, self.states, self.T, trans=trans, inplace=True)
        # else:
        #     old_states = self.states
        #
        #     new_states = update_states_step(self.graph_numbered, self.states, self.T, trans=trans, inplace=True)
        #
        #     assert self.states == new_states
        #
        #     if _debug_last_node_flipped == True:
        #         assert old_states != self.states
        #     else:
        #         assert old_states == self.states
        #
        #     if trans=='single':
        #         assert abs(np.subtract(self.states, old_states).sum()) <= 2, \
        #             'only one spin should flip at a time, but apparently more flipped.\n' \
        #             'old state: ' + str(old_states) + '\nnew state: ' + str(self.states) \
        #             + '\ndifference in sum of spins: ' + str(np.subtract(self.states, old_states).sum())


    def go(self, dt=1, cur_states=None, trans='async'):
        if dt == 0:
            return []
        elif dt < 0:
            if trans in ('async', 'single'):
                dt = abs(dt)  # going forward is the same as going backward, statistically
            else:
                # I think 'sync' depends on the current state? If so then...
                raise NotImplementedError('Don\'t know how to go back in trans=sync mode!')

        if cur_states is None:
            new_states_list = update_states_steps(self.graph_numbered, self.states, self.T, numsteps=dt, trans=trans, inplace=True)

            if len(new_states_list) > 0:
                if type(new_states_list[0]) == dict:
                    # remove the states for the nudger nodes (which are fixed and meaningless)
                    return [{nix: s for nix, s in d.iteritems() if nix >= 0}
                            for d in new_states_list]
                else:
                    return new_states_list
            else:
                return new_states_list
        else:
            cur_states2 = list(cur_states)  # prevent overwriting caller's variable
            new_states_list = update_states_steps(self.graph_numbered, cur_states2, self.T,
                                                  numsteps=dt, trans=trans, inplace=True)

            if len(new_states_list) > 0:
                if type(new_states_list[0]) == dict:
                    # remove the states for the nudger nodes (which are fixed and meaningless)
                    return [{nix: s for nix, s in d.iteritems() if nix >= 0}
                            for d in new_states_list]
                else:
                    return new_states_list
            else:
                return new_states_list


    def state(self):
        '''
        Same as `self.states` except that the nudger nodes are omitted in the dictionary format
        :return:
        '''
        if type(self.states) == dict:
            # remove the states for the nudger nodes (which are fixed and meaningless)
            return {nix: s for nix, s in self.states.iteritems() if nix >= 0}
        else:
            return self.states


    def equilibrate(self, steps_per_node=20, trans='async'):
        if trans == 'single':
            self.go(self.size * steps_per_node)
        else:
            self.go(steps_per_node)

        return self.states


    def state_distributions(self, steps_per_node=10000, trans='async', return_also_variance=False):
        '''

        :param steps_per_node:
        :param trans:
        :return: array of (#nodes, #states) with average probabilities per state per node
        '''
        n = self.size * steps_per_node if trans == 'single' else steps_per_node
        state_probs = np.zeros((self.size, len(_statespace)))
        state_probs_2 = np.zeros((self.size, len(_statespace)))

        for i in xrange(n):
            probs = [[prob_state(self.graph_numbered, nix, self.states, T=self.T, state=s)
                      for s in _statespace]
                     for nix in self.graph_numbered.nodes_iter() if nix >= 0]
            state_probs += probs
            if return_also_variance:
                state_probs_2 += np.power(probs, 2)

            # self.next(trans=trans)
            # now that the probabilities per state for all nodes have already been computed, bypass
            # the .next() function (slow) and directly draw new states:
            ix = 0  # note: `ix` and `nix` are probably always equal but just in case
            for nix in self.graph_numbered.nodes_iter():
                if nix >= 0:
                    self.states[nix] = np.random.choice(_statespace, p=probs[ix])
                    ix += 1
            # note: I do it explicitly in a loop above because self.states could be dict or list

        if not return_also_variance:
            return state_probs / n
        else:
            return state_probs / n, state_probs_2 / n - (state_probs / n)**2.0


    def state_distributions_after(self, dt=1, steps_per_node=10000, trans='async'):
        '''
        Gives the distribution of states of each node for the system `self` at time t, so S[t], where the
        current state (`self.states`) is called S[0].
        :param dt:
        :param steps_per_node:
        :param trans: using 'async' is faster than 'single'
        :return: array of (#nodes, #states) with average probabilities per state per node
        '''
        n = self.size * steps_per_node if trans == 'single' else steps_per_node
        state_probs = np.zeros((self.size, len(_statespace)))

        original_state = copy.deepcopy(self.states)

        for i in xrange(n):
            self.go(dt-1, trans=trans)

            probs = [[prob_state(self.graph_numbered, nix, self.states, T=self.T, state=s)
                      for s in _statespace]
                     for nix in self.graph_numbered.nodes_iter() if nix >= 0]
            state_probs += probs

            # reset state and do it again
            self.states = copy.deepcopy(original_state)

        return state_probs / n


    def read_graph_from_csv(self, fn_edge_weights, fn_node_weights):
        """

        :param fn_edge_weights: format: first row and column are index (directed)
        :param fn_node_weights: format: first column is index, first row is also index
        (like "node, externalField")
        """

        matrix_weights = pd.read_csv(fn_edge_weights, index_col=0)

        vector_hs = pd.read_csv(fn_node_weights, index_col=0, header=0)
        vector_hs = vector_hs['externalField']
        vector_hs_list = [vector_hs[col] for col in matrix_weights.columns]

        graph = nx.from_numpy_matrix(np.array(matrix_weights), create_using=nx.DiGraph())

        for colix, col in enumerate(matrix_weights.columns):
            graph.node[colix]['h'] = vector_hs_list[colix]

        self.graph_numbered = graph
        self.size = graph.number_of_nodes()


    def average_absolute_magnetization(self, numsteps=50, trans='async'):

        mags = []

        if trans == 'single':
            for step in xrange(numsteps):
                self.go(self.size)

                mags.append(np.sum(self.states) / float(self.size))
        else:
            for step in xrange(numsteps):
                self.go(1)

                mags.append(np.sum(self.states) / float(self.size))

        return (np.mean(map(np.abs, mags)), np.std(map(np.abs, mags)))


    def __str__(self):
        return str(self.states)


def average_absolute_magnetization(network_fn, T, numsteps=50, trans='async'):
    """
    Helper function to determine the expected average absolute magnetization for an Ising spin network
    at a given temperature (or set of temperatures).
    :param network_fn: .graphml
    :param T: float or enumerable
    :param numsteps:
    :param trans:
    :return: dict with temperatures as keys and absolute magnetizations as values
    :rtype: dict of float
    """

    if np.isscalar(T):
        T = (T,)

    if '.graphml' in network_fn or '.gml' in network_fn:
        graph = nx.read_graphml(network_fn)
    else:
        graph = nx.read_adjlist(network_fn, create_using=nx.DiGraph())

    graph = nx.convert_node_labels_to_integers(graph)

    abs_mag_per_temp = dict()

    for temp in T:
        system = IsingNetwork(graph, T=temp)

        system.equilibrate(trans=trans)

        abs_mag = system.average_absolute_magnetization(numsteps=numsteps, trans=trans)

        abs_mag_per_temp[temp] = abs_mag

    return abs_mag_per_temp


if __name__ == "__main__":

    T = 0.5
    analysis = _possible_analyses[0]
    L = 75

    parser = argparse.ArgumentParser(description='Compute the reproduction numbers R0 for each node in a dynamical'
                                                 ' system.')

    parser.add_argument('-L', help='Size of the lattice is LxL.', type=int, default=10)
    parser.add_argument('--size', help='Size of the network.', type=int, default=100)
    parser.add_argument('-Ts', nargs=3, type=float, default=[0.1, 1.0, 0.1],
                        help='Specify the array of temperature values to use as a list of 3 values, space-separated: '
                             '<start> <stop> <step> (like numpy.arange).')
    parser.add_argument('-T', help='Specify a single temperature value.', type=float, default=1.0)
    parser.add_argument('--flip_prob', help='Specify a flipping fraction: temperature will be fitted to match this.',
                        type=float, required=False)
    parser.add_argument('--num_equil_steps', '--equil', '--num_equil',
                        help='Number of time steps to evolve the system starting from a random initial confguration'
                             ' in order to equilibrate the system.', default=[0], nargs='+', type=int)
    parser.add_argument('--analysis', help='Specify the analysis to perform. Must be one of: '+str(_possible_analyses),
                        default=_possible_analyses[0])
    parser.add_argument('--dynamics', help='Specify the rule of dynamics to use for state evolution of nodes. '
                                           'Must be one of: ' + str(_possible_dynamics),
                        default=_possible_dynamics[0])
    parser.add_argument('--ntrials', help='Per initial state, how many simulations (state trajectory samples)'
                                          ' should be gathered, in order to estimate (conditional) probabilities?',
                        default=1000, type=int)
    parser.add_argument('--numruns', '--nruns', help='Number of initial states to sample, each of which I will'
                                                     'perform <ntrials> simulations for.',
                        default=1, type=int)
    parser.add_argument('--numinits', '--num_inits', '--num_init_states', help='Number of (random) initial system '
                                                                               'states'
                                                                               'to sample.',
                        default=1, type=int)
    parser.add_argument('--nudge_temp', help='Magnitude of the nudge to perform on a node. This quantity will be added'
                                             ' to the temperature (-T) of a given node. May be negative.',
                        type=float, required=False)
    parser.add_argument('--nudge_nodeids', help='Explicitly specify the node ids, in [0, N), space-separated, '
                                                'for which I will try to nudge and measure the impact of it.',
                        type=int, nargs='+', required=False)
    parser.add_argument('--network', help='Filename storing a network structure, which I should import.',
                        type=str, required=False)
    parser.add_argument('--numsteps', '--steps', help='Number of steps to perform (meaning depends on --analysis).',
                        type=int, required=False)


    args = parser.parse_args()

    # parse arguments passed as command-line args
    temps = np.arange(args.Ts[0], args.Ts[1], args.Ts[2], dtype=float)
    analysis = args.analysis
    _dynamics = args.dynamics
    num_equil_steps_list = args.num_equil_steps
    num_equil_steps = num_equil_steps_list[0]
    T = args.T
    L = args.L
    ntrials = args.ntrials
    numruns = args.numruns
    num_init_states = args.numinits
    size = args.size

    if not args.network:
        # create the network of interactions among the nodes (for now: square lattice)
        graph = nx.grid_2d_graph(L, L, periodic=True, create_using=nx.DiGraph())
    else:
        graph = nx.read_adjlist(args.network, create_using=nx.DiGraph())

    # initialize random state of the system
    states = {node: np.random.choice(_statespace) for node in graph.nodes_iter()}

    # num_equil_steps = int(L * 2) + 1
    # for t in xrange(num_equil_steps):
    #     states = update_states_step(graph, states, T)
    #
    # node_id = np.random.choice(xrange(graph.number_of_nodes()))
    # node_id = graph.nodes()[node_id]
    #
    # print 'debug: state of node:', states[node_id]
    # print 'debug: states of nbrs:', [states[nbr] for nbr in graph.predecessors(node_id)]
    # print 'debug: prob states node:', [prob_state(graph, node_id, states, T, s) for s in _statespace], ' where state' \
    #        ' space = ', _statespace
    #
    # succ_id = np.random.choice(xrange(len(graph.successors(node_id))))
    # succ_id = graph.successors(node_id)[succ_id]
    #
    # print 'debug: mutual information between pair:', mutual_information_step(graph, node_id, succ_id, states, T)
    #
    # print 'debug: array of mutual information between nbrs:', \
    #     [mutual_information_step(graph, node_id, succ_id, states, T) for succ_id in graph.successors(node_id)]
    # print 'debug: sum of mutual information between nbrs:', \
    #     sum([mutual_information_step(graph, node_id, succ_id, states, T) for succ_id in graph.successors(node_id)])
    # print 'debug: sum of mutual information between all (check):', \
    #     sum([mutual_information_step(graph, node_id, succ_id, states, T) for succ_id in graph.nodes()])

    if analysis in ('single_temp',):
        # r0s = [sum([mutual_information_step(graph, node, succ_id, states, T) for succ_id in graph.successors(node_id)])
        #        for node in graph.nodes()]
        #
        # plt.hist(r0s)
        # plt.show()
        pass
    elif analysis in ('fit_temp',):
        prob_edge = 2.0 * (1.0 / float(size))

        assert args.flip_prob
        target_flipping_fraction = args.flip_prob

        graph = nx.binomial_graph(size, prob_edge, directed=True)

        graph_numbered = g2c.to_numbered_graph(graph)
        graph = None  # safety, do not use anymore; use graph_numbered

        # frac_flips = equil_flip_probability_per_spin(graph_numbered, T, num_equil_steps, numruns, ntrials)
        fitted_temp = fit_temperature_to_flip_prob(graph_numbered, target_flipping_fraction)

        print 'note: fitted_temp is', fitted_temp
    elif analysis in ('nudge',):
        assert args.nudge_temp
        assert args.numsteps

        nudge_temp = args.nudge_temp

        # used e.g. in output file name as identifier
        network_name = os.path.splitext(os.path.basename(args.network))[0]

        print 'debug: network_name =', network_name

        graph_numbered = g2c.to_numbered_graph(graph)

        if args.nudge_nodeids:
            nudge_nodeids = args.nudge_nodeids
        else:
            nudge_nodeids = range(graph_numbered.number_of_nodes())

        impact_per_node = dict()

        for nodeid in nudge_nodeids:
            assert nodeid in graph_numbered.nodes()

            impact_per_node[nodeid] = []

        for runid in xrange(args.numinits):
            print 'debug: performing run #', runid, ' out of', args.numinits, '(num_equil_steps=' + \
                                                                              str(num_equil_steps) + ')'

            # initialize random state of the system
            states = [np.random.choice(_statespace) for node in xrange(graph_numbered.number_of_nodes())]

            equil_state = update_states_steps(graph_numbered, states, T, num_equil_steps,
                                              trans='async', inplace=True)[-1]

            # print 'debug: done equilibrating... Now onto impact measurement... (nudge_nodeids=' + \
            #         str(nudge_nodeids) + ')'

            for nodeid in nudge_nodeids:
                print 'debug: starting for nodeid =', nodeid

                nudge_array = np.zeros(graph_numbered.number_of_nodes())
                nudge_array[nodeid] = nudge_temp

                impact_per_node[nodeid].append(impact_of_nudge(graph_numbered, equil_state, T,
                                                          numsteps=args.numsteps, added_energy=nudge_array,
                                                          trans='async',
                                                          ntrials=ntrials))

        avg_impact_per_node = dict()
        std_impact_per_node = dict()

        for nodeid in nudge_nodeids:
            avg_impact_per_node[nodeid] = np.average(impact_per_node[nodeid], axis=0)
            std_impact_per_node[nodeid] = np.std(impact_per_node[nodeid], axis=0)

            assert len(avg_impact_per_node[nodeid]) == args.numsteps
            assert len(std_impact_per_node[nodeid]) == args.numsteps

        output_avg_impact_fn = 'avg_impact_nudge_dT' + str(nudge_temp) + '_' + str(network_name) + '_T' + str(T) \
                               + '.csv'

        fout = open(output_avg_impact_fn, 'wb')
        csvw = csv.writer(fout, delimiter=',')

        for nodeid in graph_numbered.nodes_iter():
            if avg_impact_per_node.has_key(nodeid):
                csvw.writerow(avg_impact_per_node[nodeid])
            else:
                csvw.writerow([np.nan])

        fout.close()

        output_std_impact_fn = 'std_impact_nudge_dT' + str(nudge_temp) + '_' + str(network_name) + '.csv'

        fout = open(output_std_impact_fn, 'wb')
        csvw = csv.writer(fout, delimiter=',')

        for nodeid in graph_numbered.nodes_iter():
            if std_impact_per_node.has_key(nodeid):
                csvw.writerow(std_impact_per_node[nodeid])
            else:
                csvw.writerow([np.nan])

        fout.close()

        print 'debug: wrote results of impact analysis to', output_avg_impact_fn, 'and', output_std_impact_fn

    elif analysis in ('pca',):
        # num_networks = 1
        prob_edge = 2.0 * (1.0 / float(size))
        assume_uniform_probs_next_state = True
        verbose = True
        force_connected_graph = True
        # num_init_states = 1000
        subset_lengths = [1, 2, 3, 4, 5]

        # for i in xrange(num_networks):
        # print 'note: processing graph #' + str(i) + ' out of', num_networks

        graph = nx.binomial_graph(size, prob_edge, directed=True)

        if force_connected_graph:
            max_tries_connect_graph = 1000
            try_conn = 0
            while not nx.connected.is_connected(graph.to_undirected()) and try_conn < max_tries_connect_graph:
                graph = nx.binomial_graph(size, prob_edge, directed=True)
                try_conn += 1

            if verbose:
                print 'debug: it took me', try_conn, 'trials to create a connected network.'
                if nx.connected.is_connected(graph.to_undirected()):
                    print 'debug: I succeeded.'
                else:
                    print 'debug: I failed.'

        graph_numbered = g2c.to_numbered_graph(graph)
        graph = None  # safety, do not use anymore; use graph_numbered

        matrix = []

        start_time = time.time()

        for init_state_id in xrange(num_init_states):

            if verbose:
                print 'debug: initial state #', init_state_id, ' out of', num_init_states, ' (', \
                    time.time() - start_time, 'seconds elapsed)'

            init_system_state = [np.random.choice(_statespace) for node in graph_numbered.nodes_iter()]

            # equilibrate the system, if desired (usually not in this case)
            if num_equil_steps > 0:
                update_states_steps(graph_numbered, init_system_state, T, num_equil_steps, trans='async', inplace=True)

            ipm = mutual_informations_node_nodes_nosampling(graph_numbered, T, num_runs=numruns,
                                                                 verbose=False, given_initial_state_node=True)

            assert not ipm.entropy_nextstate_node is None
            assert not np.isscalar(ipm.entropy_nextstate_node)
            assert np.isscalar(ipm.entropy_nextstate_node[0])
            assert np.isfinite(ipm.entropy_nextstate_node[0])

            # the entropy of next states (t=1) is estimated by mutual_informations_node_nodes_nosampling by sampling
            # initial states ('numruns' of them). However, ipm.entropy_nextstate_node_given_initstate_system is not
            # estimated by sampling and is thus exact. This leads to e.g. an integrated information amount of
            # 0.9999 - 1.0 = -0.000000001, which is very small and is only negative due to the sampling problem.
            #
            if assume_uniform_probs_next_state:
                assert num_equil_steps == 0

                # I assume that you are sampling random initial states to compute ipm.entropy_nextstate_node in
                # mutual_informations_node_nodes_nosampling, so numruns == 1 would seem a bit strange? numruns=10000
                # sounds more like it...
                assert numruns > 1

                # fill in the theoretical entropy of each node's next (t=1) state, assuming that the initial state is
                # initiialized randomly and the ensemble of all possible initial states is considered.
                ipm.entropy_nextstate_node = [np.log2(len(_statespace))]*graph_numbered.number_of_nodes()

                # the fact that a uniform random initial state (t=0) leads to uniform random node states at t=1 is true
                # for the kinetic ising model and the voter model, for sure. But it would e.g. not be true for the RBN
                # dynamics, or cellular automata, in general. If you added a new type of dynamics which does satisfy this
                # property then add it to the list here.
                assert _dynamics in ('glauber', 'voter_model')

                assert not ipm.entropy_nextstate_node is None
                assert not np.isscalar(ipm.entropy_nextstate_node)
                assert np.isscalar(ipm.entropy_nextstate_node[0])
                assert np.isfinite(ipm.entropy_nextstate_node[0])

            ipm.entropy_nextstate_node_given_initstate_system = \
                entropy_nextstate_nodes_given_system_initstate(graph_numbered, T, init_system_state)

            assert not ipm.entropy_nextstate_node_given_initstate_system is None

            try:
                mi_total_per_node = np.subtract(ipm.entropy_nextstate_node,
                                                ipm.entropy_nextstate_node_given_initstate_system)
            except TypeError, e:
                print 'error: ipm.entropy_nextstate_node =', ipm.entropy_nextstate_node
                print 'error: ipm.entropy_nextstate_node_given_initstate_system =', \
                    ipm.entropy_nextstate_node_given_initstate_system

                raise RuntimeError

            assert len(mi_total_per_node) == graph_numbered.number_of_nodes()
            assert np.isscalar(mi_total_per_node[0])
            assert mi_total_per_node[0] >= 0.0

            if __debug__:
                negative_mi_integrated_list = []

            mi_pairs_per_node = []

            for next_node_label in graph_numbered.nodes_iter():
                # reminder of structure: ipm.mi...[node_id][given_node_state][other_node_id]

                predecessors = graph_numbered.predecessors(next_node_label)

                mi_pairs_predecessors = \
                    [ipm.mi_pairwise_initstate_nextstate[pred_id][init_system_state[pred_id]][next_node_label]
                            for pred_id in predecessors]
                mi_pairs_all = \
                    [ipm.mi_pairwise_initstate_nextstate[pred_id][init_system_state[pred_id]][next_node_label]
                            for pred_id in graph_numbered.nodes_iter()]
                mi_total = mi_total_per_node[next_node_label]

                assert np.isscalar(mi_total)
                if not mi_total >= 0.0:
                    print '------------------------'
                    print 'WARNING: mutual_information(node, all node\'s predecessors) < 0.0  (variable name mi_total)'
                    print 'error: mi_total =', mi_total
                    print 'error: predecessors =', predecessors
                    print 'error: assume_uniform_probs_next_state =', assume_uniform_probs_next_state
                    print '------------------------'
                assert mi_total >= 0.0 or not assume_uniform_probs_next_state

                mi_integrated = mi_total - sum(mi_pairs_predecessors)

                assert np.isscalar(mi_integrated)

                '''
                NOTE:

                This part of the code measures the mutual information quantities for a single network, and for a single
                initial state of the system. As a result the initial state (for the mutual informations) is not uniformly
                random (in fact far from it), so I(sj_1 : S_0) - Sum[I(sj_1 : si_0), i] is not valid because the sum is
                not valid, and therefore it can become negative (mi_integrated). Concluding, do not use mi_integrated :-).
                '''

                if __debug__:
                    if mi_integrated < 0.0:
                        negative_mi_integrated_list.append(mi_integrated)

                if num_init_states == 1:
                    print 'node id', next_node_label, '(+1)' if init_system_state[next_node_label] == 1 else '(-1)', \
                        ':\tmi_total =', mi_total, ' ('+str(ipm.entropy_nextstate_node[next_node_label]) + ' - ' + \
                                                    str(ipm.entropy_nextstate_node_given_initstate_system[next_node_label]) \
                                                    + ')',\
                        '\tmi_pairs =', mi_pairs_predecessors

                mi_integrated = None  # be sure it is not used, because in this setting here it is not meaningful (can <0)

                mi_pairs_per_node.append(mi_pairs_all)

            if __debug__ and len(negative_mi_integrated_list) > 0:
                avg_neg_mi_int = np.average(negative_mi_integrated_list)
                min_avg_mi_int = np.min(negative_mi_integrated_list)

                assert avg_neg_mi_int > -0.1 or numruns < 10000 or _dynamics != 'glauber'

                if verbose and num_init_states == 1:
                    print 'debug:', (len(negative_mi_integrated_list) / float(graph_numbered.number_of_nodes())) * 100.0, \
                        '% of the integrated MI is negative. The average negative value is', avg_neg_mi_int, \
                        '(min =', min_avg_mi_int, ')'
                    print 'debug: negative integrated MIs:', negative_mi_integrated_list

            # TODO: try other macroscopic variables, such as the 'critical' temperature? (where |M|(T) == 0.5?)
            asymptotic_order = asymptotic_average_state(init_system_state,
                                                        steps=nx.diameter(graph_numbered.to_undirected()))

            row = list(mi_total_per_node)
            column_names = ['t'+str(i) for i in xrange(len(mi_total_per_node))]

            for next_node_label in graph_numbered.nodes_iter():
                for predecessor_node_label in graph_numbered.predecessors(next_node_label):
                    assert np.isscalar(mi_pairs_per_node[next_node_label][predecessor_node_label])

                    row.append(mi_pairs_per_node[next_node_label][predecessor_node_label])
                    column_names.append(str(predecessor_node_label)+'->'+str(next_node_label))

            matrix.append(row)

        matrix = np.transpose(matrix)

        print 'note: will now start PCA on the', len(matrix), str(len(matrix[0])) + '-dimensional data points...'

        pca_result = pca.pca_matrix(matrix, subset_lengths, column_names, colid_res=-1)

        # output to files
        g2c.write_adjlists_csv(graph_numbered, './last_graph.csv')
        fout = open('./last_pca_result.obj', 'wb')
        pickle.dump(pca_result, fout)
        fout.close()

        print 'note: number of subsets: ', map(len, pca_result)
        print 'note: principle components: ', map(lambda subs: subs[0], pca_result)
        print 'note: top 5 principle components per subset length: ', map(lambda subs: subs[:5], pca_result)

        # print 'debug: mis_node_nodes =', np.transpose([map(int, graph_numbered.nodes()),
        #                                                map(float, ipm.mi_pairwise_initstate_nextstate[0])])
        # print 'debug: graph_numbered.successors(node) =', graph_numbered.successors(0)
        # print 'debug: I(S_1 : si_0)[0] =', mi_system[0]

        # TODO: list a few measures of individual MI and the total MI (or II) and e.g. the total magnetization
        # into a props file, so that pca_*.py can process it? Or do a more Mathematica-like PCA here yourself?
        # Better?
    elif analysis in ('many_temps'):
        magnetization_per_connected_component = True

        if magnetization_per_connected_component:
            connected_components = [cc for cc in nx.connected_components(graph.to_undirected())]
            cc_sizes = map(len, connected_components)

            print 'debug: there are', len(connected_components), 'connected components in the network, over which' \
                                                                 ' I will average the magnetization. Sizes are:', \
                    (str(cc_sizes[:10]) + '...' if len(cc_sizes) > 10 else str(cc_sizes))
        else:
            connected_components = range(graph.number_of_nodes())

        plot_ix = 1
        for num_equil_steps in num_equil_steps_list:
            print 'debug: Working on the subplot ', plot_ix, ' for num_equil_steps=',num_equil_steps

            plt.subplot(len(num_equil_steps_list), 2, 2 * plot_ix - 1)

            r0s_per_temp = []
            magnetization_per_temp = []

            for T in temps:
                print 'debug: computing R0s for T=', T

                magnetization_per_temp_per_run = []
                r0s_per_temp_per_run = []

                for runid in xrange(num_init_states):
                    # equilibrate system for this temp
                    states = {node: np.random.choice(_statespace) for node in graph.nodes_iter()}
                    for t in xrange(num_equil_steps):
                        states = update_states_step(graph, states, T)

                    if not magnetization_per_connected_component:
                        magnetization_per_temp_per_run.append(np.abs(np.sum(states.values()) / float(len(states))))
                    else:
                        weighted_total_magn_ccs = 0.0

                        for cc in connected_components:
                            states_cc = [states[node] for node in cc]

                            mag_cc = np.abs(np.sum(states_cc) / float(len(states_cc)))

                            weighted_total_magn_ccs += len(states_cc) / float(graph.number_of_nodes()) * mag_cc

                        assert np.isscalar(weighted_total_magn_ccs)
                        assert 0.0 <= weighted_total_magn_ccs <= 1.0

                        magnetization_per_temp_per_run.append(weighted_total_magn_ccs)

                    r0s = [sum([mutual_information_step(graph, node, succ_id, states, T)
                                for succ_id in graph.successors(node)])
                           for node in graph.nodes()]
                    r0s_per_temp_per_run.append(r0s)

                magnetization_per_temp.append(np.average(magnetization_per_temp_per_run))
                r0s_per_temp.append(np.average(r0s_per_temp_per_run, axis=0))

            if __debug__:
                try:
                    print 'debug: max num unique values per temp:', max(map(len, map(set, r0s_per_temp)))
                except:
                    print 'debug: r0s_per_emp =', r0s_per_temp

                    assert False

            mean_r0_per_temp = map(np.average, r0s_per_temp)
            max_r0_per_temp = map(np.max, r0s_per_temp)
            # std_r0_per_temp = map(np.std, r0s_per_temp)

            plt.plot(temps, mean_r0_per_temp, '-k', linewidth=2)

            for ti in xrange(len(r0s_per_temp)):
                plt.plot([temps[ti]]*len(r0s_per_temp[ti]), r0s_per_temp[ti], 'x', color='0.5')

            plt.plot(temps, max_r0_per_temp, ':k', linewidth=1, color='0.3')
            if plot_ix == len(num_equil_steps_list):
                plt.xlabel('Temperature T')
            if plot_ix == 1:
                plt.ylabel('R0 (sum of pair-wise MI per node)')
                plt.title('R0 versus temperature. L='+str(L)+', dynamics = '+str(_dynamics) + '\n(Only meaningful'
                                                                      'for num_equil_steps=0)')

            plt.savefig('./last_result.png')
            plt.savefig('./last_result.pdf')

            plt.subplot(len(num_equil_steps_list), 2, 2 * plot_ix)

            plt.plot(temps, map(abs, magnetization_per_temp), '-k')
            if plot_ix == len(num_equil_steps_list):
                plt.xlabel('Temperature T')
            if plot_ix == 1:
                plt.ylabel('Magnetization')
            plt.ylim([0.0, 1.0])

            plot_ix += 1

        plt.show()