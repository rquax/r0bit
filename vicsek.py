__author__ = 'rquax'

import copy
import numpy as np


def normalize(vec):
    norm = np.linalg.norm(vec)
    if norm > 0:
        return np.divide(vec, norm)
    else:
        return vec  # cannot normalize, has zero norm, hopefully will fix itself

def distance(vec1, vec2):
    return np.linalg.norm(np.subtract(vec1, vec2))

class Vicsek():

    def __init__(self, locs=None, velos=None, angles=None, radius=0.3, L=1.0):

        self.locs = locs
        self.velos = velos
        self.angles = angles

        if not self.locs is None:
            self.size = len(self.locs)
        else:
            self.size = 2

        self.radius = radius
        self.L = L


    def step(self, randomize_angle_norm=0.01, randomize_velo_norm=0.01, adherence=0.2):

        self.angles = [np.add(self.angles, normalize(np.random.randn(self.size)) * randomize_angle_norm)
                       for angle in self.angles]
        self.velos = [np.add(self.velos, normalize(np.random.randn()) * randomize_velo_norm)
                      for velo in self.velos]

        for ix in xrange(self.size):
            for other_ix in xrange(self.size):
                if other_ix == ix:
                    continue  # do not interact with self
                else:
                    dist = distance(self.locs[ix], self.locs[other_ix])

                    if dist <= self.radius:
                        mean_velo = np.add(self.velos[ix], self.velos[other_ix]) / 2.0
                        mean_angle = normalize(np.add(self.angles[ix], self.angles[other_ix]))

                        weight = 1.0 - dist / self.radius

                        self.velos[ix] = adherence * weight * mean_velo + (1 - adherence * weight) * self.velos[ix]
                        self.angles[ix] = normalize(adherence * weight * mean_angle
                                                    + (1 - adherence * weight) * self.angles[ix])

        self.locs = np.add(self.locs, np.multiply(self.velos, self.angles))

        # wrap around the spatial extent of the system
        for ix in xrange(self.size):
            for posix in xrange(len(self.locs[ix])):
                if self.locs[ix][posix] > self.L:
                    self.locs[ix][posix] -= self.L
                elif self.locs[ix][posix] < 0:
                    self.locs[ix][posix] += self.L

